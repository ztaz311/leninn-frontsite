module.exports = {
    async redirects() {
        return [
            {
                source: '/404',
                destination: '/404',
                permanent: true,
            },
        ]
    },
    async rewrites() {
        return [
            {
                source: '/apiv2/:path*',
                destination: 'https://cms.leninn.com/:path*'
            },

        ]
    },
    // trailingSlash: true,
    poweredByHeader: false,
    env: {
        NEXTAUTH_URL: 'https://leninn.com',
        NEXTAUTH_URL_INTERNAL: 'https://leninn.com',
        // HOSTNAME: 'https://api-leninn-76ro.graphcdn.app',
        // HOSTNAME: 'https://leninn.com/apiv2',
        HOSTNAME: 'https://cms.leninn.com',
        HOSTNAME_CMS: 'https://cms.leninn.com',
        GOOGLE_ID: '68112483721-ehm6q26gu2815ao8e8o2nbu1304sqr8o.apps.googleusercontent.com',
        GOOGLE_SECRET: '2cXWieETUtkKNWAVP-sL_RlW',
        FACEBOOK_ID: '2738814566448671',
        FACEBOOK_SECRET: 'b0d7a40609439d19882cc82e71ffe8af',
    },
}