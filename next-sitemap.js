module.exports = {
    siteUrl: process.env.NEXTAUTH_URL || 'https://leninn.com',
    generateRobotsTxt: true,
    exclude: ['/address', '/cart', '/checkout', '/myprofile', '/order-complete', '/orders', '/search', '/slider'], // <= exclude here
    // robotsTxtOptions: {
    //     additionalSitemaps: [
    //         `${process.env.NEXTAUTH_URL || 'https://leninn.com'}/server-sitemap.xml`, // <==== Add here
    //     ],
    // },
}