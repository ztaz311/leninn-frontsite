import * as Config from '../constants/Config';
import axios from 'axios';
import base64 from 'base-64';
const callApi = (endpoint, method = 'GET', body, header = '') => {
    return axios({
        method: method,
        url: `${Config.API_URL}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            // consumer_key: `${Config.CK}`,
            // consumer_secret: `${Config.CS}`,
            lang: `${Config.LANG}`,
        }
    }).then(response => {
        // console.log('HTTP', response);
        return response.data;
    }).catch((error) => {
        // console.log('HTTP123', error);
        return error.response
    });
}
const callApiProxy = (endpoint, method = 'GET', body, header = '') => {
    return axios({
        method: method,
        url: `${Config.API_URL_PROXY}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            // consumer_key: `${Config.CK}`,
            // consumer_secret: `${Config.CS}`,
            lang: `${Config.LANG}`,
        }
    }).then(response => {
        // console.log('HTTP', response);
        return response.data;
    }).catch((error) => {
        // console.log('HTTP123', error);
        return error.response
    });
}
const callApiWP = (endpoint, method = 'GET', body, header = '') => {
    return axios({
        method: method,
        url: `${Config.API_URL_WP}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            lang: `${Config.LANG}`,
            // consumer_key: `${Config.CK}`,
            // consumer_secret: `${Config.CS}`,
        }
    }).then(response => {
        // console.log('HTTP', response);
        return response.data;
    }).catch((error) => {
        // console.log('HTTP123', error);
        return error.response
    });
}
const callApiWP_Proxy = (endpoint, method = 'GET', body, header = '') => {
    return axios({
        method: method,
        url: `${Config.API_URL_WP_PROXY}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            lang: `${Config.LANG}`,
            // consumer_key: `${Config.CK}`,
            // consumer_secret: `${Config.CS}`,
        }
    }).then(response => {
        // console.log('HTTP', response);
        return response.data;
    }).catch((error) => {
        // console.log('HTTP123', error);
        return error.response
    });
}
const callApiMAIN = (endpoint, method = 'GET', body, header = '') => {
    return axios({
        method: method,
        url: `${Config.API_URL_MAIN}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            lang: `${Config.LANG}`,
        }
    }).then(response => {
        // console.log('HTTP', response);
        return response.data;
    }).catch((error) => {
        // console.log('HTTP123', error);
        return error.response
    });
}
const callApiMAIN_Proxy = (endpoint, method = 'GET', body, header = '') => {
    return axios({
        method: method,
        url: `${Config.API_URL_MAIN_PROXY}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            lang: `${Config.LANG}`,
        }
    }).then(response => {
        // console.log('HTTP', response);
        return response.data;
    }).catch((error) => {
        // console.log('HTTP123', error);
        return error.response
    });
}
const callApiWithHeader = async (endpoint, method = 'GET', body, upload = '') => {
    // const auValue = await AsyncStorage.getItem('userToken');
    const encode = base64.encode(email + ":" + password);
    const auKey = 'Authorization';
    const auValue = 'Basic ' + encode;
    let header = {
        "Authorization": `${auValue}`,
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    if (upload === true) {
        header = {
            "Authorization": `${auValue}`,
            // "Content-Type": "application/json",
            "Accept": "application/json",
            'Content-Type': 'multipart/form-data',
        }
    } else {
        header = {
            "Authorization": `${auValue}`,
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    }
    // if (xplace) header["X-Place-Id"] = body?.place_uuid || "gVxqPuyBt9gxtAbg99Vs-";
    return axios({
        method: method,
        url: `${Config.API_URL}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            lang: `${Config.LANG}`,
        }
    }).then(response => {
        return response;
    }).catch(error => {
        if (error.response && error.response.status == 401) {
            var res = {
                data: {
                    status: 401,
                    message: 'Authentication required'
                },
                status: 401,
            }
            return res;
        } else if (error.response) {

        } else {

        }
    });
};
const callApiV3 = (endpoint, method = 'GET', body, header = '') => {
    return axios({
        method: method,
        url: `${Config.API_URL_OP}${endpoint}`,
        data: body,
        headers: header,
        timeout: 90000,
        params: {
            // consumer_key: `${Config.CK}`,
            // consumer_secret: `${Config.CS}`,
            lang: `${Config.LANG}`,
        }
    }).then(response => {
        // console.log('HTTP', response);
        return response.data;
    }).catch((error) => {
        // console.log('HTTP123', error);
        return error.response
    });
}

module.exports = {
    callApi,
    callApiWithHeader,
    callApiWP,
    callApiMAIN,
    callApiProxy,
    callApiWP_Proxy,
    callApiMAIN_Proxy,
    callApiV3
};
