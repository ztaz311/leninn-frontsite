const NextI18Next = require('next-i18next').default
// const { localeSubpaths } = require('next/config').default()
const path = require('path')

module.exports = new NextI18Next({
    defaultLanguage: 'vn',
    otherLanguages: ['vn', 'en'],
    // localeSubpaths: { en: 'en', vn: 'vn' },
    shallowRender: true,
    localePath: path.resolve('./public/locales')

})
