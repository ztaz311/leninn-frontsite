import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html>
                <Head />
                <body>
                    <Main />
                    <NextScript />
                    <script async src="https://www.googletagmanager.com/gtag/js?id=G-PYJQPWPM8B"></script>
                    {/* <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-PYJQPWPM8B');
</script> */}
                    <script dangerouslySetInnerHTML={{
                        __html: `window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                      
                        gtag('config', 'G-PYJQPWPM8B');`
                    }} />
                    <script dangerouslySetInnerHTML={{
                        __html: `window.fbAsyncInit = function() {
                            FB.init({
                                xfbml: true,
                                version: 'v10.0'
                                });
                            };

                            (function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
                            fjs.parentNode.insertBefore(js, fjs);
                           }(document, 'script', 'facebook-jssdk'));`
                    }} />

                    {/* <div className="fb-customerchat" data-page_id="1636400616586241">
                        <div id="fb-root"></div></div> */}
                    <div className="fb-customerchat"
                        attribution="biz_inbox"
                        page_id="1636400616586241">

                    </div>
                </body>
            </Html>
        )
    }
}

export default MyDocument