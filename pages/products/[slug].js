import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { Footer } from '../../components/Aside/Footer'
import { Header } from '../../components/Aside/Header'
import { ListProduct } from '../../components/Content/ListProduct'
import { Category } from "../../components/Content/Product/Category"
import { UICategory } from "../../components/Model/UIStore"
import { getProduct, getProductCategories, getLogoHome, getFooter } from '../../constants/querry'

export default function Home(props) {
    // console.log('props', props)
    const router = useRouter()
    const slug = router.query.slug || []
    const itemCategories = UICategory.useState(s => s.DATA_CATEGORY_REQUEST);
    const [categoryActive, setCategoryActive] = useState('');
    const [itemCategoriesN, setItemCategories] = useState(itemCategories.length > 0 ? itemCategories : []);

    return (
        <div className="">

            <div className="md:h-17 h-12">
                <Header pageClass={true} logo={props.logo} cate={props.cate} />
                <Head>
                    <title>Leninn | Product</title>
                    {/* <link rel="shortcut icon" href="../img/favicon.png" /> */}
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div>
                {
                    props?.data2?.filter(s => s.parent === 0).length > 0 &&
                    <Category active={slug} data={props?.data2?.filter(s => s.parent === 0)} dataChil={props?.data2?.filter(s => s.parent !== 0)} />
                }
                {props?.data && <ListProduct dataAvId={categoryActive} dataPr={props?.data} dataCa={itemCategoriesN} id={props?.id} pageInfo={props?.pageInfo} />}

                {/* {loading && <Loading />}
                {(!loading && itemCategoriesN.length > 0 && checkDonePr == true && slug.length > 0) && <ListProduct dataAvId={categoryActive} dataPr={itemProducts} dataCa={itemCategoriesN} />} */}
            </div>
            <div className="md:mt-16 mt-4">
                <Footer footer={props.footer} />
            </div>
        </div>

    )
}
export async function getStaticPaths() {
    const res = await getProductCategories()
    const data = await res;
    const resFooter = await getFooter()
    const dataFooter = await resFooter;
    const paths = data.data.data.productCategories.nodes.map(data => ({
        params: { slug: data.slug },
    }));
    return {
        paths,
        fallback: true
    }
}

export async function getStaticProps({ params }) {
    try {
        const res2 = await getProductCategories()
        const data2 = await res2;

        const resLogo = await getLogoHome()
        const dataLogo = await resLogo;

        const resFooter = await getFooter()
        const dataFooter = await resFooter;
        let xx = [...data2.data.data.productCategories.nodes]
        xx.forEach(element => {
            element.parent = element.parentDatabaseId || 0
            element.id = element.databaseId
            element.menu_order = element.menuOrder

        });

        let id = ''
        xx.forEach(element => {
            if (element.slug === params.slug) {
                id = element.id
            }
        });

        if (id == '') {
            return {
                redirect: {
                    destination: '/404',
                    permanent: false,
                },
            }
        }
        const res = await getProduct(5000, id, '')
        const data = await res;

        return {
            props: {
                data: data.data.data.products?.nodes, pageInfo: data.data.data.products?.pageInfo, data2: xx, id: id,
                logo: dataLogo.data.data.getHeader.siteLogoUrl,
                cate: data2.data.data.productCategories.nodes,
                footer: dataFooter?.data?.data
            }, // will be passed to the page component as props
            revalidate: 1
        }
    } catch (error) {
        // return {
        //     redirect: {
        //         destination: '/404',
        //         permanent: false,
        //     },
        // }
    }

}
