import Head from 'next/head'
import React from 'react'
import { Footer } from '../../components/Aside/Footer'
import { Header } from '../../components/Aside/Header'
import { ListProduct } from '../../components/Content/ListProduct'
import { Category } from "../../components/Content/Product/Category"
import { getProduct, getProductCategories, getLogoHome, getFooter } from '../../constants/querry'

export default function Home(props) {
    return (
        <div id="div4">

            <div className="md:h-17 h-12" >
                <Header pageClass={true} logo={props.logo} cate={props.cate} />
                <Head>
                    <title>Leninn | Product</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div >
                {
                    props?.data?.filter(s => s.parent === 0)?.length > 0 &&
                    <Category active={''} data={props?.data.filter(s => s.parent === 0)} dataChil={props.data.filter(s => s.parent !== 0)} />
                }
                {props?.data2?.length > 0 &&
                    <ListProduct dataAvId={''} dataPr={props?.data2} pageInfo={props?.pageInfo} />
                }


            </div>
            <div className="md:mt-16 mt-4">
                <Footer footer={props.footer} />
            </div>
        </div>

    )
}
export async function getStaticProps({ params }) {
    const res = await getProductCategories()
    const data = await res;
    let xx = [...data.data.data.productCategories.nodes]
    xx.forEach(element => {
        element.parent = element.parentDatabaseId || 0
        element.id = element.databaseId
        element.menu_order = element.menuOrder

    });

    const res2 = await getProduct(100000)
    const data2 = await res2;
    const resLogo = await getLogoHome()
    const dataLogo = await resLogo;
    const resFooter = await getFooter()
    const dataFooter = await resFooter;
    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: {
            data: xx, data2: data2.data.data.products?.nodes, pageInfo: data2.data.data.products?.pageInfo,
            logo: dataLogo.data.data.getHeader.siteLogoUrl,
            cate: data.data.data.productCategories.nodes,
            footer: dataFooter?.data?.data

        }, // will be passed to the page component as props
        revalidate: 1
    }
}