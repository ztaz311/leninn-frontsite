import React, { useEffect, useState } from 'react'
import { actGetOrder } from '../action/auth'
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { Loading } from "../components/Aside/Loading"
import { UIStore } from "../components/Model/UIStore"
import { Menu } from "../components/MyProfile/Menu"
import { Order } from "../components/MyProfile/Order"
import { Title } from '../components/MyProfile/Title'
import { useRouter } from 'next/router'
import { getListOrder } from '../constants/querry'
import { signOut, useSession } from 'next-auth/client';

export default function Home() {
    // const authID = UIStore.useState(s => s.auth?.data?.profile?.id)
    const [dataOder, setDataOrder] = useState([])
    const [perPage, setPerPage] = useState(12)
    const [page, setPage] = useState(1)
    const [loading, setLoading] = useState(false)
    const [loading2, setLoading2] = useState(true)
    const [check, setCheck] = useState(0)
    const router = useRouter()
    const auth = UIStore.useState(s => s.auth)
    const profile = UIStore.useState(s => s.profile)
    // console.log('profile', profile)
    const [session] = useSession();

    const handleClickLogOut = async () => {
        await UIStore.update(s => { s.auth = { token: '' }; s.profile = {} });
        if (session) {
            await signOut()
        }
        // if (localStorage.getItem('auth') !== undefined && localStorage.getItem('auth') !== null) {
        //     localStorage.removeItem('auth')
        // }
        document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        setTimeout(() => {
            router.replace('/')
        }, 300);
    }
    useEffect(async () => {
        if (auth?.customer?.databaseId === '' || auth?.customer?.databaseId === undefined) {
            setCheck(check + 1)
        } else {
            await setLoading(true)
            await getListOrder(auth.customer.databaseId, auth.token).then(data => {
                // console.log('1,dat', data);
                if (data.status === 200) {
                    if (data?.data?.data?.orders?.nodes.length >= perPage) {
                        setLoading2(true)
                    } else {
                        setLoading2(false)
                    }
                } else {
                    setLoading2(false)
                }
                if (data?.data?.data?.orders?.nodes !== null && data?.data?.data?.orders?.nodes !== undefined) {
                    setDataOrder([...dataOder, ...data?.data?.data?.orders?.nodes])
                    setLoading(false)
                } else {
                    handleClickLogOut()
                }
            })
            // async function setLoad() {
            //     if (auth.token !== undefined) {
            //         await setLoading(true)
            //         await actGetOrder(auth.user.userId, page, perPage).then(data => {
            //             if (data.status === 200) {
            //                 if (data.data.length >= perPage) {
            //                     setLoading2(true)
            //                 } else {
            //                     setLoading2(false)
            //                 }
            //             } else {
            //                 setLoading2(false)
            //             }
            //             setDataOrder([...dataOder, ...data.data])
            //             setLoading(false)
            //         })
            //     }
            // }
            // setLoad()
        }
    }, [page])
    useEffect(async () => {
        if (check > 0) {
            if (auth?.customer?.databaseId === '' || auth?.customer?.databaseId === undefined) {
                router.replace('/')
            } else {
                await setLoading(true)
                await getListOrder(auth.customer.databaseId, auth.token).then(data => {
                    if (data.status === 200) {
                        if (data?.data?.data?.orders?.nodes.length >= perPage) {
                            setLoading2(true)
                        } else {
                            setLoading2(false)
                        }
                    } else {
                        setLoading2(false)
                    }
                    if (data?.data?.data?.orders?.nodes !== null && data?.data?.data?.orders?.nodes !== undefined) {
                        setDataOrder([...dataOder, ...data?.data?.data?.orders?.nodes])
                        setLoading(false)
                    } else {
                        handleClickLogOut()
                    }
                })
                // async function setLoad() {
                //     if (authID !== undefined) {
                //         await setLoading(true)
                //         await actGetOrder(authID, page, perPage).then(data => {
                //             if (data.status === 200) {
                //                 if (data.data.length >= perPage) {
                //                     setLoading2(true)
                //                 } else {
                //                     setLoading2(false)
                //                 }
                //             } else {
                //                 setLoading2(false)
                //             }
                //             setDataOrder([...dataOder, ...data.data])
                //             setLoading(false)
                //         })
                //     }
                // }
                // setLoad()
            }
        }
    }, [check, page])

    const hanleSeeMore = () => {
        setPage(page + 1)
    }

    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
            </div>
            <div>
                <Title />
                <div className="container-fix  mt-6">
                    <div className="px-4">
                        <div className="grid md:grid-cols-4 grid-cols-1 gap-4">
                            <div className="">
                                <Menu dataActive={'orders'} />
                            </div>
                            <div className="md:col-span-3">

                                {
                                    dataOder.length > 0 || !loading ?
                                        <Order orders={dataOder} hanleSeeMore={hanleSeeMore} loading2={loading2} />
                                        : <Loading />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>

    )
}
