import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
const options = {
    // site: process.env.NEXTAUTH_URL,
    session: {
        // Use JSON Web Tokens for session instead of database sessions.
        // This option can be used with or without a database for users/accounts.
        // Note: `jwt` is automatically set to `true` if no database is specified.
        jwt: true,

        // Seconds - How long until an idle session expires and is no longer valid.
        // maxAge: 30 * 24 * 60 * 60, // 30 days
        maxAge: 10 * 60 * 60, // 1 days

        // Seconds - Throttle how frequently to write to database to extend a session.
        // Use it to limit write operations. Set to 0 to always update the database.
        // Note: This option is ignored if using JSON Web Tokens 
        updateAge: 24 * 60 * 60, // 24 hours
    },
    providers: [
        Providers.Google({
            clientId: process.env.GOOGLE_ID,
            clientSecret: process.env.GOOGLE_SECRET,
        }),
        Providers.Facebook({
            clientId: process.env.FACEBOOK_ID,
            clientSecret: process.env.FACEBOOK_SECRET
        })
    ],

    callbacks: {
        // async redirect(url, baseUrl) {
        //     console.log('a', url);
        //     console.log('b', baseUrl);
        //     return 'https://beta.leninn.com'
        // },
        async session(session, token) {
            // Add property to session, like an access_token from a provider.
            session.accessToken = token.accessToken
            // maxAge: 30 * 24 * 60 * 60
            maxAge: 1 * 60 * 60
            return session
        },
        async jwt(token, user, account) {
            // Initial sign in
            if (account && user) {
                return {
                    accessToken: account.accessToken,
                    accessTokenExpires: Date.now() + account.expires_in * 1000,
                    refreshToken: account.refresh_token,
                    user,
                };
            }

            // Return previous token if the access token has not expired yet
            if (Date.now() < token.accessTokenExpires) {
                return token;
            }

            // Access token has expired, try to update it
            return token
        },
    },
}
export default (req, res) => NextAuth(req, res, options)