import { useEffect } from 'react'
import { signIn, useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { Loading } from '../../components/Aside/Loading'
const SignInPage = () => {
    const [session, loading] = useSession()
    const router = useRouter()
    const key = router.asPath.split('/')
    // console.log('12');
    useEffect(() => {
        if (session === null || session === undefined) {
            if (key[2] === 'facebook' && loading) {
                // console.log('1234');
                signIn('facebook')
            } else if (key[2] === 'google' && loading) {
                signIn('google')
            }
        } else {
            window.close()
        }
    }, [session])

    return (
        <div className="text-center mt-12">Đang chuyển hướng....
            <br />
            <Loading />
        </div>
    )
}

export default SignInPage