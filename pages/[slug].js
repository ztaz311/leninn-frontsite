import React from 'react'
import { actGetIntro } from "../action/page"
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { Contact } from '../components/Content/Contact'
import { getLogoHome, getProductCategories } from '../constants/querry';

export default function Home(props) {

    const { t } = props
    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} logo={props.logo} cate={props.cate} />
            </div>
            <div>
                {!Array.isArray(props?.data?.data) && <Contact data={props.data.data} />}
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>
    )
};


export async function getStaticPaths() {

    return {
        paths: [
            { params: { slug: 'return-policy' } },
            { params: { slug: 'chinh-sach-doi-tra' } },
            { params: { slug: 'privacy-policy' } },
            { params: { slug: 'chinh-sach-bao-mat' } },
            // { params: { slug: 'terms-conditions' } },
            { params: { slug: 'about' } },
            { params: { slug: 've-chung-toi' } },
        ],
        fallback: false
    }
}
export async function getStaticProps({ params }) {
    const res = await actGetIntro(params.slug, null, 'proxy')
    const resLogo = await getLogoHome()
    const resCategory = await getProductCategories()
    const dataLogo = await resLogo;
    const dataCate = await resCategory;
    const data = await res;
    if (!data) {
        return {
            notFound: true,
        }
    }
    return {
        props: { data: data, logo: dataLogo.data.data.getHeader.siteLogoUrl, cate: dataCate.data.data.productCategories.nodes }, // will be passed to the page component as props
        revalidate: 1

    }
}