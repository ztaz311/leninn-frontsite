import '../styles/globals.css'
import { appWithTranslation } from '../i18n'
import Router from 'next/router';
import React, { useEffect, useState, useRef } from 'react'
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
// import { Loading } from "../components/Aside/Loading";
import { LoadingAll } from '../components/Aside/LoadingAll';
import { ToastProvider, useToasts } from 'react-toast-notifications';
import { UIStore } from "../components/Model/UIStore";
import 'react-inner-image-zoom/lib/InnerImageZoom/styles.css';
import "keen-slider/keen-slider.min.css"
import Animate from 'react-smooth'
import { Provider } from 'next-auth/client'
import { providers } from "next-auth/client";
import '../styles/styles.css'

// import MessengerCustomerChat from 'react-messenger-customer-chat';
// import MessengerChat from '../components/MessengerChat'

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    let comment = document.createComment(`
    =========================================================
    * Power by TechUp Vietnam
    =========================================================

    * Home Page: https://www.techup.vn
    * Email: hello@techup.vn
    * Hotline: 0949893869 
    * Coded by TechUp Vietnam
    =========================================================
    `)
    document.insertBefore(comment, document.documentElement);
  }, [])



  const [loading, setLoading] = useState(false)
  const [scroll, setScroll] = useState(false)
  Router.events.on('routeChangeStart', () => { NProgress.start(), setLoading(true) });
  Router.events.on('routeChangeComplete', () => { NProgress.done(), setLoading(false) });
  Router.events.on('routeChangeError', () => { NProgress.done(), setLoading(false) });

  useEffect(() => {
    const auth = ("; " + document.cookie).split("; auth=").pop().split(";").shift()
    const next18 = ("; " + document.cookie).split("; next-i18next=").pop().split(";").shift()
    UIStore.update(s => { s.language = next18 })

    if (auth !== undefined && auth !== null && auth !== '') {
      try {
        if (JSON.parse(auth)?.authToken === undefined) {
          document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
        } else {
          UIStore.update(s => { s.auth = { ...JSON.parse(auth), token: JSON.parse(auth)?.authToken }; })
        }


      } catch (error) {
        document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
      }
    }

    providers().then(data => {
      UIStore.update(s => { s.providers = data; })
    })

    //direct https
    var httpTokens = /^http:\/\/(.*)$/.exec(window.location.href);
    if (httpTokens) {
      window.location.replace('https://' + httpTokens[1]);
    }
  }, [])
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [scroll])
  const [showUp, setShowUp] = useState(false)
  const handleScroll = () => {
    var scrollY = window.scrollY;
    if (scrollY > 10) {
      setShowUp(true)
    } else {
      setShowUp(false)
    }
  }

  return (
    <>
      <Provider session={pageProps.session}>
        {loading && <LoadingAll />}
        <ToastProvider autoDismissTimeout={3000} autoDismiss>
          <div>
            <Component {...pageProps} />

            {showUp &&
              <Animate from={{ opacity: 0, transform: 'translateX(0)', }}
                to={{ opacity: 1, transform: 'translateX(300)', }}
                easing="ease-in"
              >
                {
                  ({ opacity }) =>
                  (
                    <button className="fixed bottom-24 right-5 " style={{ opacity }} onClick={() => setScroll(!scroll)}>
                      <img className="" src="../img/ic_up.svg" alt="qa" className="" />

                    </button>
                  )
                }
              </Animate>
            }

          </div>
        </ToastProvider>
      </Provider>
    </>
  )
}

export default appWithTranslation(MyApp)
