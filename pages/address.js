import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { useToasts } from 'react-toast-notifications'
import { actGetInfor, actUpdateInfor } from '../action/auth'
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { Loading } from "../components/Aside/Loading"
import { UIStore } from "../components/Model/UIStore"
import { Address } from "../components/MyProfile/Address"
import { Menu } from "../components/MyProfile/Menu"
import { Title } from '../components/MyProfile/Title'
import { withTranslation } from '../i18n'
import { getInforUser, updateAddress } from '../constants/querry'
import { signOut, useSession } from 'next-auth/client';

function Home(props) {
    const router = useRouter()
    const { addToast } = useToasts();
    // const authID = UIStore.useState(s => s.auth?.data?.profile?.id)
    const auth = UIStore.useState(s => s.auth)
    const profile = UIStore.useState(s => s.profile)
    const [check, setCheck] = useState(0)
    const [session] = useSession();

    const handleClickLogOut = async () => {
        await UIStore.update(s => { s.auth = { token: '' }; s.profile = {} });
        if (session) {
            await signOut()
        }
        // if (localStorage.getItem('auth') !== undefined && localStorage.getItem('auth') !== null) {
        //     localStorage.removeItem('auth')
        // }
        document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        setTimeout(() => {
            router.replace('/')
        }, 300);
    }
    useEffect(() => {
        if (auth?.customer?.id === '' || auth?.customer?.id === undefined) {
            setCheck(check + 1)
        } else {

            if (profile?.id !== undefined) {
                getInforUser(auth.customer.id, auth?.token).then(data => {
                    if (data?.data?.data?.customer !== null && data?.data?.data?.customer !== undefined) {
                        UIStore.update(s => { s.profile = data?.data?.data?.customer })
                    } else {
                        handleClickLogOut()
                    }
                })
            } else {
                setCheck(check + 1)
            }
        }
    }, [auth])

    useEffect(() => {
        if (check > 0) {
            if (auth?.customer?.id === '' || auth?.customer?.id === undefined) {
                router.replace('/')
            } else {
                getInforUser(auth.customer.id, auth?.token).then(data => {
                    // console.log('2', data)
                    if (data?.data?.data?.customer !== null && data?.data?.data?.customer !== undefined) {
                        UIStore.update(s => { s.profile = data?.data?.data?.customer })
                    } else {
                        handleClickLogOut()
                    }
                })
            }
        }
    }, [check])

    const updateCustomer = (data, close) => {
        // var body = {

        //     shipping: {
        //         first_name: data.fullname,
        //         "address_1": data.address,
        //         "city": data.city,
        //         email: data.email
        //     },
        //     billing: {
        //         phone: data.phone,
        //         first_name: data.fullname,
        //         "address_1": data.address,
        //         "city": data.city,
        //         email: data.email
        //     }
        // }

        // console.log('body', body);
        updateAddress(auth.customer.id, data.fullname, data.phone, data.city, data.address, data.email, auth?.token).then(res => {
            // console.log('res', res);
            if (res?.data?.data?.updateCustomer?.customer !== undefined) {
                addToast(props.t('saved_successfully'), { appearance: 'success' });
                let xx = { ...profile }
                xx.billing = {
                    ...xx.billing,
                    address1: data.address,
                    city: data.city,
                    phone: data.phone,
                    firstName: data.fullname,
                    email: data.email
                }
                UIStore.update(s => { s.profile = xx })
            } else {
                addToast(props.t('error'), { appearance: 'error' });
            }

        })
        // actUpdateInfor(authID, body).then(data => {
        //     if (data.status === 200) {
        //         UIStore.update(s => { s.profile = data.data })
        //         // close()
        //         addToast(props.t('saved_successfully'), { appearance: 'success' });
        //     } else {
        //         addToast(props.t('error'), { appearance: 'error' });
        //     }
        // })
    }



    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
            </div>
            <div>
                <Title />
                <div className="container-fix mt-6">
                    <div className="px-4">
                        <div className="grid md:grid-cols-4 grid-cols-1 gap-4">
                            <div className="">
                                <Menu dataActive={'address'} />
                            </div>
                            <div className="md:col-span-3">
                                {
                                    profile?.id === undefined ?
                                        <Loading /> :
                                        <Address profile={profile.billing} updateCustomer={(data, close) => updateCustomer(data, close)} />
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>

    )
}

export default withTranslation('common')(Home);