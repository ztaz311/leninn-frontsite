import Head from 'next/head'
import { Header } from '../components/Aside/Header'
import { Footer } from '../components/Aside/Footer'

export default function Custom404() {
    return (
        <div className="">

            <div className="md:h-17 h-12">
                <Header pageClass={true} />
                <Head>
                    <title>Leninn | 404</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div>
                <h1 className='justify-items-center items-center text-center mt-6'>404 - Page Not Found</h1>
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>
    )
}