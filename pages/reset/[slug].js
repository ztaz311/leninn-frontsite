import Head from 'next/head'
import { Header } from '../../components/Aside/Header'
import { Footer } from '../../components/Aside/Footer'
import { withTranslation } from '../../i18n'
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router'
import { actChangePasswordEmail } from '../../action/auth'
import { useToasts } from 'react-toast-notifications'
import { UIStore } from '../../components/Model/UIStore'
function Reset(props) {
    const router = useRouter()
    const urlParams = new URLSearchParams(router.query.slug);
    const { addToast } = useToasts();


    const [data, setData] = useState({
        password: '',
        passwordErr: '',
        confirm_password: '',
        confirm_passwordErr: ''
    })

    const onCheck = () => {
        let password = ''
        let confirm_password = ''
        if (data.password === '') {
            password = props.t('password_required')
        } else if (data.password.length < 6) {
            password = props.t('password_must')
        } else {
            password = ''
        }

        if (data.confirm_password === '') {
            confirm_password = props.t('confirm_password_required')
        } else if (data.confirm_password !== data.password) {
            confirm_password = props.t('compare_password')
        } else {
            confirm_password = ''
        }

        setData({ ...data, passwordErr: password, confirm_passwordErr: confirm_password })

        if (password === '' && confirm_password === '') {
            var body = {
                key: urlParams.get('key'),
                login: urlParams.get('login'),
                password: data.password
            }
            actChangePasswordEmail(body).then(data => {
                if (data.status === 200) {
                    addToast(props.t('success'), { appearance: 'success' });
                    UIStore.update(s => { s.checkChangePassword = true })
                    router.push('/')
                } else {
                    addToast(props.t('error'), { appearance: 'error' });
                }
            })
        }
    }

    const { t } = props
    return (
        <div className="">

            <div className="md:h-17 h-12">
                <Header pageClass={true} />
                <Head>
                    <title>Leninn</title>
                    <link rel="shortcut icon" href="../img/favicon.png" />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div>
                <h1 className='justify-items-center items-center text-center mt-6 mx-0 md:mx-64'>
                    <div className="pt-6 pb-6 mx-8">
                        <div className="">
                            <div className="mb-3">
                                <div className="text-base font-semibold mb-3">{t('new_password')}</div>
                                <input className="px-6 text-lg py-4 w-full md:w-96  border border-gray-300" placeholder={t('password')} value={data.password} onChange={(e) => setData({ ...data, password: e.target.value })} type="password" />
                                <div className="text-red-500">{data.passwordErr}</div>
                            </div>
                            <div className="mb-3">
                                <input className="px-6 text-lg py-4 w-full md:w-96 border border-gray-300" placeholder={t('confirm_password')} value={data.confirm_password} onChange={(e) => setData({ ...data, confirm_password: e.target.value })} type="password" />
                                <div className="text-red-500">{data.confirm_passwordErr}</div>
                            </div>
                            <div className="flex flex-col items-center md:mt-9 mt-3">
                                <button className="text-lg w-full md:w-96 py-4 bg-black text-white font-bold uppercase" onClick={() => onCheck()}>{t('send')}</button>
                            </div>
                        </div>
                    </div>
                </h1>

            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>
    )
}
export default withTranslation('common')(Reset);