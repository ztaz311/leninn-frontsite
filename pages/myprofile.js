import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { useToasts } from 'react-toast-notifications'
import { actGetInfor, actUpdateInfor } from '../action/auth'
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { Loading } from "../components/Aside/Loading"
import { UIStore } from "../components/Model/UIStore"
import { Menu } from "../components/MyProfile/Menu"
import { MyProfile } from "../components/MyProfile/MyProfile"
import { Title } from '../components/MyProfile/Title'
import { withTranslation } from '../i18n'
import { getInforUser, updateInfor } from '../constants/querry'
import { signOut, useSession } from 'next-auth/client';

function Home(props) {
    const { addToast } = useToasts();
    const router = useRouter()
    const [check, setCheck] = useState(0)
    // const authID = UIStore.useState(s => s.auth?.data?.profile?.id)
    const auth = UIStore.useState(s => s.auth)
    const profile = UIStore.useState(s => s.profile)
    const [session] = useSession();

    const handleClickLogOut = async () => {
        await UIStore.update(s => { s.auth = { token: '' }; s.profile = {} });
        if (session) {
            await signOut()
        }
        // if (localStorage.getItem('auth') !== undefined && localStorage.getItem('auth') !== null) {
        //     localStorage.removeItem('auth')
        // }
        document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        setTimeout(() => {
            router.replace('/')
        }, 300);
    }
    // console.log('auth?.token', auth?.token)
    useEffect(() => {
        if (auth?.token === '') {
            setCheck(check + 1)
        } else {
            if (profile?.email === undefined) {
                getInforUser(auth.customer.id, auth?.token).then(data => {
                    // console.log('1', data);
                    if (data?.data?.data?.customer !== null && data?.data?.data?.customer !== undefined) {
                        UIStore.update(s => { s.profile = data?.data?.data?.customer })
                    } else {
                        handleClickLogOut()
                    }
                })
            }
            // if (profile?.id === undefined) {
            //     getInforUser()
            //     // actGetInfor(authID).then(data => {
            //     //     if (data?.status === 200) {
            //     //         // setData(data.data)
            //     //         UIStore.update(s => { s.profile = data.data })
            //     //     }
            //     // })
            // }
        }
    }, [auth])

    useEffect(() => {
        if (check > 0) {
            if (auth?.token === '') {
                router.replace('/')
            } else {
                getInforUser(auth.customer.id, auth?.token).then(data => {
                    // console.log('2', data);
                    if (data?.data?.data?.customer !== null && data?.data?.data?.customer !== undefined) {
                        UIStore.update(s => { s.profile = data?.data?.data?.customer })
                    } else {
                        handleClickLogOut()
                    }
                })
                // actGetInfor(authID).then(data => {
                //     if (data?.status === 200) {
                //         // setData(data.data)
                //         UIStore.update(s => { s.profile = data.data })
                //     }
                // })
            }
        }
    }, [check])

    const updateCustomer = (name, phone, email, close) => {


        updateInfor(auth.user.id, auth.customer.id, name, phone, email, auth.token).then(data => {
            if (data?.data?.data?.updateCustomer?.customer?.billing !== undefined && data?.data?.data?.updateUser?.user !== undefined) {
                let xx = { ...profile }
                xx.firstName = data.data.data.updateUser.user.firstName
                xx.email = email
                xx.billing = { ...xx.billing, phone: data?.data?.data?.updateCustomer?.customer?.billing.phone }
                UIStore.update(s => { s.profile = xx })
                // close()
                addToast(props.t('saved_successfully'), { appearance: 'success' });
            } else {
                // console.log('data', data.data.errors[0].message);

                addToast(data.data.errors[0].message, { appearance: 'error' });
            }

        })

        // actUpdateInfor(authID, body).then(data => {
        //     if (data.status === 200) {
        //         UIStore.update(s => { s.profile = data.data })
        //         // close()
        //         addToast(props.t('saved_successfully'), { appearance: 'success' });
        //     } else if (data.status === 400) {
        //         addToast(props.t('err_email'), { appearance: 'error' });

        //     } else {
        //         addToast(props.t('error'), { appearance: 'error' });
        //     }
        // })
    }


    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
            </div>
            <div>
                <Title />
                <div className="container-fix  mt-6">
                    <div className="px-4">
                        <div className="grid md:grid-cols-4 grid-cols-1 gap-4">
                            <div className="">
                                <Menu dataActive={'myprofile'} />
                            </div>
                            <div className="col-span-3">
                                {
                                    profile?.id === undefined ?
                                        <Loading />

                                        : <MyProfile data={profile} updateCustomer={(name, phone, close) => updateCustomer(name, phone, close)} profile={profile} auth={auth} />
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>
    )
}
export default withTranslation('common')(Home);