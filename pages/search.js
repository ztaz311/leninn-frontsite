import Head from 'next/head'
import React, { useState, useEffect } from 'react'
import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router'
import { Header } from '../components/Aside/Header'
import { Search } from '../components/Content/Search'
import { Footer } from '../components/Aside/Footer'
import { Loading } from "../components/Aside/Loading"
import { SeeMore } from '../components/Button/SeeMore'
import { ItemProduct } from '../components/Content/ItemProduct'
import { ItemBlog } from '../components/Content/ItemBlog'
import { i18n, withTranslation } from '../i18n'
import Link from 'next/link'
import { getProduct } from '../constants/querry'
import { actGetListSearchProduct } from "../action/page"
function Home(props) {
    const router = useRouter()
    const keyword = router.query.keyword || ''
    const [loading, setLoading] = useState(true)
    const [typeS, setTypeS] = useState('product')
    const [itemSearchProduct, setItemSearchProduct] = useState([])
    const [perPage, setPerPage] = useState(12)
    const [pagePr, setPagePr] = useState(1)
    const [showSeeMore, setShowSeeMore] = useState(false)
    const [aa, setaa] = useState(false)
    const [pageInfo, setPageInfo] = useState('')
    const hanleSeeMore = (e) => {
        setPagePr(pagePr + 1);
        setShowSeeMore(false);
        setLoading(true);
    }
    // const setSearchPrAgain = async x => {
    //     let dataPr = [...itemSearchProduct]
    //     for await (let element of x.data) {
    //         var imgN = element.img ? element.img.slide(0, element.img.length - 4) : ''
    //         var arr1 = {
    //             id: element.id,
    //             name: element.title,
    //             slug: element.slug,
    //             permalink: element.permalink,
    //             date_created: element.date_created,
    //             status: element.status,
    //             regular_price: element.regular_price,
    //             sale_price: element.sale_price,
    //             type: element.type,
    //             price: element.price,
    //             images: [{ src: imgN + '-570x570.jpg' }],
    //         }
    //         dataPr.push(arr1)
    //     }
    //     await setItemSearchProduct(dataPr);
    //     await setLoading(false);
    //     await seeMoreFx()
    //     function seeMoreFx() {
    //         if ((x.total > (x.data.length * pagePr)) && x.data.length <= perPage) {
    //             setShowSeeMore(true)
    //         }
    //     }
    // }
    // const setSearchPrAgainNew = async x => {
    //     let dataPr = []
    //     for await (let element of x.data) {
    //         var imgN = element.img ? element.img.slice(0, element.img.length - 4) : ''
    //         var arr1 = {
    //             id: element.id,
    //             name: element.title,
    //             slug: element.slug,
    //             permalink: element.permalink,
    //             date_created: element.created_at,
    //             status: element.status,
    //             regular_price: element.regular_price,
    //             sale_price: element.sale_price,
    //             type: element.type,
    //             price: element.price,
    //             images: [{ src: imgN + '-570x570.jpg' }],
    //         }
    //         dataPr.push(arr1)
    //     }
    //     await setItemSearchProduct(dataPr);
    //     await setLoading(false);
    //     await seeMoreFx()
    //     function seeMoreFx() {
    //         if ((x.total > (x.data.length * pagePr)) && x.data.length <= perPage) {
    //             setShowSeeMore(true)
    //         }
    //     }
    // }

    useEffect(() => {
        if (keyword) {
            setPerPage(1);
            setLoading(true)
            // setItemSearchProduct([])
            // actGetListSearchProduct(perPage, pagePr, keyword, typeS, null).then(data => setSearchPrAgainNew(data))
            getProduct(12, 0, '', keyword).then(data => {
                setLoading(false)
                if (data.data.data.products.pageInfo.hasNextPage) {
                    setShowSeeMore(true)
                    setPageInfo(data.data.data.products.pageInfo.endCursor)
                } else {
                    setShowSeeMore(false)
                }

                // console.log('data', data.data.data.products)
                setItemSearchProduct(data.data.data.products.nodes)
            })
        }
    }, [keyword]);
    useEffect(() => {
        if (pagePr > 1) {
            setLoading(true)
            // setItemSearchProduct([])
            getProduct(12, 0, pageInfo, keyword).then(data => {
                setLoading(false)
                if (data.data.data.products.pageInfo.hasNextPage) {
                    setShowSeeMore(true)
                    setPageInfo(data.data.data.products.pageInfo.endCursor)
                } else {
                    setShowSeeMore(false)
                }
                // console.log('data', data.data.data.products.nodes)
                setItemSearchProduct([...itemSearchProduct, ...data.data.data.products.nodes])
            })
            // actGetListSearchProduct(perPage, pagePr, keyword, typeS, null).then(data => setSearchPrAgain(data))
        }
    }, [pagePr]);
    const { t } = props
    // console.log('123', itemSearchProduct)
    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
            </div>
            <div>
                <div className="container-fix">
                    <div className="px-4">
                        <div className="md:mt-20 mt-8">
                            <div>
                                <div className="leading-5 mb-2">
                                    {t('results_for_search')}
                                </div>
                                <div className="font-semibold text-xl leading-7">
                                    {keyword}
                                </div>
                            </div>
                            <div className="md:mt-12 mt-6">
                                <div className="flex items-center font-semibold opacity-50 text-xs">
                                    <div className="cursor-pointer py-2 md:mr-12 mr-6">{t('product')}</div>
                                    <div className="cursor-pointer py-2 md:mr-12 mr-6 hidden">{t('gallery')}</div>
                                    <div className="cursor-pointer py-2 md:mr-12 mr-6 hidden">{t('blog')}</div>
                                </div>
                                <div className="md:mt-14 mt-8">
                                    <div className="product-search">
                                        <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
                                            {
                                                itemSearchProduct.length > 0 && itemSearchProduct.map((content, keys) => {
                                                    return <ItemProduct key={keys} data={content} />
                                                })


                                            }
                                            {
                                                itemSearchProduct.length === 0 && <div><h1><b>{t('empty_product')}</b></h1></div>

                                            }
                                        </div>
                                    </div>

                                    <div className="gallery-search hidden">
                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-8 gap-y-4">
                                            <Link href="/">
                                                <a>
                                                    <div className="border-b-2 border-gray-300 flex flex-col justify-between md:h-24">
                                                        <div className=" text-lg font-semibold leading-5">
                                                            How women's only mountain sports brand LaMunt focuses on
                                                            the female perspective
                                                        </div>
                                                        <div className="text-xs opacity-50 leading-3 pb-5">
                                                            Gallery
                                                        </div>
                                                    </div>
                                                </a>
                                            </Link>
                                        </div>
                                    </div>
                                    <div className="blog-search hidden">
                                        <div>
                                            {
                                                aa && <ItemBlog src={'../img/img_noi_bat.png'} />
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            {
                                showSeeMore && <SeeMore clickSeeMore={(e) => hanleSeeMore(e)} />
                            }
                            {loading && <Loading />}
                        </div>
                    </div>
                </div>
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>

    )
};
export default withTranslation('common')(Home)