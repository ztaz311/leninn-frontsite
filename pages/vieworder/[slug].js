import Head from 'next/head'
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { Header } from '../../components/Aside/Header'
import { Footer } from '../../components/Aside/Footer'
import Link from 'next/link'
import { Menu } from "../../components/MyProfile/Menu";
// import { actGetListSearchProduct } from "../action/page"
import { OrderDetail } from '../../components/MyProfile/OrderDetail'
import { Title } from '../../components/MyProfile/Title'
import { UIStore } from "../../components/Model/UIStore";
import { actDetailtOrder } from '../../action/auth'
export default function Home() {

    const router = useRouter()
    // const slug = router.query.slug || ''
    const auth = UIStore.useState(s => s.auth)
    const item = UIStore.useState(s => s.order)
    const [data, setData] = useState('')
    // useEffect(() => {
    //     if (authID !== undefined) {
    //         actDetailtOrder(slug).then(data => {
    //             console.log('data1', data)
    //             if (data.status === 200) {
    //                 setData(data.data)
    //             }
    //         })
    //     }
    // }, [authID])
    const [check, setCheck] = useState(0)
    useEffect(() => {
        if (auth?.token === '') {

            setCheck(check + 1)
        } else {

            setData(item)
            // actDetailtOrder(slug).then(data => {
            //     if (data.status === 200) {
            //         setData(data.data)
            //     }
            // })
        }
    }, [auth])

    useEffect(() => {
        if (check > 0) {
            if (auth?.token === '') {
                router.replace('/')
            } else {
                // actDetailtOrder(slug).then(data => {
                //     if (data.status === 200) {
                //         setData(data.data)
                //     }
                // })
                setData(item)
            }
        }
    }, [check])

    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
            </div>
            <div>
                <Title />
                <div className="container-fix mt-6">
                    <div className="px-4">
                        <div className="grid md:grid-cols-4 grid-cols-1 gap-4">
                            <div className="">
                                <Menu dataActive={'orders'} />
                            </div>
                            <div className="md:col-span-3">
                                {
                                    data !== '' && <OrderDetail data={data} />

                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>

    )
}
