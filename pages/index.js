import Link from 'next/link'
import React from 'react'
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { ListProductHome } from '../components/Content/ListProductHome'
import { ListQA } from '../components/Content/ListQA'
import { getBanner, getLogoClient, getProduct, getProductBestSeller, getLogoHome, getProductCategories, getFooter } from '../constants/querry'
import { withTranslation } from '../i18n'
import { Slide } from 'react-slideshow-image';

function Home(props) {
  const { t } = props

  return (
    <div>
      <div className="h-0">
        <Header logo={props.logo} cate={props.cate} />
      </div>
      <div>
        <Slide easing="ease" indicators={true} pauseOnHover={false}
          duration={3000}
          // autoplay={false}
          style={{ position: 'relative' }}
          prevArrow={<div style={{ position: 'absolute', left: 20 }}>
            <img
              className="md:w-14 md:h-14 w-10 h-10 cursor-pointer "
              src="../img/left-qa.svg"
              alt="right" />
          </div>}
          nextArrow={<div style={{ position: 'absolute', right: 20 }}>
            <img className="md:w-14 md:h-14 w-10 h-10 cursor-pointer " src="../img/right-qa.svg" alt="right" />
          </div>}
        >
          {
            props.banner.map((item, index) => {
              if (item.slideTrangChu.link !== null) {
                return (
                  <a href={item.slideTrangChu.link} key={index + 'banner'}>
                    <img className={`w-full object-cover h-60 md:h-full pointer-events-none`} src={item.slideTrangChu.slideHome.img.sourceUrl} alt="Lenin" />
                  </a>
                )
              }
              return (
                <img className={`w-full object-cover h-60 md:h-full pointer-events-none`} src={item.slideTrangChu.slideHome.img.sourceUrl} alt="Lenin" key={index + 'banner'} />
                // <img className={`w-full object-cover h-60 md:h-full pointer-events-none`} src={'img/bg-test.jpg'} alt="Lenin" key={index + 'banner'} />
              )
            })
          }
        </Slide>
      </div>
      <div className="pt-14">
        <div>
          <h1 className="font-bold text-lg leading-snug px-4 container-fix">
            NEWEST ARRIVALS
          </h1>
          <ListProductHome data={props.data} />
        </div>

        <Link href="/products">
          <a>
            <div className="pt-12 flex justify-center items-center">
              <div className="uppercase mr-4 text-sm font-semibold">{t('see_more_product')}</div>
              <img src="../img/arrowlongright.svg" alt="arrow" />
            </div>
          </a>
        </Link>
      </div>

      {props?.data2.length > 0 &&
        <div className=" pt-24">
          <div>
            <h1 className="font-bold text-lg leading-snug px-4 container-fix ">
              BEST SELLERS
            </h1>
            <ListProductHome data={props.data2} />
          </div>

          <Link href="/products">
            <a>
              <div className="pt-12 flex justify-center items-center">
                <div className="uppercase mr-4 text-sm font-semibold">{t('see_more_product')}</div>
                <img src="../img/arrowlongright.svg" alt="arrow" />
              </div>
            </a>
          </Link>
        </div>
      }

      {/* <div className="pt-14">
        <ListHotNewsHome data={props.data2} />
      </div> */}

      {/* md:mt-16 mt-4 */}
      <div className="">
        <ListQA data={props.data3} />
      </div>
      <div className="md:mt-16 mt-4">
        <Footer footer={props.footer} />
      </div>
    </div >

  )
}

export async function getStaticProps({ params }) {
  // const res = await actGetListProduct('', 1, 8, null)
  // const data = await res;
  const res = await getProduct()
  const data = await res;

  const res2 = await getProductBestSeller()
  const data2 = await res2;

  // const res3 = await actGetListAcf(5, 1, null, 'proxy')
  const res3 = await getLogoClient()
  const data3 = await res3;

  const res4 = await getBanner()
  const data4 = await res4;
  const resLogo = await getLogoHome()
  const resCategory = await getProductCategories()
  const dataLogo = await resLogo;
  const dataCate = await resCategory;

  const resFooter = await getFooter()
  const dataFooter = await resFooter;
  // if (!data2) {
  //   return {
  //     notFound: true,
  //   }
  // }

  return {
    props: {
      data: data.data.data.products?.nodes,
      data2: data2.data.data.products?.nodes,
      data3: data3.data.data.logoClient.clientLogo.logoClient,
      banner: data4.data.data.slidesTrangChu.nodes,
      logo: dataLogo.data.data.getHeader.siteLogoUrl,
      cate: dataCate.data.data.productCategories.nodes,
      footer: dataFooter?.data?.data
    }, // will be passed to the page component as props
    revalidate: 1
  }
}

export default withTranslation('common')(Home);
