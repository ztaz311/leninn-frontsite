import Head from 'next/head'
import React from 'react'
import { Footer } from '../../components/Aside/Footer'
import { Header } from '../../components/Aside/Header'
import { ListProduct } from '../../components/Content/ListProduct'
import { getProductSale } from '../../constants/querry'

export default function Home(props) {


    return (
        <div id="div4">
            <div className="md:h-17 h-12" >
                <Header pageClass={true} />
                <Head>
                    <title>Leninn | SALES</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div >
                {props?.data2?.length > 0 &&
                    <ListProduct dataAvId={''} dataPr={props?.data2} pageInfo={props?.pageInfo} />
                }
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>

    )
}
export async function getStaticProps({ params }) {

    const res2 = await getProductSale(10000)
    const data2 = await res2;

    return {
        props: { data2: data2.data.data.products?.nodes, pageInfo: data2.data.data.products?.pageInfo }, // will be passed to the page component as props
        revalidate: 1
    }
}