import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { actGetPost } from "../../action/post"
import { Footer } from '../../components/Aside/Footer'
import { Header } from '../../components/Aside/Header'
import { DetailBlog } from '../../components/Content/DetailBlog'
import { actGetListPost } from "../../action/post"
import Head from 'next/head'

export default function Home(props) {
    const router = useRouter()
    const slug = router.query.slug || []

    const [itemPost, setItemPost] = useState(props.data)

    useEffect(() => {
        if (props.data === undefined) {
            router.replace('/404')
        } else {
            setItemPost(props.data)
        }
    }, [slug])

    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
                <Head>
                    <title>Leninn | Post</title>
                    <link rel="shortcut icon" href="../img/favicon.png" />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div>
                {itemPost && <DetailBlog data={itemPost.data} />}
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>
    )
}


export async function getStaticPaths() {
    const res = await actGetListPost(10, 1, null, 'proxy')
    const data = await res;
    const paths = data.data.map(data => ({
        params: { slug: data.slug },
    }));
    return {
        paths,
        fallback: true
    }
}
export async function getStaticProps({ params }) {
    const res = await actGetPost(params.slug, null, 'proxy')
    const data = await res;
    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: { data: data }, // will be passed to the page component as props
        revalidate: 1
    }
}