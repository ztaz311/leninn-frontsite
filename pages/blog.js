import Head from 'next/head'
import React from 'react'
import { actGetListPost } from "../action/post"
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { ListBlog } from '../components/Content/ListBlog'

export default function Home(props) {
    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
                <Head>
                    <title>Leninn | Blog</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div>
                <ListBlog data={props.data} />
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>

    )
}
export async function getStaticProps({ params }) {
    const res = await actGetListPost(6, 1, null, 'proxy')
    const data = await res;
    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: { data: data }, // will be passed to the page component as props
        revalidate: 1

    }
}