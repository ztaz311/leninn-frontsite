import { useRouter } from 'next/router'
import React from 'react'
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { Complete } from "../components/Cart/Complete"
import { Menu } from "../components/Cart/Menu"
import { getProductCategories, getLogoHome, getFooter } from '../constants/querry'

export default function Home(props) {
    const router = useRouter()

    // console.log('route', router.query?.numberOder)

    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} logo={props.logo} cate={props.cate} />
            </div>
            <div>
                <Menu name='complete' />
                <Complete numberOder={router.query?.numberOder} />
            </div>
            <div className="md:mt-16 mt-4">
                <Footer footer={props.footer} />
            </div>
        </div>
    )
}
export async function getStaticProps() {
    const res = await getProductCategories()
    const data = await res;
    const resLogo = await getLogoHome()
    const dataLogo = await resLogo;
    const resFooter = await getFooter()
    const dataFooter = await resFooter;
    return {
        props: {
            logo: dataLogo.data.data.getHeader.siteLogoUrl,
            cate: data.data.data.productCategories.nodes,
            footer: dataFooter?.data?.data
        },
        revalidate: 1
    }
}