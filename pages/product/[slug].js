import { NextSeo } from 'next-seo'
import Head from 'next/head'
import React from 'react'
import { Footer } from '../../components/Aside/Footer'
import { Header } from '../../components/Aside/Header'
import { DetailProduct } from '../../components/Content/DetailProduct'
// import useSWR from 'swr'
import { getDetailProduct, getProduct, getLogoHome, getProductCategories, getFooter } from '../../constants/querry'

export default function Home(props) {
    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} logo={props.logo} cate={props.cate} />
                <Head>
                    <title>Leninn | Product</title>
                    <link rel="shortcut icon" href="../img/favicon.png" />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
                <NextSeo
                    title={`${props?.data?.name || 'products'} | Leninn`}
                    description={props?.data?.description?.replace(/<[^>]*>?/gm, '') || ''}
                    additionalMetaTags={[
                        {
                            property: 'og:title',
                            content: props?.data?.name || ''
                        },
                        {
                            property: 'og:image',
                            content: props?.data?.image.sourceUrl || ''
                        },
                    ]}
                />
            </div>
            <div>
                {/* {loading && <Loading />} */}
                {props?.data && <DetailProduct data={props?.data} />}
            </div>
            <div className="md:mt-16 mt-4">
                <Footer footer={props.footer} />
            </div>
        </div>

    )
}

export async function getStaticPaths() {
    const res = await getProduct(5000)
    const data = await res;
    const paths = data.data.data.products.nodes.map(data => ({
        params: { slug: data.slug },
    }));
    return {
        paths,
        fallback: true
    }
}
// getStaticProps
// getServerSideProps
export async function getStaticProps({ params }) {
    const res = await getDetailProduct(params.slug)
    const data = await res;
    const resLogo = await getLogoHome()
    const resCategory = await getProductCategories()
    const dataLogo = await resLogo;
    const dataCate = await resCategory;
    const resFooter = await getFooter()
    const dataFooter = await resFooter;
    if (data.data.data.products.nodes.length === 0) {
        return {
            redirect: {
                destination: '/404',
                permanent: false,
            },
        }
    }


    return {
        props: {
            data: data.data.data.products.nodes[0],
            logo: dataLogo.data.data.getHeader.siteLogoUrl,
            cate: dataCate.data.data.productCategories.nodes,
            footer: dataFooter?.data?.data

            // , category: slug, validationId: data3?.data || '' 
        }, // will be passed to the page component as props
        revalidate: 1
    }
}


