import Head from 'next/head'
import React, { useState, useEffect } from 'react'
import styles from '../styles/Home.module.css'
import { Header } from '../components/Aside/Header'
import { ListProductHome } from '../components/Content/ListProductHome'
import { ListHotNewsHome } from '../components/Content/ListHotNewsHome'
import { ListGallery } from '../components/Content/ListGallery'
import { Footer } from '../components/Aside/Footer'
import { actGetListGallery } from "../action/gallery";
import { listAlbum } from '../constants/querry'

export default function Home(props) {

    return (
        <div className="">
            <div className="h-0">
                <Header pageClass={true} />
                <Head>
                    <title>Leninn | Gallery</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div>
                <ListGallery data={props.data} />
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>

    )
}

export async function getStaticProps({ params }) {
    const res = await listAlbum()
    const data = await res;
    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: { data: data.data.data.theGalleries.edges }, // will be passed to the page component as props
        revalidate: 1
    }
}
