import React, { useEffect, useState } from 'react'
import { Footer } from '../components/Aside/Footer'
import { Header } from '../components/Aside/Header'
import { Checkout } from '../components/Cart/Checkout'
import { getProductCategories, getLogoHome, getFooter } from '../constants/querry'

export default function Home(props) {


    const [cart, setCart] = useState('')
    const [price, setPrice] = useState('')
    useEffect(() => {
        var cart2 = localStorage.getItem('cart')
        if (cart2 !== undefined && cart2 !== null) {
            setCart(JSON.parse(cart2))
        }
        var price2 = localStorage.getItem('priceCheckout')
        if (price2 !== undefined && price2 !== null) {
            setPrice(JSON.parse(price2))
        }
    }, [])
    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} logo={props.logo} cate={props.cate} />
            </div>
            <div>
                {
                    price !== '' &&
                    <Checkout cart={cart} price={price} />
                }

            </div>
            <div className="md:mt-16 mt-4">
                <Footer footer={props.footer} />
            </div>
        </div>
    )
}
export async function getStaticProps() {
    const res = await getProductCategories()
    const data = await res;
    const resLogo = await getLogoHome()
    const dataLogo = await resLogo;
    const resFooter = await getFooter()
    const dataFooter = await resFooter;
    return {
        props: {
            logo: dataLogo.data.data.getHeader.siteLogoUrl,
            cate: data.data.data.productCategories.nodes,
            footer: dataFooter?.data?.data
        },
        revalidate: 1
    }
}