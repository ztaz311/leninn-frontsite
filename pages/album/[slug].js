import Head from 'next/head'
import React, { useState, useEffect } from 'react'
import { Header } from '../../components/Aside/Header'
import { ListAlbum } from '../../components/Content/ListAlbum'
import { Footer } from '../../components/Aside/Footer'
import { useRouter } from 'next/router'
import { actGetAlbum } from "../../action/gallery";
import { Loading } from "../../components/Aside/Loading";
import { actGetListGallery } from "../../action/gallery";
import { useKeenSlider } from "keen-slider/react"
// "postbuild": "next-sitemap",
import { listAlbum } from '../../constants/querry'
export default function Home(props) {

    // useEffect(async () => {
    //     // const resNext = await listAlbum("", "", 'YXJyYXljb25uZWN0aW9uOjIwMDk=', 1)
    //     // const dataNext = await resNext;
    //     // console.log('123', dataNext);

    //     // const res = await listAlbum('fw20-destruction-rebirth-lookbook')
    //     // const data = await res;
    //     // console.log('path', data);
    // }, [])

    // console.log('props', props);
    const router = useRouter()
    const slug = router.query.slug || []
    const [itemAlbum, setItemAlbum] = useState(props.data)

    const [currentSlide, setCurrentSlide] = useState(0)
    const [sliderRef, slider] = useKeenSlider({
        loop: false,
        // resetSlide: true,
        slideChanged(s) {
            setCurrentSlide(s.details().relativeSlide)
        },
    })
    useEffect(() => {
        if (props.data === undefined) {
            router.replace('/404')
        } else {
            setItemAlbum(props.data)
        }
    }, [slug, sliderRef])


    // console.log('props', props)

    return (
        <div className="">
            <div className="md:h-17 h-12">
                <Header pageClass={true} />
                <Head>
                    <title>Leninn | Album</title>
                    <link rel="shortcut icon" href="../img/favicon.png" />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
            </div>
            <div>
                {/* {loading && <Loading />} */}
                {itemAlbum && <ListAlbum data={itemAlbum}
                    next={props.next} prev={props.prev}
                    sliderRef={sliderRef} currentSlide={currentSlide} slider={slider} slug={slug}
                />}
            </div>
            <div className="md:mt-16 mt-4">
                <Footer />
            </div>
        </div>
    )
}

export async function getStaticPaths() {
    const res = await listAlbum()
    const data = await res;
    const paths = data.data.data.theGalleries.edges.map(data => ({
        params: { slug: data.node.slug },
    }));
    return {
        paths,
        fallback: true
    }
}

export async function getStaticProps({ params }) {
    const res = await listAlbum(params.slug)
    const data = await res;


    const resNext = await listAlbum("", "", data.data.data.theGalleries.edges[0].cursor, 1)
    const dataNext = await resNext;

    const resPrev = await listAlbum("", data.data.data.theGalleries.edges[0].cursor, "", 1)
    const dataPrev = await resPrev;

    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: { data: data.data.data.theGalleries.edges, next: dataNext.data.data.theGalleries.edges, prev: dataPrev.data.data.theGalleries.edges }, // will be passed to the page component as props
        revalidate: 1
    }
}