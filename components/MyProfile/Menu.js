import React, { memo } from 'react';
import Link from "next/link";
import { UIStore } from "../Model/UIStore";
import { i18n, withTranslation } from '../../i18n'
import { useSession, signIn, signOut } from 'next-auth/client'
import { useRouter } from 'next/router'
export const Menu = withTranslation('common')(memo((props) => {
    const { t } = props
    const [session] = useSession();
    const router = useRouter()

    const logout = async () => {
        await UIStore.update(s => { s.auth = { token: '' }; s.profile = {} });
        if (session) {
            await signOut()
        }
        document.cookie = await "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

        setTimeout(() => {
            router.replace('/')
        }, 300);
    }

    return (
        <div>
            <div className="md:px-4 py-3 md:pl-4 md:pr-8 md:border-r-2 md:border-gray-200">
                <div className="border-b-1 border-gray-300 py-3">
                    <h1 className="uppercase text-xl font-semibold px-3">My Account</h1>
                </div>
                <div className="mt-4">
                    <div className={`flex py-2 ${props.dataActive == 'myprofile' ? 'bg-gray-200' : ''}`}>
                        <Link href="/myprofile" >
                            <a className="text-sm font-semibold px-3 w-full">
                                {t('account_detail')}

                            </a>
                        </Link>
                    </div>
                    <div className={`flex py-2 ${props.dataActive == 'orders' ? 'bg-gray-200' : ''}`}>
                        <Link href="/orders">
                            <a className="text-sm font-semibold px-3 w-full">
                                {t('order')}
                            </a>
                        </Link>
                    </div>
                    <div className={`flex py-2 ${props.dataActive == 'address' ? 'bg-gray-200' : ''}`}>
                        <Link href="/address" >
                            <a className="text-sm font-semibold px-3 w-full">
                                {t('address')}
                            </a>
                        </Link>
                    </div>
                    <div className="flex py-2 cursor-pointer">
                        <div onClick={() => logout()} className="text-sm font-semibold px-3 w-full" >
                            {t('logout')}
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
}));
