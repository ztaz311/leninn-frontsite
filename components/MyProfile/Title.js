import React, { memo } from 'react';
import { i18n, withTranslation } from '../../i18n'

export const Title = withTranslation('common')(memo((props) => {
    const { t } = props
    return (
        <div className=" bg-black text-white font-semibold ">
            <div className="flex justify-center items-center h-20">
                <h1 className=" text-2xl"> {t('my_account')}</h1>
            </div>
        </div>
    );
}));
