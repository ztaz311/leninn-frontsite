import Link from "next/link";
import React, { memo } from 'react';
import { withTranslation } from '../../i18n';
import { formatDate, formatPrice } from '../Conver/unit';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Loading } from "../../components/Aside/Loading"
import { UIStore } from '../Model/UIStore'
export const Order = withTranslation('common')(memo((props) => {
    const { t } = props


    return (
        <div>
            <InfiniteScroll
                // blockScroll={true}
                dataLength={props.orders.length} //This is important field to render the next data
                next={props.hanleSeeMore}
                hasMore={props.loading2}
                loader={<div className="text-center mr-0 md:mr-48 lg:mr-72">
                    <Loading />
                </div>}
                endMessage={
                    <p className="text-center mr-0 md:mr-48 lg:mr-72 mt-3">
                        <b>{props.t('see_all')}</b>
                    </p>
                }
            >
                <table className="shop_table_responsive">
                    <thead className="hidden-sm">
                        <tr className="border-b-1 border-gray-300 block md:table-row">
                            <th className="md:w-2/12 py-3 text-center px-3 uppercase">{t('order')}</th>
                            <th className="md:w-3/12 py-3 text-left uppercase">{t('date')}</th>
                            <th className="md:w-2/12 py-3 text-center uppercase">{t('status')}</th>
                            <th className="md:w-3/12 py-3 text-center uppercase">{t('total')}</th>
                            <th className="md:w-2/12 py-3 text-center uppercase">{t('action')}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            props.orders.map((item, index) => {
                                // console.log('11', item);
                                return (
                                    <tr className="border-b-1 border-gray-300 " key={index}>
                                        <td className="py-4 md:text-center text-right md:table-cell block" data-title={t('order')}>#{item.databaseId}</td>
                                        <td className="py-4 text-sm font-semibold md:text-left text-right md:table-cell block" data-title={t('date')}>{formatDate(item.date)}</td>
                                        <td className="py-4 md:text-center text-right md:table-cell block text-xs" data-title={t('status')}>{item.status.charAt(0).toUpperCase() + item.status.slice(1)}</td>
                                        <td className="py-4 md:text-center text-right md:table-cell block text-sm" data-title={t('total')}>{item.total} </td>
                                        <td className="py-4 md:text-center text-right md:table-cell block text-sm" data-title={t('action')}>
                                            <Link href={`/vieworder/${item.databaseId}`}>
                                                <a className="px-3 py-2 bg-black text-white font-semibold" onClick={() => UIStore.update(s => { s.order = item })}>
                                                    {t('view')}
                                                </a>
                                            </Link>
                                        </td>
                                    </tr>
                                )
                            })
                        }

                    </tbody>
                </table>
            </InfiniteScroll>


        </div>
    );
}));
