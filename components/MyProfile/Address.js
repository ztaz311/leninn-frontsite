import React, { memo, useState, useEffect } from 'react';
import { i18n, withTranslation } from '../../i18n'

export const Address = withTranslation('common')(memo((props) => {
    const [basic, setBasic] = useState(true)
    const [passowdChange, setPassowdChange] = useState(false)

    const handleInfoBasic = (props) => {
        setBasic(!basic)
    }

    const [data, setData] = useState({
        fullname: '',
        email: '',
        phone: '',
        city: '',
        address: ''
    })
    useEffect(() => {
        setData({
            fullname: props.profile?.firstName,
            email: props.profile.email,
            phone: props.profile?.phone,
            city: props.profile?.city,
            address: props.profile?.address1
        })

        return () => {
            setData({
                fullname: '',
                email: '',
                phone: '',
                city: '',
                address: ''
            })
        }
    }, [props.profile])
    const { t } = props
    return (
        <div className="px-4 py-4">
            <div>
                <div>
                    <label className="text-xl font-semibold mr-1 uppercase">{t('shipping_address')}</label>
                    <button className="text-xs font-semibold hidden" onClick={(e) => handleInfoBasic(e)}>(Edit)</button>
                </div>
                {
                    !basic &&
                    <div className="px-4">
                        <div className="mt-2">
                            <label className="text-sm font-semibold">
                                {t('full_name')}:
                        </label>
                            <p>{props.profile.shipping.first_name}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                Email:
                        </label>
                            <p>{props.profile.email}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                {t('phone')}::
                        </label>
                            <p>{props.profile.billing.phone}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                {t('province_city')}:

                        </label>
                            <p>{props.profile.shipping.city}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                {t('address')}:
                        </label>
                            <p>{props.profile.shipping.address_1}</p>
                        </div>
                    </div>
                }
                {
                    basic &&
                    <div className="py-4">
                        <div className="mt-1 md:px-4">
                            <div>
                                <label className="w-full block font-semibold">{t('full_name')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" value={data.fullname} placeholder="Full name" onChange={(e) => setData({ ...data, fullname: e.target.value })} />
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">Email</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" value={data.email} placeholder="Email" onChange={(e) => setData({ ...data, email: e.target.value })} />
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">{t('phone')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" value={data.phone} placeholder="Phone" onChange={(e) => setData({ ...data, phone: e.target.value })} />
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">{t('province_city')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" value={data.city} placeholder={t('province_city')} onChange={(e) => setData({ ...data, city: e.target.value })} />
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">{t('address')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" value={data.address} placeholder={t('address')} onChange={(e) => setData({ ...data, address: e.target.value })} />
                            </div>

                        </div>
                        <div className="mt-6 md:px-4 flex md:justify-start justify-center">
                            <button className="font-semibold text-white border-2 border-black bg-black px-5 py-2 mr-3" onClick={() => props.updateCustomer(data, handleInfoBasic)}>{t('update_info')}</button>
                            {/* <button className="font-semibold  px-5 py-2 border-2 border-black" onClick={(e) => handleInfoBasic(e)}>{t('cancel')}</button> */}
                        </div>
                    </div>

                }

            </div>
        </div>
    );
}));
