import React, { memo, useState, useEffect } from 'react';
import { i18n, withTranslation } from '../../i18n'
import { useToasts } from 'react-toast-notifications'
import { updatePassword, qrLogin } from '../../constants/querry'
import { UIStore } from '../Model/UIStore';
export const MyProfile = withTranslation('common')(memo((props) => {
    const { addToast } = useToasts();
    const [basic, setBasic] = useState(true)
    const [passowdChange, setPassowdChange] = useState(false)
    const handleChangePass = () => {
        setPassowdChange(!passowdChange)
    }
    const handleInfoBasic = () => {
        setBasic(!basic)
    }

    const { data } = props
    const [dataChange, setDataChange] = useState({
        fullname: '',
        phone: '',
        email: '',
    })
    const [dataPass, setDataPass] = useState({
        password: '',
        newPassword: '',
        checkNewPassword: '',
    })
    const [errPass, setErrPass] = useState({
        password: '',
        newPassword: '',
        checkNewPassword: '',
    })


    useEffect(() => {
        setDataChange({
            fullname: data.firstName !== '' ? data.firstName : data?.billing?.firstName,
            phone: data?.billing?.phone,
            email: data.email
        })
        return () => {
            setDataChange({ fullname: '', phone: '', email: '' })
        }
    }, [data])


    const onChangePass = () => {
        let password, newPassword, checkNewPassword = ''

        if (dataPass.password === '') {
            password = props.t('password_required')
        } else {
            password = ''
        }
        if (dataPass.newPassword === '') {
            newPassword = props.t('password_required')
        } else if (dataPass.newPassword.length < 6) {
            newPassword = props.t('password_must')
        } else {
            newPassword = ''
        }

        if (dataPass.checkNewPassword === '') {
            checkNewPassword = props.t('confirm_password_required')
        } else if (dataPass.checkNewPassword !== dataPass.newPassword) {
            checkNewPassword = props.t('compare_password')
        } else {
            checkNewPassword = ''
        }

        setErrPass({
            password, newPassword, checkNewPassword
        })
        if (password === '' && newPassword === '' && checkNewPassword === '') {
            qrLogin(props.profile.email, dataPass.password).then(res => {
                if (res?.data?.data?.login?.authToken !== undefined) {
                    let xx = { ...props.auth }
                    xx.authToken = res?.data?.data?.login?.authToken
                    UIStore.update(s => { s.auth = xx })
                    updatePassword(props.auth.user.id, dataPass.newPassword, res?.data?.data?.login?.authToken).then(response => {
                        if (response?.data?.data?.updateUser?.user?.id !== undefined) {
                            addToast(props.t('password_success'), { appearance: 'success' });
                        }
                    })
                } else {
                    addToast(props.t('password_worng'), { appearance: 'warning' });
                }
            })
        }
    }

    const { t } = props

    return (
        <div className="md:px-4 py-4">
            <div>
                <div>
                    <label className="text-xl font-semibold mr-1 uppercase">{t('info_basic')}</label>
                    <button className="text-xs font-semibold hidden" onClick={(e) => handleInfoBasic(e)}>({t('edit')})</button>
                </div>
                {!basic &&
                    <div className="px-4">
                        <div className="mt-2">
                            <label className="text-sm font-semibold">
                                {t('full_name')}:
                        </label>
                            <p>{data.first_name !== '' ? data.first_name : data.username}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                Email:
                        </label>
                            <p>{data.email}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                {t('phone')}:
                        </label>
                            <p>{data?.billing?.phone}</p>
                        </div>
                    </div>
                }
                {
                    basic &&
                    <div className="py-4">
                        <div className="mt-1 px-4">
                            <div>
                                <label className="w-full block font-semibold">{t('full_name')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" value={dataChange.fullname} placeholder={t('full_name')} onChange={(e) => setDataChange({ ...dataChange, fullname: e.target.value })} />
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">Email</label>
                                <input
                                    // className={`px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full ${dataChange.email.substring(0, 3) !== 'FB_' && 'bg-gray-200'}`}
                                    className={`px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full bg-gray-200`}
                                    readOnly={dataChange.email !== ''}
                                    value={dataChange.email}
                                    placeholder="Email"
                                    onChange={(e) => setDataChange({ ...dataChange, email: e.target.value })}
                                />
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">{t('phone')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" value={dataChange.phone} placeholder={t('phone')} onChange={(e) => setDataChange({ ...dataChange, phone: e.target.value })} />
                            </div>
                        </div>
                        <div className="mt-6 flex md:justify-start justify-center">
                            <button className="font-semibold text-white border-2 border-black bg-black px-5 py-2 mr-3" onClick={() => props.updateCustomer(dataChange.fullname, dataChange.phone, dataChange.email, handleInfoBasic)}>{t('update_info')}</button>
                            {/* <button className="font-semibold  px-5 py-2 border-2 border-black" onClick={(e) => handleInfoBasic(e)}>{t('cancel')}</button> */}
                        </div>
                    </div>

                }

            </div>
            <div className="">
                <button className="text-xl font-semibold uppercase setting-list dropdown-toggle relative" onClick={(e) => handleChangePass(e)}>{t('change_password')}</button>
                {
                    passowdChange &&
                    <div className="py-4">
                        <div className="mt-1 px-4">
                            <div>
                                <label className="w-full block font-semibold">{t('password_old')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" placeholder={t('password_old')} type="password" value={dataPass.password} onChange={(e) => setDataPass({ ...dataPass, password: e.target.value })} />
                                <div className="text-md text-red-400">{errPass.password}</div>
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">{t('password_new')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" placeholder={t('password_new')} type="password" value={dataPass.newPassword} onChange={(e) => setDataPass({ ...dataPass, newPassword: e.target.value })} />
                                <div className="text-md text-red-400">{errPass.newPassword}</div>
                            </div>
                            <div className="mt-3">
                                <label className="w-full block font-semibold">{t('confirm_password')}</label>
                                <input className="px-3 py-2 border-2 border-gray-200 mt-2 max-w-sm w-full" placeholder={t('confirm_password')} type="password" value={dataPass.checkNewPassword} onChange={(e) => setDataPass({ ...dataPass, checkNewPassword: e.target.value })} />
                                <div className="text-md text-red-400">{errPass.checkNewPassword}</div>
                            </div>

                        </div>
                        <div className="mt-6 md:px-4 flex md:justify-start justify-center">
                            <button className="font-semibold text-white bg-black px-5 py-2 mr-3 border-2 border-black" onClick={onChangePass}>{t('update_password')}</button>
                            {/* <button className="font-semibold  px-5 py-2 border-2 border-black" onClick={(e) => handleChangePass(e)}>{t('cancel')}</button> */}
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}));
