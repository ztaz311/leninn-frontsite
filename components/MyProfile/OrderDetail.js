import React, { memo } from 'react';
import { withTranslation } from '../../i18n';
import { formatPrice } from '../Conver/unit';
export const OrderDetail = withTranslation('common')(memo((props) => {
    const { t } = props
    return (
        <div>
            <div className=" py-6 px-8">
                <div className="py-3 font-semibold text-xl  mb-2 uppercase">{t('order_detail')} </div>
                <div className="px-6 py-2 ">
                    <div>
                        <div className="flex justify-between border-b-1 border-gray-200 py-4">
                            <label className="font-semibold uppercase">{t('profuct')}</label>
                            <label className="font-semibold uppercase">{t('subtotal')}</label>
                        </div>
                        {
                            props.data.lineItems.edges.map((item, index) => {
                                return (
                                    <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm" key={index}>
                                        <label className="">{item.node.variation.name}</label>
                                        <span>{formatPrice(item.node.total)}</span>
                                    </div>
                                )
                            })
                        }

                    </div>
                    <div>
                        {/* <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                            <label className="font-semibold">Tổng phụ</label>
                            <span className="font-semibold">{props.data.total} {props.data.currency}</span>
                        </div>
                        <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                            <label className="font-semibold">Mã khuyến mại (CODEGIFT)</label>
                            <span className="font-semibold">$2,264.00</span>
                        </div> */}
                        <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                            <label className="font-semibold">{t('shippingTotal')}</label>
                            <span className="font-semibold">{formatPrice(props.data.shippingLines.nodes[0].total)}</span>
                        </div>
                        <div className="flex justify-between py-4 items-center border-b-1 border-gray-200">
                            <label className="font-semibold text-xl">{t('total')}</label>
                            <span className="font-semibold text-xl">{props.data.total}</span>
                        </div>
                    </div>
                </div>
                <div className="py-2 mt-6">
                    <div>
                        <label className="text-xl font-semibold mr-1 uppercase">{t('info_basic')}</label>
                    </div>
                    <div className="px-6">
                        <div className="mt-2">
                            <label className="text-sm font-semibold">
                                {t('full_name')}:
                        </label>
                            <p>{props.data.billing?.firstName !== '' ? props.data.billing?.firstName : props.data.shipping?.firstName}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                Email:
                        </label>
                            <p>{props.data.billing.email}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                {t('phone')}:
                        </label>
                            <p>{props.data.billing.phone}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                {t('province_city')}:
                        </label>
                            <p>{props.data.billing.city}</p>
                        </div>
                        <div>
                            <label className="text-sm font-semibold">
                                {t('address')}:
                        </label>
                            <p>{props.data.billing.address1}</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    );
}));
