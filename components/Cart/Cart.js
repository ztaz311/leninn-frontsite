import { useRouter } from 'next/router';
import React, { memo, useEffect, useState } from 'react';
import { withTranslation } from "../../i18n";
import { ItemCart } from "../Cart/ItemCart";
import { Menu } from "../Cart/Menu";
import { UIStore } from "../Model/UIStore";
import { formatPrice } from '../Conver/unit'
import { useToasts } from 'react-toast-notifications'
import { actGetCoupons } from '../../action/product'
import { checkCart } from '../../constants/querry'
import { addToCart, getCart, clearCart, checkout } from '../../constants/querry'
import { LoadingAll } from '../Aside/LoadingAll';

export const Cart = withTranslation('common')(memo((props) => {
    const { addToast } = useToasts();
    const router = useRouter()
    const [loading, setLoading] = useState(true)
    const [sale, setSale] = useState(false)
    const cart = UIStore.useState(s => s.cart);
    const [cartLocal, setCartLocal] = useState([])
    const [price, setPrice] = useState({
        totalFetch: 0,
    })
    const [priceSV, setPriceSV] = useState(null)

    useEffect(() => {

        let totalFetch = 0
        cart?.data?.forEach(element => {

            totalFetch += (parseFloat(element.price || 0) * parseFloat(element.quantily))
        });
        setPrice({ ...price, totalFetch })

    }, [cart])

    useEffect(() => {
        setCartLocal(cart.data)
    }, [cart])

    const [data, setData] = useState({
        maKM: 0,
        vat: 0
    })

    const handleClickSub = async (index) => {

        var cartFetch = await [...cartLocal]
        if (cartFetch[index].quantily > 1) {
            await setLoading(true)

            cartFetch[index] = await { ...cartFetch[index], quantily: cartFetch[index].quantily - 1 }
            await setCartLocal(cartFetch)
            await updateCart2(cartFetch)
        }

    }

    const handleClickPlus = async (index) => {
        await setLoading(true)
        var cartFetch = await [...cartLocal]
        cartFetch[index] = await { ...cartFetch[index], quantily: cartFetch[index].quantily + 1 }
        await setCartLocal(cartFetch)
        await updateCart2(cartFetch)


    }

    const removeCart = async (index) => {
        await setLoading(true)
        let dataFetch = await [...cartLocal]
        await dataFetch.splice(index, 1);
        await setCartLocal(dataFetch)

        await updateCart2(dataFetch)
    }
    const updateCart2 = (data) => {
        let cart = {
            total: data.length,
            data: data
        }
        localStorage.setItem('cart', JSON.stringify(cart))
        UIStore.update(s => { s.cart = cart });
    }
    const updateCart = () => {
        let cart = {
            total: cartLocal.length,
            data: cartLocal
        }
        localStorage.setItem('cart', JSON.stringify(cart))
        UIStore.update(s => { s.cart = cart });
    }


    const gotoCheckout = async () => {
        // UIStore.update(s => { s.price = { priceVAT: priceVAT, totalFetch: price.totalFetch, maKM: formatPrice(data.maKM), vat: data.vat, totalAll: formatPrice(price.totalFetch + priceVAT - data.maKM) } });
        const cc = await haha()
        if (!cc.status) {
            if (cart.total > 0) {
                var priceLocal = { fullPriceSV: priceSV, priceVAT: priceVAT, totalFetch: price.totalFetch, maKM: formatPrice(data.maKM), vat: data.vat, totalAll: formatPrice(price.totalFetch + priceVAT - data.maKM) }
                await localStorage.setItem('priceCheckout', JSON.stringify(priceLocal))
                await router.push('/checkout')
            } else {
                addToast(props.t('cart_null'), { appearance: 'warning' });
                router.push('/products')
            }

        } else {
            addToast(props.t('check_cart'), { appearance: 'warning', autoDismiss: false });
            setCartLocal(cc.data)
            updateCart2(cc.data)
            window.scroll({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }

    }
    const { t } = props
    // console.log('loading', loading);
    // console.log('cartLocal', cartLocal);
    useEffect(async () => {
        if (cartLocal.length > 0) {
            await setLoading(true)
            await clearCart(localStorage.getItem('woocommerce-session') || null).then(async (data) => {
                if (data?.headers['woocommerce-session']) {
                    await localStorage.setItem('woocommerce-session', data.headers['woocommerce-session'])
                }
            })
            for (let [index, element] of cartLocal.entries()) {
                await addToCart(element.validationId, element.quantily, localStorage.getItem('woocommerce-session') || null).then(async (data) => {
                    if (data?.headers['woocommerce-session']) {
                        await localStorage.setItem('woocommerce-session', data.headers['woocommerce-session'])
                    }
                }).catch(e => console.log('1', e))
                if (index === cartLocal.length - 1) {
                    await getCart(localStorage.getItem('woocommerce-session') || null).then(async (data) => {
                        if (data?.headers['woocommerce-session']) {
                            await localStorage.setItem('woocommerce-session', data.headers['woocommerce-session'])
                        }
                        if (data.data.data.cart.subtotal) {
                            setPriceSV(data.data.data.cart)
                        }
                        // await setTimeout(() => {
                        setLoading(false)
                        // }, 1000);
                    }).catch(e => console.log('1', e))
                }
            }
        } else {
            await setLoading(false)
            await setPriceSV({
                total: '0₫',
                shippingTotal: "0₫",
                subtotal: "0₫"
            })
        }

    }, [cartLocal])




    const priceVAT = ((data.vat * price.totalFetch) / 100)
    const total = formatPrice(price.totalFetch + priceVAT - data.maKM)
    const totalNoCurency = price.totalFetch + priceVAT - data.maKM





    const formatCurencyToNumber = (currency) => {
        // var currency = "528.000₫"; //it works for US-style currency strings as well
        var cur_re = /\D*(\d+|\d.*?\d)(?:\D+(\d{2}))?\D*$/;
        var parts = cur_re.exec(currency);
        var number = parseFloat(parts[1].replace(/\D/, '') + '.' + (parts[2] ? parts[2] : '00'));
        return number.toString()
    }


    const [cartOnline, setCartOnline] = useState([])
    const [checkChange, setCheckChange] = useState(false)


    let cardd = []
    const ff = (data) => {
        cardd = data
    }


    async function haha() {
        let cc = false
        if (cartLocal.length > 0) {
            for (let index = 0; index < cartLocal.length; index++) {
                await checkCart(cartLocal[index].validationId).then(async (res) => {
                    if (res.data.data.productVariation) {
                        let dataOnline = await res.data.data.productVariation,
                            dataOffline = await cartLocal[index]

                        if (formatCurencyToNumber(dataOnline.price) != formatCurencyToNumber(dataOffline.price) || dataOnline.stockStatus !== "IN_STOCK") {
                            setCheckChange(true)
                            cc = true
                            if (dataOnline.stockStatus !== "IN_STOCK") {

                            } else {
                                cardd.push(await { ...dataOffline, price: formatCurencyToNumber(dataOnline.price) })
                                ff(cardd)
                            }
                        } else {
                            cardd.push(await { ...dataOffline, price: formatCurencyToNumber(dataOnline.price) })
                            ff(cardd)
                        }
                    }
                })
            }
            setCartOnline(cardd)

        }
        return { status: cc, data: cardd }
    }


    useEffect(() => {
        haha()
    }, [cartLocal])


    // console.log('11', cartOnline)
    // console.log('2', checkChange)

    return (
        <>
            {loading && <LoadingAll />}
            <Menu name='cart' />
            <div className="container-fix w-full pt-6">
                <div className="px-4">
                    <div className="grid grid-cols-1 md:grid-cols-3 md:gap-4">
                        <div className="col-span-2 md:px-4 py-4">
                            <div>
                                <table className="table-fixed shop_table_responsive  woocommerce-cart-form__contents w-full">
                                    <thead className="">
                                        <tr className="border-b-1 border-gray-300 w-full">
                                            <th className="w-2/12 py-3 text-center px-3"></th>
                                            <th className="w-3/12 py-3 text-left">{t('product')}</th>
                                            <th className="w-2/12 py-3 text-center">{t('price')}</th>
                                            <th className="w-2/12 py-3 text-center">{t('quantily')}</th>
                                            <th className="w-2/12 py-3 text-center">{t('subtotal')}</th>
                                            <th className="w-1/12 py-3 text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            cartLocal.length === 0 && <tr className=""><td colSpan="6"><div className="mt-12"><h1 className="text-center"><b>{t('empty_cart')}</b></h1></div></td></tr>
                                        }
                                        {
                                            cartLocal.map((item, index) => {
                                                return (
                                                    <ItemCart key={index} data={item} handleClickSub={() => handleClickSub(index)} handleClickPlus={() => handleClickPlus(index)} removeCart={() => removeCart(index)} />
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                            {/* <div className="md:flex md:justify-between mt-6 block">
                                <div className="flex justify-between md:justify-start">
                                    <input className="border border-gray-300 md:py-3  px-3 md:max-w-xs mr-3" placeholder={t('promotion')} value={promotion} onChange={(e) => setPromotion(e.target.value)} />
                                    <button className="md:px-5 px-3 py-3 text-white font-semibold bg-gray-900" onClick={() => checkCoupons()}>{t('apply')}</button>
                                </div>
                               
                            </div> */}
                            {/* <div className="mt-4 md:mt-0 md:block flex justify-center">
                                    <button className="px-5 py-3 font-semibold bg-gray-100" onClick={updateCart}>{t('update_cart')}</button>
                                </div> */}
                        </div>
                        <div className="bg-red md:mb-0 mt-4">
                            <div className="p-6 border-gray-200 border-2">
                                <h2 className="font-semibold text-xl">{t('cart_totals')}</h2>
                                <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                                    <label className="font-semibold">{t('subtotal')}</label>
                                    <span>{priceSV?.subtotal ? priceSV.subtotal : formatPrice(price.totalFetch)}</span>
                                </div>
                                <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                                    <label className="font-semibold">{t('shippingTotal')}</label>
                                    <span>{priceSV?.shippingTotal ? priceSV.shippingTotal : '0₫'}</span>
                                </div>
                                {/* <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                                    <label className="font-semibold">{t('coupon_code')}</label>
                                    {data.maKM !== '' && !isNaN(data.maKM) && <span>-{formatPrice(data.maKM)}</span>}
                                </div> */}
                                {/* <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                                    <label className="font-semibold">VAT({data?.vat || 0}%)</label>
                                    {data.vat !== '' && <span>{formatPrice(priceVAT)}</span>}
                                </div> */}
                                <div className="flex justify-between py-4 items-center">
                                    <label className="font-semibold text-xl">{t('total')}</label>
                                    <span className="font-semibold text-xl">{priceSV?.total ? priceSV.total : total} </span>
                                </div>

                                <div className="mt-3">
                                    <button disabled={priceSV?.total ? priceSV.total === '0₫' ? true : false : total === '0₫' ? true : false}
                                        className="w-full text-center uppercase text-white bg-gray-900 text-sm font-semibold py-3" onClick={() => gotoCheckout()}>{t('proceed_to_checkout')}</button>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}));
