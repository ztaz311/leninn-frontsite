import React, { memo } from 'react';
import Link from 'next/link'
import { i18n, withTranslation } from "../../i18n";
export const Menu = withTranslation('common')(memo((props) => {
    const { t } = props
    return (
        <>
            <div className=" bg-black text-white font-semibold ">
                <div className="flex justify-center items-center h-20 text-xs md:text-base uppercase">

                    <div className={props.name !== 'cart' ? 'opacity-50 cursor-pointer	' : "cursor-pointer	"}>
                        {t('shopping_cart')}
                    </div>

                    <div className="md:mx-3 mx-2 opacity-50">
                        <img className="w-6" src="../img/arrowlongrightwhite.svg" />
                    </div>

                    <div className={props.name !== 'checkout' ? 'opacity-50 cursor-pointer	' : "cursor-pointer	"}>
                        {t('checkout')}
                    </div>

                    <div className="md:mx-3 mx-2 opacity-50">
                        <img className="w-6" src="../img/arrowlongrightwhite.svg" />
                    </div>

                    <div className={props.name !== 'complete' ? 'opacity-50 cursor-pointer' : 'cursor-pointer'}>
                        {t('complete')}
                    </div>

                </div>
            </div>
        </>
    );
}));
