import React, { memo, useState, useEffect } from 'react';
import Link from 'next/link'
import { Menu } from "../Cart/Menu";
import { Cart } from './Cart';
import { useForm } from "react-hook-form";
import { actSendCart } from '../../action/product'
import { useRouter } from 'next/router'
import { LoadingAll } from '../../components/Aside/LoadingAll';
import { UIStore } from "../Model/UIStore"
import { i18n, withTranslation } from "../../i18n";
import { formatPrice } from '../Conver/unit'
import { actGetInfor } from '../../action/auth'
import { getInforUser, sendOrders, checkout } from '../../constants/querry'
import city from '../../constants/city'

export const Checkout = withTranslation('common')(memo((props) => {
    const [loading, setLoading] = useState(false)
    const router = useRouter()
    const auth = UIStore.useState(s => s.auth)
    const profile = UIStore.useState(s => s.profile)
    const [address, setAddress] = useState({
        city: {
            id: 0, name: ''
        },
        district: {
            id: 0, name: ''
        },
        ward: {
            id: 0,
            name: ''
        }
    })
    const [defaultData, setDefaultData] = useState({
        fullname: '',
        city: '',
        address: '',
        phone: '',
        email: '',
        method_shipping: 'cod'
    })
    const formatCurencyToNumber = (currency) => {
        // var currency = "528.000₫"; //it works for US-style currency strings as well
        // var cur_re = /\D*(\d+|\d.*?\d)(?:\D+(\d{2}))?\D*$/;
        // var parts = cur_re.exec(currency);
        // var number = parseFloat(parts[1].replace(/\D/, '') + '.' + (parts[2] ? parts[2] : '00'));
        var number = currency?.toString()?.match(/\d/g);
        number = number.join("");
        // console.log('currency', currency);

        return number.toString()
    }
    useEffect(() => {
        if (auth?.authToken !== undefined) {
            if (profile?.email === undefined) {
                getInforUser(auth.customer.id, auth?.token).then(data => {
                    if (data?.data?.data?.customer !== undefined) {
                        UIStore.update(s => { s.profile = data?.data?.data?.customer })
                    }
                })
            }

        }
    }, [auth])
    useEffect(() => {
        if (profile?.email !== undefined) {
            setDefaultData({
                ...defaultData,
                fullname: profile?.billing?.firstName ? profile?.billing?.firstName : profile.firstName,
                city: profile?.billing?.city || "",
                address: profile?.billing?.address1 || "",
                phone: profile?.billing?.phone || "",
                email: (profile?.billing?.email?.substring(0, 3) !== 'FB_' && profile?.billing?.email !== "") ?
                    profile?.billing?.email
                    :
                    (profile?.email.substring(0, 3) !== 'FB_' ? profile?.email : "")

            })
        }
    }, [profile])



    // useEffect(() => {
    //     if (auth?.data?.profile?.id !== undefined) {
    //         actGetInfor(auth.data.profile.id).then(data => {
    //             UIStore.update(s => { s.profile = data.data })
    //         })
    //     }
    //     // return () => {

    //     // }
    // }, [auth?.data?.profile?.id])
    // useEffect(() => {
    //     if (profile?.id !== undefined) {
    //         setDefaultData({
    //             fullname: profile.first_name,
    //             city: profile?.shipping?.city,
    //             address: profile?.shipping?.address_1,
    //             phone: profile?.billing?.phone,
    //             email: profile?.email.substring(0, 3) !== 'FB_' ? profile?.email : ''
    //         })
    //     }
    // }, [profile])

    const onChangeAddress = (e, key) => {
        let id = e.target.value
        let name = e.target[e.target.selectedIndex].text
        let checkPos = {}
        if (e.target.name === 'city') {
            checkPos = {
                district: {
                    id: 0,
                    name: 0
                },
                ward: {
                    id: 0,
                    name: 0
                }
            }
        }
        if (e.target.name === 'district') {
            checkPos = {
                ward: {
                    id: 0,
                    name: 0
                }
            }
        }
        setAddress({
            ...address,
            [e.target.name]: { id, name },
            ...checkPos
        })

    }
    const queryfy = obj => {
        // Make sure we don't alter integers.
        if (typeof obj === 'number') {
            return obj;
        }

        if (Array.isArray(obj)) {
            const props = obj.map(value => `${queryfy(value)}`).join(',');
            return `[${props}]`;
        }

        if (typeof obj === 'object') {
            const props = Object.keys(obj)
                .map(key => `${key}:${obj[key] !== 'VN' ? queryfy(obj[key]) : obj[key]}`)
                .join(',');
            return `{${props}}`;
        }

        return JSON.stringify(obj);
    };

    const { register, errors, handleSubmit } = useForm();
    const onSubmit = async (data) => {
        var line_items = await []
        for (var element of props.cart?.data) {
            let dataFetch = await {}
            dataFetch.productId = await element.id
            if (element.validationId !== '') {
                dataFetch.variationId = await element?.validationId
            }
            dataFetch.quantity = await element?.quantily
            await line_items.push(dataFetch)
        };
        // console.log('line_items', line_items)

        var body = await {
            shipping: {
                firstName: data.name,
                lastName: '',
                address1: data.address,
                address2: '',
                city: `${address.ward.name} ${address.district.name} ${address.city.name}`,
                country: 'VN',
                state: '',
                postcode: '90000',
                email: data.email,
                phone: data.phone,
                company: '',
            },
            billing: {
                firstName: data.name,
                lastName: '',
                address1: data.address,
                address2: '',
                city: `${address.ward.name} ${address.district.name} ${address.city.name}`,
                country: 'VN',
                state: '',
                postcode: '90000',
                email: data.email,
                phone: data.phone,
                company: '',
            },
            shipToDifferentAddress: true,
            paymentMethod: defaultData.method_shipping,
            isPaid: false,
        };

        await setLoading(true)
        await checkout(queryfy(body), auth.token, localStorage.getItem('woocommerce-session') || null).then(data => {
            setLoading(false)
            if (data.data.data.checkout !== null) {
                localStorage.removeItem('cart')
                UIStore.update(s => {
                    s.cart = {
                        total: 0,
                        data: []
                    }
                })
                router.replace({
                    pathname: '/order-complete',
                    query: { numberOder: data?.data.data.checkout.order.orderNumber },
                })
            } else {
                window.alert('that bai')
            }

        })
    }

    const { t } = props
    var language = props.i18n.language
    // console.log('props.price', props.price);

    return (
        <>
            {loading && <LoadingAll />}
            <Menu name='checkout' />
            <div className="container-fix w-full pt-12">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="px-4">
                        <div className="grid grid-cols-1 md:grid-cols-2 md:gap-8">
                            <div>
                                <div>
                                    <div className="text-center py-3 font-semibold text-xl uppercase mb-5">{t('your_information')}</div>
                                    <div className="md:px-6 px-3 py-2">
                                        <div>
                                            <div className="py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('full_name')} (*)</label>
                                                <input className="w-full px-2 py-2 border-2 " defaultValue={defaultData.fullname} placeholder={t('full_name')} name="name" ref={register({ required: t('name_required') })} />
                                                {errors.name && <span className="text-red-500">{errors.name?.message}</span>}
                                            </div>



                                            <div className="col-span-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('province_city')} (*)</label>
                                                <div className="flex justify-between mt-2 text-sm relative bg-F1F6F8 rounded-md border_select_cart">
                                                    <select className="w-full h-10 px-1 outline-none arrow_select_none z-20 bg-transparent border-2 text-base"
                                                        required name="city" value={address.city.id} onChange={e => onChangeAddress(e)}>
                                                        <option value="" >{t('select_province')}</option>
                                                        {
                                                            Object.keys(city).map(function (key, index) {
                                                                return <option key={city[key]?.id} value={city[key]?.id}>{city[key]?.name}</option>
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                            </div>

                                            <div className="md:col-span-1 col-span-1 py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('district')} (*)</label>
                                                <div className="flex justify-between mt-2 text-sm relative bg-F1F6F8 rounded-md border_select_cart">
                                                    <select className="w-full h-10 px-1 outline-none arrow_select_none z-20 bg-transparent border-2 text-base"
                                                        required name="district" value={address.district.id} onChange={e => onChangeAddress(e)}>
                                                        <option value="" value_name="">{t('select_district')}</option>
                                                        {
                                                            address.city.id > 0 &&
                                                            Object.keys(city[address.city.id].data).map(function (key, index) {
                                                                return <option key={city[address.city.id].data[key]?.id}
                                                                    value_name={city[address.city.id].data[key]?.name}
                                                                    value={city[address.city.id].data[key]?.id}>{city[address.city.id].data[key]?.name}</option>
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                            </div>


                                            <div className="md:col-span-1 col-span-1 py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('wards')} (*)</label>
                                                <div className="flex justify-between mt-2 text-sm relative bg-F1F6F8 rounded-md border_select_cart">
                                                    <select className="w-full h-10 px-1 outline-none arrow_select_none z-20 bg-transparent border-2 text-base"
                                                        required name="ward" value={address.ward.id} onChange={e => onChangeAddress(e)}
                                                    >
                                                        <option value="">{t('select_wards')}</option>
                                                        {
                                                            address.district.id > 0 &&
                                                            Object.keys(city[address.city.id].data[address.district.id].data).map(function (key, index) {
                                                                return <option key={index}
                                                                    value_name={city[address.city.id].data[address.district.id].data[key]?.name}
                                                                    value={city[address.city.id].data[address.district.id].data[key]?.id}>
                                                                    {city[address.city.id].data[address.district.id].data[key]?.name}
                                                                </option>
                                                            })
                                                        }
                                                    </select>
                                                </div></div>

                                            {/* < */}




                                            {/* <div className="py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold" >{t('province_city')} *</label>
                                                <input className="w-full px-2 py-2 border-2 " defaultValue={defaultData.city} placeholder={t('province_city')} name="city" ref={register({ required: t('province_city_required') })} />
                                                {errors.city && <span className="text-red-500">{errors.city?.message}</span>}
                                            </div> */}
                                            <div className="py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('address')} (*)</label>
                                                <input className="w-full px-2 py-2 border-2 " defaultValue={defaultData.address} placeholder={t('address')} name="address" ref={register({ required: t('address_required') })} />
                                                {errors.address && <span className="text-red-500">{errors.address?.message}</span>}
                                            </div>
                                            <div className="py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('phone')} (*)</label>
                                                <input className="w-full px-2 py-2 border-2 " defaultValue={defaultData.phone} placeholder={t('phone')} name="phone" ref={register({ required: t('phone_required') })} pattern="[0-9]{10}" title={t('check_phone')} />
                                                {errors.phone && <span className="text-red-500">{errors.phone?.message}</span>}
                                            </div>
                                            <div className="py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">Email (*)</label>
                                                <input className="w-full px-2 py-2 border-2 " defaultValue={defaultData.email} placeholder="Email" name="email" ref={register({ required: t('email_required'), pattern: { value: /^\S+@\S+$/i, message: t('incorrect_email_format') } })} />
                                                {errors.email && <span className="text-red-500">{errors.email?.message}</span>}
                                            </div>
                                            <div>
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('payment_method')} (*)</label>
                                                <div className=" pt-2 pb-4 border-b  mt-3 border-gray-200">
                                                    <label>
                                                        <input className="h-4" type="radio" name="payment_method" value="cod" checked={defaultData.method_shipping === 'cod'} onChange={() => setDefaultData({ ...defaultData, method_shipping: 'cod' })} />
                                                        <p className="text-sm font-semibold uppercase leading-5 pb-1">{t('cod')}</p>
                                                    </label>
                                                    {/* <span className="ml-5 block text-sm font-medium normal-case opacity-80 ">
                                                        {t('cod')}
                                                    </span> */}

                                                </div>
                                                <div className=" pt-2 pb-4 border-b mt-3 border-gray-200">
                                                    <label>
                                                        <input className="h-4" type="radio" name="payment_method" value="bacs" checked={defaultData.method_shipping === 'bacs'} onChange={() => setDefaultData({ ...defaultData, method_shipping: 'bacs' })} />
                                                        <p className="text-sm font-semibold uppercase leading-5 pb-1">{t('bacs')}</p>
                                                    </label>
                                                    <span className="ml-5 block text-sm font-medium normal-case opacity-80 ">
                                                        {t('bank1')}  <span className="font-semibold">NGO QUANG PHONG</span>
                                                    </span>
                                                    <span className="ml-5 block text-sm font-medium normal-case opacity-80 ">
                                                        {t('bank2')}: <span className="font-semibold">0011004151200</span>
                                                    </span>
                                                    <span className="ml-5 block text-sm font-medium normal-case opacity-80 ">
                                                        {t('bank3')}
                                                    </span>
                                                    <span className="ml-5 block text-sm font-medium normal-case opacity-80 ">
                                                        {t('bank4')}: <span className="font-semibold">{props.price?.fullPriceSV?.total ? props.price?.fullPriceSV?.total : props.price.totalAll}</span>
                                                    </span>
                                                    <span className="ml-5 block text-sm font-medium normal-case opacity-80 ">
                                                        {t('bank5')}
                                                    </span>
                                                </div>


                                            </div>
                                            <div className="py-2">
                                                <label className="w-full block text-sm mb-2 text-gray-500 font-semibold">{t('note')}</label>
                                                <textarea className="w-full px-2 py-2 border-2 h-40" placeholder={t('note')} name="description" ref={register}></textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="css-checkout reveiew relative">
                                <div className="bg-F8F8F8 py-6 md:px-8 px-3">
                                    <div className="text-center py-3 font-semibold text-xl uppercase mb-5">{t('your_cart')}</div>
                                    <div className="md:px-6 px-3 py-2 bg-white">
                                        <div>
                                            <div className="flex justify-between border-b-1 border-gray-200 py-4">
                                                <label className="font-semibold uppercase">{t('product')}</label>
                                                <label className="font-semibold uppercase">{t('subtotal')}</label>
                                            </div>
                                            {
                                                props.cart?.data?.map((item, index) => {
                                                    return (
                                                        <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm" key={index}>
                                                            <label className="">{item.name}  x{item.quantily}</label>
                                                            <span>{formatPrice(formatCurencyToNumber(item.quantily) * formatCurencyToNumber(item.price))} </span>

                                                        </div>
                                                    )
                                                })
                                            }

                                        </div>
                                        <div>
                                            <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                                                <label className="font-semibold">{t('subtotal')}</label>
                                                {/* <span className="font-semibold">{formatPrice(props.price.totalFetch)}</span> */}
                                                <span className="font-semibold">{formatPrice(formatCurencyToNumber(props.price.fullPriceSV?.subtotal)) || '0₫'}</span>
                                            </div>
                                            <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                                                <label className="font-semibold">{t('shippingTotal')}</label>
                                                <span className="font-semibold">{formatPrice(formatCurencyToNumber(props.price.fullPriceSV?.shippingTotal)) || '0₫'}</span>
                                            </div>
                                            {/* <div className="flex justify-between border-b-1 border-gray-200 py-4 text-sm">
                                                <label className="font-semibold">VAT({props.price.vat}%)</label>
                                                <span className="font-semibold">{formatPrice(props.price.priceVAT)}</span>
                                            </div> */}
                                            <div className="flex justify-between py-4 items-center">
                                                <label className="font-semibold text-xl">{t('total')}</label>
                                                <span className="font-semibold text-xl">{props.price?.fullPriceSV?.total ? props.price?.fullPriceSV?.total : props.price.totalAll} </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className=" py-2 mt-6 hidden">
                                        <div className="px-2 font-semibold text-sm">{t('direct_bank_transfer')}</div>
                                        <div className="bg-white px-4 pt-4 mt-3 text-sm text-gray-600">
                                            <p>Make your payment directly into our bank account.Please use your Order ID as the payment reference.Your order won’t be shipped until the funds have cleared in our account.</p>
                                        </div>
                                    </div>
                                    <div className=" py-2 mt-3">
                                        <div className="px-2 font-semibold text-sm">{t('policy')}</div>
                                        <div className="bg-white px-4 pt-4 mt-3 text-sm text-gray-600">
                                            <p>{t('text_privacy_policy')}
                                                <Link href={language == 'vn' ? "/chinh-sach-bao-mat" : "/privacy-policy"}><a className="text-blue-500 font-semibold"> {t('privacy_policy')} </a></Link>
                                                {t('and')}
                                                <Link href={language == 'vn' ? "/chinh-sach-doi-tra" : "/return-policy"}><a className="text-blue-500 font-semibold"> {t('return_policy')}</a></Link>.
                                            </p>

                                        </div>
                                    </div>
                                    <div className="mt-4">
                                        <div className="flex items-center">
                                            <label className="label-nho text-sm"> {t('text_agree_terms')} *
                                                <input type="checkbox" ref={register({ required: t('require_checkout') })} name="checkbox" />
                                                <span className="checkmark rounded"></span>
                                            </label>
                                        </div>
                                        {errors.checkbox && <span className="text-red-500">{errors.checkbox?.message}</span>}
                                    </div>
                                    <div>
                                        <div className="mt-3">
                                            <button className="w-full text-center uppercase text-white bg-gray-900 text-sm font-semibold py-3">{t('proceed_to_checkout')}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div >
        </>
    );
}));
