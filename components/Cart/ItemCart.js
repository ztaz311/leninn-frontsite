import React, { memo } from 'react';
import { withTranslation } from "../../i18n";
import { formatPrice } from '../Conver/unit';
import Link from 'next/link'
export const ItemCart = withTranslation('common')(memo((props) => {
    // const [quantily, setQuantily] = useState(1)
    // const [itemCart, setItemCart] = useState(true)
    const handleRemoveItemCart = (e) => {
        props.removeCart()
    }

    const { t } = props
    const checkArr = () => {
        // console.log('props.data.color', props.data.color)
        if (props.data?.size !== null && props.data?.color !== null) {
            return "( " + props.data.size + ' - ' + props.data.color + " )"
        } else if (props.data?.size !== null) {
            return "( " + props.data.size + " )"
        } else if (props.data?.color !== null) {
            return "( " + props.data.color + " )"
        }
        else {
            return ''
        }
    }
    const formatCurencyToNumber = (currency) => {
        // console.log('currency', currency);
        // var currency = "528.000₫"; //it works for US-style currency strings as well
        var cur_re = /\D*(\d+|\d.*?\d)(?:\D+(\d{2}))?\D*$/;
        var parts = cur_re.exec(currency);
        // var number = parseFloat(parts[1].replace(/\D/, '') + '.' + (parts[2] ? parts[2] : '00'));
        return parts[1].match(/\d/g).join("").toString()
    }
    return (
        <>
            <tr className="border-b-1 border-gray-300 woocommerce-cart-form__cart-item" >
                {/* <td className="py-4 text-center px-4"><img src="https://woodmart.xtemos.com/wp-content/uploads/2016/08/product-accessories-8.jpg" alt="sanpham" /></td> */}
                <td className="py-4 text-center px-4 product-thumbnail">
                    <Link href={`/product/${props.data.slug}`}>
                        <a>
                            <img src={props.data.image} alt="sanpham" />
                        </a>
                    </Link>
                </td>
                <td className="py-4 text-sm font-semibold product-name" style={{ justifyContent: 'start' }}>
                    <Link href={`/product/${props.data.slug}`}>
                        <a>
                            <span className="w-full">{props.data.name} </span>
                            <div>{checkArr()}</div>
                        </a>
                    </Link>
                </td>
                <td className="py-4 text-center text-xs product-price" data-title={t('price')}>{formatPrice(formatCurencyToNumber(props.data.price))} </td>

                <td className="py-4 text-center text-sm product-quantity" data-title={t('quantily')}>
                    <div className="flex-1 flex justify-end w-full items-center">
                        <div className="styled-select relative border-gray-200 border-2 flex py-1 w-20">
                            <div className="px-2 font-semibold cursor-pointer border-r-2 border-gray-200 py-1" onClick={() => props.handleClickSub()}>-</div>
                            <input className="w-6 text-center" value={props.data.quantily} readOnly />
                            <div className="px-2 font-semibold cursor-pointer border-l-2 border-gray-200 py-1" onClick={(e) => props.handleClickPlus(e)}>+</div>
                        </div>
                    </div>

                </td>
                <td className="py-4 text-center text-sm font-semibold" data-title={t('subtotal')}>{formatPrice(formatCurencyToNumber(props.data.price) * formatCurencyToNumber(props.data.quantily))} </td>
                <td className="py-4 text-center text-sm product-remove"><button className="text-3xl text-gray-900 font-semibold outline-none" onClick={(e) => handleRemoveItemCart(e)}>×</button></td>
            </tr>
        </>
    );
}));

