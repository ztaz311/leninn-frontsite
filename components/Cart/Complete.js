import Link from 'next/link';
import React, { memo, useState } from 'react';
import { i18n, withTranslation } from "../../i18n";
export const Complete = withTranslation('common')(memo((props) => {
    const { t } = props
    return (
        <>
            <div className="mt-6">
                <div className="flex justify-center">
                    <label className="uppercase font-semibold text-2xl text-center">{t('order_complete')}!</label>
                </div>
                <div className="uppercase justify-center mt-7 flex-col">
                    <div className="pt-2 text-center">{props.t('check_email')}</div>
                    <div className="flex items-center justify-center">
                        <div className="pt-2 text-center">{props.t('bank6')}: &nbsp;</div>
                        <span className="font-semibold text-3xl self-start ">#{props.numberOder}</span>
                    </div>
                </div>
                <div className="flex justify-center mt-4">
                    <Link href="/">
                        <a className="" style={{ backgroundColor: '#eba125' }}>
                            <div className="px-5 py-3 font-bold">{t('back_home')}</div>
                        </a>
                    </Link>
                </div>
            </div>
        </>
    );
}));

