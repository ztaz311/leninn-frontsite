import Link from 'next/link';
import React, { memo, useEffect, useState } from 'react';
import { getFooter } from '../../constants/querry';
import { withTranslation } from '../../i18n';
export const Footer = withTranslation('common')(memo((props) => {

    const [data, setData] = useState({
        address1: "110 Nguyễn Hy Quang, Đống Đa, Hà Nội",
        address2: "40A Ấu Triệu, Hoàn Kiếm, Hà Nội",
        hotline: "090 222 6803",
        mxh: {
            facebook: "https://www.facebook.com/leninnskateshop/",
            instagram: "https://www.facebook.com/leninnskateshop/",
            youtube: "",
        }
    })
    useEffect(() => {
        if (props?.footer !== undefined) {
            if (props.i18n.language === 'vn' && props.footer?.footerVI?.footer !== undefined) {
                setData(props.footer?.footerVI.footer)
            }
            if (props.i18n.language === 'en' && props.footer?.footerVI?.footer !== undefined) {
                setData(props.footer?.footerEN.footer)
            }
        } else {
            getFooter().then(res => {
                if (props.i18n.language === 'vn' && res?.data?.data?.footerVI?.footer !== undefined) {
                    setData(res?.data?.data?.footerVI.footer)
                }
                if (props.i18n.language === 'en' && res?.data?.data?.footerVI?.footer !== undefined) {
                    setData(res?.data?.data?.footerEN.footer)
                }

            })
        }
    }, [props.i18n.language])
    const { t } = props
    // console.log(props.i18n.language)
    var language = props.i18n.language
    return (
        <>
            <div className="bg-F7F7F7">
                <div className="container-fix w-full">
                    <div className="px-4">
                        <div className="grid grid-cols-12 gap-4 py-8">
                            <div className="md:col-span-3 col-span-12">
                                <div className="flex items-center mb-2">
                                    <img className="mr-3" src="../img/ic_left.svg" alt="qa" />
                                    <div className="text-sm leading-4"><Link href={language == 'vn' ? "/chinh-sach-bao-mat" : "/privacy-policy"}><a>{t('privacy_policy')}</a></Link></div>
                                </div>
                                <div className="flex items-center mb-2">
                                    <img className="mr-3" src="../img/ic_left.svg" alt="qa" />
                                    <div className="text-sm leading-4"><Link href={language == 'vn' ? '/chinh-sach-doi-tra' : "/return-policy"}><a>{t('return_policy')}</a></Link></div>
                                </div>
                            </div>
                            <div className="md:col-span-5 col-span-12">
                                {data?.address1 !== '' && data?.address1 !== undefined &&
                                    <div className="flex items-center mb-2">
                                        <div className="text-sm leading-4 mr-1 font-semibold">{t('facility')} 1:</div>
                                        <div className="text-sm leading-4 flex-1">{data.address1}</div>
                                    </div>
                                }
                                {data?.address2 !== '' && data?.address2 !== undefined &&
                                    <div className="flex items-center mb-2">
                                        <div className="text-sm leading-4 mr-1 font-semibold">{t('facility')} 2:</div>
                                        <div className="text-sm leading-1 flex-1">{data.address2}</div>
                                    </div>
                                }
                                {data?.hotline !== '' && data?.hotline !== undefined &&
                                    <div className="flex items-center mb-2">
                                        <div className="text-sm leading-4 mr-1 font-semibold">{t('hotline')}:</div>
                                        <div className="text-sm leading-4">{data.hotline}</div>
                                    </div>
                                }
                            </div>
                            <div className="md:col-span-4 col-span-12">
                                <div className="flex items-center mb-5">
                                    <div className="text-sm leading-4 ">{t('follow_us_on_social_media')}</div>
                                </div>
                                <div className="flex items-center">
                                    {data?.mxh?.facebook !== '' && data?.mxh?.facebook !== undefined && data?.mxh?.facebook !== null && <Link href={data?.mxh?.facebook}><a target="_blank"><img className="mr-5" src="../img/ic-facebook.svg" alt="qa" /></a></Link>}
                                    {data?.mxh?.instagram !== '' && data?.mxh?.instagram !== undefined && data?.mxh?.instagram !== null && <Link href={data?.mxh?.instagram}><a target="_blank"><img className="mr-5" src="../img/ic-inta.svg" alt="qa" /></a></Link>}
                                    {data?.mxh?.youtube !== '' && data?.mxh?.youtube !== undefined && data?.mxh?.youtube !== null && <Link href={data?.mxh?.youtube}><a target="_blank"><img className="mr-5" src="../img/youtube.svg" alt="qa" /></a></Link>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="flex justify-center items-center pt-7 pb-8 border-t-1 border-gray-300">
                    <div className="text-gray-400 text-sm leading-4">© 2021 {t('lennin_all_rights_reserved')}</div>
                </div>

            </div>

        </>
    )
}));