import { signOut, useSession } from 'next-auth/client';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { memo, useEffect, useRef, useState } from 'react';
import { useToasts } from 'react-toast-notifications';
import { getLogoHome, getProductCategories, refeshToken } from '../../constants/querry';
import { i18n, withTranslation } from '../../i18n';
import { FormModel } from "../Model/FormModel";
import { UIStore } from "../Model/UIStore";

export const Header = withTranslation('common')(memo((props) => {
    const router = useRouter()
    const cart = UIStore.useState(s => s.cart);
    const { t } = props

    let cartTotal = 0
    cart.data.forEach(element => {
        cartTotal += element.quantily
    });
    const { addToast } = useToasts();
    const auth = UIStore.useState(s => s.auth)
    const category = UIStore.useState(s => s.category)
    const category1 = category?.filter(s => s.parent === 0).sort((a, b) => a?.menu_order - b?.menu_order)
    let category2 = category?.filter(s => s.parent !== 0)?.reduce((r, a) => {
        r[a.parent] = [...r[a.parent] || [], a];
        return r;
    }, {});


    const [session] = useSession();
    const inputRef = useRef()
    useEffect(() => {
        var cart = localStorage.getItem('cart')
        if (cart !== undefined && cart !== null) {
            UIStore.update(s => { s.classNoti = 'add'; s.cart = JSON.parse(cart); })
        }

        if (props?.cate !== undefined) {
            let xx = [...props.cate]

            const xx2 = xx.map(elem => {
                return Object.assign({}, elem, {
                    ...elem,
                    parent: elem.parentDatabaseId || 0,
                    id: elem.databaseId,
                    menu_order: elem.menuOrder,
                });
            });

            UIStore.update(s => { s.category = xx2 })
        } else {
            getProductCategories().then(data => {
                let xx = [...data.data.data.productCategories.nodes]
                xx.forEach(element => {
                    element.parent = element.parentDatabaseId || 0
                    element.id = element.databaseId
                    element.menu_order = element.menuOrder

                });
                UIStore.update(s => { s.category = xx })
            })
        }
    }, [props?.cate])
    useEffect(() => {
        if (auth.refreshToken) {
            setInterval(() => {
                refeshToken(auth.refreshToken).then(res => {
                    // console.log('res', res.data.data.refreshJwtAuthToken.authToken);
                    // console.log('auth', auth);
                    let xx = { ...auth }
                    xx.authToken = res.data.data.refreshJwtAuthToken.authToken
                    xx.token = res.data.data.refreshJwtAuthToken.authToken
                    UIStore.update(s => { s.auth = xx });
                })
            }, 1000 * 60 * 3);
        } else {
            // console.log('1');
        }
    }, [auth])
    // 

    const classNoti = UIStore.useState(s => s.classNoti);
    // const cartTotal = UIStore.useState(s => s.cart.total);
    const [isOpen, setIsOpen] = useState(false)
    const [menu, setStateMenu] = useState(false)
    const [search, setStateSearch] = useState(false)
    const [searchMb, setStateSearchMb] = useState(false)
    const [userL, setStateUserL] = useState(false)
    const [removeClass, setRemoveClass] = useState(true)
    const [stateScrollHeader, setStateScrollHeader] = useState(false)
    const [keywordS, setKeywordS] = useState('')
    const [lang, setLang] = useState('')
    const [openLang, setOpenLang] = useState(false)
    useEffect(() => {
        setLang(i18n.language)
        setOpenLang(false);
    }, [i18n.language])
    const checkChangePassword = UIStore.useState(s => s.checkChangePassword)
    useEffect(() => {
        if (checkChangePassword) {
            setTimeout(() => {
                if (auth.token !== '') {
                    setStateUserL(!userL);
                } else {
                    setIsOpen(!isOpen);
                }
            }, 500);
        }
    }, [checkChangePassword])
    // useEffect(() => {
    //     if (auth) {
    //         UIStore.update(s => { s.auth = { token: '' } });
    //         // if (localStorage.getItem('auth') !== undefined && localStorage.getItem('auth') !== null) {
    //         //     localStorage.removeItem('auth')
    //         // }
    //         document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    //         closeUserL();
    //         // router.push('/')
    //     }
    // }, [auth])
    const handleClickOpenLang = (e) => {
        setOpenLang(!openLang);
    }
    const handleClickLogOut = async () => {
        await UIStore.update(s => { s.auth = { token: '' }; s.profile = {} });
        if (session) {
            await signOut()
        }
        // if (localStorage.getItem('auth') !== undefined && localStorage.getItem('auth') !== null) {
        //     localStorage.removeItem('auth')
        // }
        document.cookie = "auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        closeUserL();
    }
    const closeMenu = () => {
        setStateMenu(false);
        setOpenLang(false);
    }
    const closeSearch = () => {
        setStateSearch(false);
    }
    const closeSearchMb = () => {
        setStateSearchMb(false);
    }
    const closeUserL = () => {
        setStateUserL(false);
    }
    const handleScroll = () => {
        var scrollY = window.scrollY;
        if (scrollY > 10 && !props.pageClass) {
            setStateScrollHeader(true)
            setRemoveClass(false);
        } else {
            setStateScrollHeader(false)
        }
        // closeMenu();
    }
    const handleClickSearch = () => {
        setStateSearch(!search);
    }
    const handleClickSearchMb = () => {
        setStateSearchMb(!search);
    }
    const handleChangeSearch = (e) => {
        setKeywordS(e.value);
    }
    const handleClickUser = () => {
        if (auth.token !== '') {
            setStateUserL(!userL);
        } else {
            setIsOpen(!isOpen);
        }
    }
    const handleClickUser2 = () => {
        if (auth.token !== '') {
            setStateUserL(true);
            router.push('/myprofile')
        } else {
            setIsOpen(!isOpen);
        }
    }
    const handleClickHeader = (e) => {
        setStateMenu(!menu);
    }
    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    });
    const hanlderEnter = () => {
        // if (e.charCode == 13) {

        if (keywordS !== '') {
            router.push('/search?keyword=' + keywordS)
        } else {
            addToast(props.t('search_null'), { appearance: 'warning' });

        }
    }
    const wrapperRef = useRef(null);
    const searchRef = useRef(null);
    const searchRefMb = useRef(null);
    const userLRef = useRef(null);
    useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
                closeMenu();
            }
            if (searchRef.current && !searchRef.current.contains(event.target)) {
                closeSearch();
            }
            if (searchRefMb.current && !searchRefMb.current.contains(event.target)) {
                closeSearchMb();
            }
            if (userLRef.current && !userLRef.current.contains(event.target)) {
                closeUserL();
            }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperRef]);
    const getIsOpen = (open) => {
        setIsOpen(false);
    }

    const checkChil = (item) => {
        if (category2 !== undefined && category2[item.id] !== undefined && category2[item.id]?.length > 0) {
            return true
        }
        return false
    }

    const logo = UIStore.useState(s => s.logo)


    useEffect(() => {
        // getLogoHome().then(res => {
        //     if (res.data.data.getHeader.siteLogoUrl !== undefined) {
        //         UIStore.update(s => { s.logo = res.data.data.getHeader.siteLogoUrl })
        //     }
        // })
        if (props?.logo !== undefined) {
            UIStore.update(s => { s.logo = props.logo })
        } else {
            getLogoHome().then(res => {
                if (res.data.data.getHeader.siteLogoUrl !== undefined) {
                    UIStore.update(s => { s.logo = res.data.data.getHeader.siteLogoUrl })
                }
            })
        }
    }, [props?.logo])

    const [toogle, setToogle] = useState(false)
    const [checkHover, setCheckHover] = useState({})
    var language = props.i18n.language


    return (
        <>
            <Head>
                <title>Leninn</title>
                <link rel="shortcut icon" href="../img/favicon.png" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>

            <div ref={wrapperRef} className="fixed w-full z-50">
                {/* bg-black */}
                <nav className={`dark:bg-gray-800  ${!props.pageClass ? (stateScrollHeader ? 'md:bg-black animation' : "md:bg-transparent animation2") : ''} ${props.pageClass ? 'md:bg-black' : "md:bg-transparent"}  ${!props.pageClass ? (stateScrollHeader ? 'bg-black animation' : "bg-black bg-opacity-30") : 'bg-black animation'}`}>
                    <div className="mx-auto md:px-16 pl-4 pr-1">
                        <div className="flex items-center justify-between md:h-17 h-14 w-full">
                            <div className="flex">
                                <Link href="/">
                                    <a className="flex-shrink-0" href="/">
                                        <img src={logo === '' ? "../../img/logo.png" : logo} alt="Lenin" />
                                    </a>
                                </Link>
                            </div>
                            <div className="hidden md:flex items-center justify-center uppercase">
                                <div className="relative">
                                    <Link href="/products" >
                                        <a ref={inputRef} className={`hover_pr text-white ${stateScrollHeader ? 'hover:text-white text-gray-200' : (!props.pageClass ? 'hover:text-gray-800' : '')} 
                                ${props.pageClass ? 'hover:text-white text-gray-200' : ''} dark:hover:text-white block px-6 py-2 rounded-md text-sm font-bold`} href="/#">
                                            {t('product')}
                                        </a>
                                    </Link>
                                    <div className={`absolute hover_pr_keep mt-9 h-auto w-72 top-0 right-0 z-10 pt-4`}>
                                        <div className="bg-gray-100 ">
                                            {
                                                category1?.map((item, index) => {
                                                    return (
                                                        <div className={checkChil(item) ? 'relative' : ''} key={index}>
                                                            <Link href={`/products/${item.slug}`}>
                                                                <a className={`flex border-b-1 py-4 px-7 hover:bg-gray-300
                                                                 ${checkChil(item) ? 'items-center justify-between hover_pr_keep_de' : ''}  
                                                                `}>
                                                                    <div>
                                                                        <h5 className="capitalize text-ms font-semibold">{item.name}</h5>
                                                                    </div>

                                                                    {checkChil(item) &&
                                                                        <div><img src="../../img/ic-left-copy-5.svg" /></div>
                                                                    }
                                                                </a>
                                                            </Link>
                                                            {checkChil(item) &&
                                                                <div className={`absolute h-auto w-72 top-0 left-100p z-10 hover_pr_keep_de_keep `}>
                                                                    <div className="bg-gray-100 border-l-2 border-gray-300">
                                                                        {
                                                                            category2[item.id] !== undefined && category2[item.id]?.length > 0 &&
                                                                            category2[item.id].sort((a, b) => a?.menu_order - b?.menu_order).map((item2, index2) => {
                                                                                return (
                                                                                    <Link href={`/products/${item2.slug}`} key={index2}>
                                                                                        <a className="flex border-b-1 py-4 px-7 hover:bg-gray-300">
                                                                                            <div>
                                                                                                <h5 className="capitalize text-ms font-semibold">{item2.name}</h5>
                                                                                            </div>
                                                                                        </a>
                                                                                    </Link>
                                                                                )
                                                                            })
                                                                        }

                                                                    </div>
                                                                </div>
                                                            }

                                                        </div>
                                                    )
                                                })
                                            }

                                        </div>
                                    </div>
                                </div>
                                {/* <Link href="/newin"><a className={`text-white ${stateScrollHeader ? 'hover:text-white text-gray-200' : (!props.pageClass ? 'hover:text-gray-800' : '')} 
                                ${props.pageClass ? 'hover:text-white' : ''} dark:hover:text-white block px-6 py-2 rounded-md font-bold text-sm`} href="/#">
                                    {t('new_in')} </a>
                                </Link> */}

                                <Link href="/saleoff"><a className={`text-white ${stateScrollHeader ? 'hover:text-white text-gray-200' : (!props.pageClass ? 'hover:text-gray-800' : '')} 
                                ${props.pageClass ? 'hover:text-white' : ''} dark:hover:text-white block px-6 py-2 rounded-md font-bold text-sm`} href="/#">
                                    {t('sales')} </a>
                                </Link>
                                {/*  */}
                                <Link href="/gallery"><a className={`text-white ${stateScrollHeader ? 'hover:text-white text-gray-200' : (!props.pageClass ? 'hover:text-gray-800' : '')} 
                                ${props.pageClass ? 'hover:text-white' : ''} dark:hover:text-white block px-6 py-2 rounded-md font-bold text-sm`} href="/#">
                                    {t('gallery')} </a>
                                </Link>
                                {/* <Link href="/blog"><a className={`text-white ${stateScrollHeader ? 'hover:text-white text-gray-200' : (!props.pageClass ? 'hover:text-gray-800' : '')} 
                                ${props.pageClass ? 'hover:text-white text-gray-200' : ''} dark:hover:text-white block px-6 py-2 rounded-md font-bold text-sm`} href="/#">
                                    {t('blog')}</a>
                                </Link> */}
                                <Link href={language === 'vn' ? '/ve-chung-toi' : "/about"}><a className={`text-white ${stateScrollHeader ? 'hover:text-white text-gray-200' : (!props.pageClass ? 'hover:text-gray-800' : '')} 
                                ${props.pageClass ? 'hover:text-white text-gray-200' : ''} dark:hover:text-white block px-6 py-2 rounded-md font-bold text-sm`} href="/#">
                                    {t('about')} </a>
                                </Link>
                            </div>

                            <div className="hidden md:flex  items-center justify-center uppercase">
                                <div className="flex  items-center justify-center uppercase px-7">
                                    <div className="relative">
                                        <div className="pr-3 cursor-pointer" onClick={(e) => handleClickSearch(e)}>
                                            <img src="../../img/search.svg" alt="search" />
                                        </div>
                                        <div ref={searchRef} className={`absolute bg-gray-200 mt-12 h-24 w-96 top-0 right-0 z-10 px-5 py-5 flex ${search ? '' : 'hidden'}`}>
                                            <input className="bg-gray-200 w-full py-3 px-3 outline-none border-b-1 border-gray-400" placeholder={t('search')} value={keywordS} onChange={e => setKeywordS(e.target.value)} onKeyPress={(e) => { e.charCode === 13 ? hanlderEnter() : null }} />
                                            <button onClick={() => hanlderEnter()}>
                                                <img className="w-5 absolute top-0 right-0 mt-10 mr-8" src="../img/searchgray.svg" />
                                            </button>
                                        </div>
                                    </div>
                                    <div className="relative">
                                        <div className="px-3 cursor-pointer" onClick={(e) => handleClickUser(e)}>
                                            <img src="../../img/user.svg" alt="user" />
                                        </div>
                                        <div ref={userLRef} className={`absolute bg-gray-100 mt-12 h-auto w-72 top-0 pt-1 right-0 z-10 ${userL ? '' : 'hidden'}`}>
                                            <Link href="/myprofile">
                                                <a className="flex border-b-1  py-4 px-5">
                                                    <div className="mr-3">
                                                        <img src="../img/user-circle.svg" alt="user" />
                                                    </div>
                                                    <div>
                                                        <h5 className="capitalize text-ms font-semibold">{t('my_account')}</h5>
                                                    </div>
                                                </a>
                                            </Link>
                                            <Link href="/orders">
                                                <a className="flex pb-4 border-b-1  py-4  px-5">
                                                    <div className="mr-3">
                                                        <img src="../img/clipboard-list.svg" alt="user" />
                                                    </div>
                                                    <div>
                                                        <h5 className="capitalize text-ms font-semibold">{t('my_order')}</h5>
                                                    </div>
                                                </a>
                                            </Link>
                                            <Link href="/address">
                                                <a className="flex pb-4 border-b-1  py-4  px-5">
                                                    <div className="mr-3">
                                                        <img src="../img/map-marker-alt.svg" alt="user" />
                                                    </div>
                                                    <div>
                                                        <h5 className="capitalize text-ms font-semibold">{t('my_address')}</h5>
                                                    </div>
                                                </a>
                                            </Link>
                                            <Link href="/">
                                                <a className="flex pb-4 border-b-1 py-4  px-5" onClick={(e) => handleClickLogOut(e)}>
                                                    <div className="mr-3">
                                                        <img src="../img/sign-out-alt.svg" alt="user" />
                                                    </div>
                                                    <div>
                                                        <h5 className="capitalize text-ms font-semibold">{t('logout')}</h5>
                                                    </div>
                                                </a>
                                            </Link>
                                        </div>
                                    </div>
                                    <Link href="/cart">
                                        <a className="relative">
                                            <div className="pl-3 pr-1 relative">
                                                <img src="../../img/cart.svg" alt="cart" />
                                            </div>
                                            {
                                                cartTotal > 0 &&
                                                <div className={` ${classNoti} noti-cart absolute rounded-full bg-white bottom-0 right-0 flex justify-center items-center text-xs font-bold -m-2 py-0-5 px-1-5`}>
                                                    {cartTotal}
                                                </div>
                                            }

                                        </a>
                                    </Link>
                                </div>

                                <div className="border-l-1 border-white h-10">

                                </div>
                                <div className="pl-7 relative">
                                    <div className="flex items-center cursor-pointer change-lang relative">
                                        <div className="uppercase text-white text-sm font-semibold mr-2">{i18n.language === 'vn' ? 'VNI' : 'ENG'}</div>
                                        <div><img src="../img/downarrow.svg" /></div>
                                    </div>
                                    <div className="absolute bg-white top-0 right-0 mt-5 change-popup text-xs font-semibold">
                                        {lang !== 'vn' && <div className="w-full px-4 py-2 cursor-pointer lang" onClick={() => i18n.changeLanguage('vn')}>{t('vn')}</div>}
                                        {lang !== 'en' && <div className="w-full px-4 py-2 cursor-pointer lang" onClick={() => i18n.changeLanguage('en')}>{t('en')}</div>}
                                    </div>
                                </div>
                            </div>
                            <div className="mr-2 flex md:hidden">
                                <div className="relative inline-flex items-center justify-center">
                                    <div className="pr-3 cursor-pointer" onClick={(e) => handleClickSearchMb(e)}>
                                        <img src="../../img/search.svg" alt="search" />
                                    </div>
                                    <div ref={searchRefMb} className={`absolute bg-gray-200 mt-14 h-24 w-80 -mr-20 top-0 right-0 z-10 px-5 py-5 flex ${searchMb ? 'md:hidden' : 'md:hidden hidden'}`}>
                                        <input className="bg-gray-200 w-full py-3 px-3 outline-none border-b-1 border-gray-400" placeholder={t('search')} value={keywordS} onChange={e => setKeywordS(e.target.value)} onKeyPress={(e) => { e.charCode === 13 ? hanlderEnter() : null }} />
                                        {/* <Link href={`/search?keyword=${keywordS}`}>
                                            <a>
                                               
                                            </a>
                                        </Link> */}
                                        <button onClick={() => hanlderEnter()}>
                                            <img className="w-5 absolute top-0 right-0 mt-10 mr-8" src="../img/searchgray.svg" />
                                        </button>
                                    </div>
                                </div>
                                <Link href="/cart">
                                    <a className="relative inline-flex items-center justify-center mr-3">
                                        <div className="pl-3 pr-1 relative">
                                            <img src="../../img/cart.svg" alt="cart" />
                                        </div>
                                        {
                                            cartTotal > 0 &&
                                            <div className={` ${classNoti} noti-cart absolute rounded-full bg-white bottom-0 right-0 flex justify-center items-center text-xs font-bold -mr-1 py-0-5 px-1-5`}>
                                                {cartTotal}
                                            </div>
                                        }
                                    </a>
                                </Link>
                                <button onClick={(e) => handleClickHeader(e)} className="text-gray-800 dark:text-white hover:text-gray-300 inline-flex items-center justify-center p-2 rounded-md focus:outline-none">

                                    <svg viewBox="0 0 60 32" width="32" height="32" fill="#ffffff">
                                        <rect width="60" height="3" rx="5"></rect>
                                        <rect y="16" width="60" height="3" rx="5"></rect>
                                        <rect y="32" width="60" height="3" rx="5"></rect>
                                    </svg>
                                </button>
                            </div>
                        </div>

                    </div>
                    <div className="md:hidden">
                        <div className={`px-2 pt-2 pb-3 space-y-1 sm:px-3 uppercase ${menu ? '' : 'hidden'} `}>

                            {/* <Link href="/products"><a className="visit_pr text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium pr_list dropdown-toggle relative">
                                {t('product')}</a>
                            </Link> */}
                            <div
                                onClick={() => setToogle(!toogle)}
                                className=" text-gray-300 hover:text-white dark:hover:text-gray-800 block px-3 py-2 rounded-md text-base font-medium pr_list dropdown-toggle relative">
                                {t('product')}
                            </div>
                            <div className={`ml-5 ${toogle ? '' : 'visit_pr_keep'} `}>
                                {
                                    category1?.map((item, index) => {
                                        return (
                                            <React.Fragment key={index}>
                                                <Link href={`/products/${item.slug}`}><a
                                                    onClick={() => { !checkChil(item) || checkHover.index === index ? closeMenu() : null, setCheckHover({ ...checkHover, index: checkHover.index === index ? -1 : index }) }} className={`text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium
                                            ${checkChil(item) ? 'pr_list dropdown-toggle relative visit_pr_keep_de' : ''}
                                            `}>
                                                    {item.name}</a>
                                                </Link>
                                                {checkChil(item) &&
                                                    <div className={`ml-5 ${checkHover?.index !== undefined && checkHover?.index === index ? '' : 'visit_pr_keep'}`}>

                                                        {

                                                            category2[item.id] !== undefined && category2[item.id]?.length > 0 &&
                                                            category2[item.id].sort((a, b) => a?.menu_order - b?.menu_order).map((item2, index2) => {
                                                                return (
                                                                    <Link href={`/products/${item2.slug}`} key={index2 + 'key2'}><a onClick={() => closeMenu()} className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                                                        {item2.name}</a>
                                                                    </Link>
                                                                )
                                                            })
                                                        }
                                                    </div>}
                                            </React.Fragment>
                                        )
                                    })
                                }
                                {/* <Link href="/products"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                    {t('product')}</a>
                                </Link>
                                <Link href="/products"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                    {t('product')}</a>
                                </Link>
                                <Link href="/products"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium 
                                pr_list dropdown-toggle relative visit_pr_keep_de 
                                ">
                                    {t('product')}</a>
                                </Link> */}
                                {/* <div className="ml-5">
                                    <Link href="/products"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                        {t('product')}</a>
                                    </Link>
                                    <Link href="/products"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                        {t('product')}</a>
                                    </Link>
                                </div> */}
                            </div>
                            <Link href="/saleoff"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                {t('sales')}</a></Link>
                            <Link href="/gallery"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                {t('gallery')}</a></Link>
                            {/* <Link href="/blog"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                {t('blog')}</a></Link> */}
                            <Link href="/about"><a className="text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                                {t('about')}</a></Link>

                            <div className={`${userL ? 'hidden' : ''}text-gray-300 hover:text-gray-800 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium`} onClick={(e) => handleClickUser2(e)}>
                                {auth.token === '' ? t('login') : t('my_account')}
                            </div>
                            <div className="lang-list dropdown-toggle text-gray-300 hover:text-gray-300 dark:hover:text-white block px-3 py-2 rounded-md text-base font-medium relative" onClick={(e) => handleClickOpenLang(e)}>
                                {t('language')}: {i18n.language === 'vn' ? t('vn') : t('en')}
                            </div>
                            {openLang &&
                                <>
                                    {lang !== 'vn' && <div className="text-gray-300 w-full px-4 py-2 cursor-pointer lang" onClick={() => { i18n.changeLanguage('vn'), UIStore.update(s => { s.language = 'vn' }) }}>{t('vn')}</div>}
                                    {lang !== 'en' && <div className="text-gray-300 w-full px-4 py-2 cursor-pointer lang" onClick={() => { i18n.changeLanguage('en'), UIStore.update(s => { s.language = 'en' }) }}>{t('en')}</div>}
                                </>
                            }
                        </div>
                    </div>
                </nav>
            </div>
            <FormModel open={isOpen} getIsOpen={(open) => getIsOpen(open)} />
        </>
    )
}));