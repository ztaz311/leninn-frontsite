
import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
export const Loading = memo(() => {
    return (
        <>
            <div className="pt-12">
                <div className="flex justify-center">
                    <img className="donut" src="../img/Bitmap.svg" alt="Bitmap" />
                </div>
            </div>
        </>
    )
});