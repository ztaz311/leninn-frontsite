export const formatPrice = (x) => {
    // x = x.toString();
    // var pattern = /(-?\d+)(\d{3})/;
    // while (pattern.test(x))
    //     x = x.replace(pattern, "$1,$2");
    // return x;
    return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(x)
}

function checkZero(data) {
    if (data.length == 1) {
        data = "0" + data;
    }
    return data;
}
export const formatDate = (date) => {
    var today = new Date(date);
    var day = today.getDate() + "";
    var month = (today.getMonth() + 1) + "";
    var year = today.getFullYear() + "";
    var hour = today.getHours() + "";
    var minutes = today.getMinutes() + "";
    var seconds = today.getSeconds() + "";

    day = checkZero(day);
    month = checkZero(month);
    year = checkZero(year);
    hour = checkZero(hour);
    minutes = checkZero(minutes);
    seconds = checkZero(seconds);
    return day + "/" + month + "/" + year + " " + hour + ":" + minutes + ":" + seconds
}