import React, { memo, useState, useRef, useEffect } from 'react';

export const CoverTime = memo((props) => {
    const [date, setDate] = useState(props.datatime)

    const CoverDate = () => {
        let d = new Date(date);
        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        let mo = new Intl.DateTimeFormat('en', { month: 'numeric' }).format(d);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        let dateN = (`${da.length > 1 ? da : '0' + da}.${mo.length > 1 ? mo : '0' + mo}.${ye}`);
        setDate(dateN)
    }
    useEffect(() => {
        if (!date || date.length < 11) {
        } else {
            CoverDate();
        }
    }, [props.time])
    return (
        <div>
            {date}
        </div>
    );
});
