import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
export const Toss = memo((props) => {
    return (
        <>
            <div className="top-0 right-0 mt-20 mr-5 w-48 h-auto bg-white fixed model-view-cart rounded border border-1 border-gray-400">
                <div className="border-b-1 border-gray-300 py-2 px-3 flex justify-between text-sm items-center">
                    <div className="font-semibold">
                        {props.t('add_to_cart')}
                    </div>
                    <button className="cursor-pointer font-semibold text-xl" onClick={props.onClose}>
                        ×
                    </button>
                </div>
                <div>
                    <div className="py-3">
                        <div className="flex ">
                            {/* <img className="w-16 mr-2" src="../img/img_sp.png" alt="xx" /> */}
                            <img className="w-16 mr-2" src={props.data.image + '?morph=300x300'} alt="xx" />
                            <div className="font-semibold text-xs">
                                <span>{props.data.quantily}</span>
                                <span> x {props.data.name}</span>

                            </div>
                        </div>
                        <Link href="/cart">
                            <a>
                                <div className="mt-4 px-4">
                                    <div className="py-1 border border-1 border-gray-900 text-sm font-semibold rounded flex justify-center cursor-pointer hover:bg-black hover:text-white">{props.t('view_cart')}</div>
                                </div>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
});