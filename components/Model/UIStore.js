import { Store } from "pullstate";

export const UIStore = new Store({
    logo: '',
    checkChangePassword: false,
    language: 'vn',
    loginType: '',
    providers: [],
    classNoti: '',
    cart: {
        total: 0,
        data: [],
    },
    category: [],
    // price: {
    //     totalFetch: 0,
    //     maKM: 0,
    //     vat: 0,
    //     totalAll: 0,
    //     priceVAT: 0
    // },
    auth: {
        token: ''
    },
    profile: {},
    order: {}

});
export const UIProduct = new Store({
    DATA_PRODUCTS_ALL_REQUEST: [],
    DATA_PRODUCTS_REQUEST: [],
    DATA_PRODUCTS_HOME: [],
});
export const UICategory = new Store({
    DATA_CATEGORY_REQUEST: []
});
export const UIPost = new Store({
    DATA_POSTS_ALL_REQUEST: [],
    DATA_POSTS_REQUEST: [],
    DATA_POSTS_HOME: [],
});
export const UIGallery = new Store({
    DATA_GALLERY_ALL_REQUEST: [],
    DATA_GALLERY_REQUEST: [],
});
export const UIAbout = new Store({
    DATA_ABOUT_REQUEST: '',
});
export const UIAcf = new Store({
    DATA_ACF_REQUEST: [],
});
// https://leninnapi.appdemo.dev/wp-json/acf/v3/options/logo-client