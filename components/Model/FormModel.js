import { signIn, signOut, useSession } from 'next-auth/client';
import React, { memo, useEffect, useRef, useState } from 'react';
import { useToasts } from 'react-toast-notifications';
import { v4 } from 'uuid';
import { actLoginFacebook, actLoginGoogle, actSendEmail } from '../../action/auth';
import { qrLogin, qrRegister, loginWithDiff, resendEmail } from '../../constants/querry';
import { withTranslation } from "../../i18n";
import { LoadingAll } from "../Aside/LoadingAll";
import { UIStore } from "../Model/UIStore";
// import NewWindow from 'react-new-window'
import NewWindow from '../NewWindow';
import { useRouter } from 'next/router'

const base64 = require('base-64');
export const FormModel = withTranslation('common')(memo((props) => {
    const { addToast } = useToasts();
    const [isOpen, setIsOpen] = useState(false)
    const language = UIStore.useState(s => s.language)
    const router = useRouter()

    useEffect(() => {
        setIsOpen(props.open);
    }, [props.open]);
    // console.log('Privider',)
    const providers = UIStore.useState(s => s.providers)
    // console.log('providers', providers)
    const [popup, setPopUp] = useState(false)
    const handleSignin = (e) => {
        e.preventDefault()
        signIn()
    }
    const handleSignout = (e) => {
        e.preventDefault()
        signOut()
    }
    // 
    const [login, setLogin] = useState('')
    const [session] = useSession();
    // console.log('session', session)
    const loginType = UIStore.useState(s => s.loginType)
    const auth = UIStore.useState(s => s.auth)
    // console.log('auth', auth)




    useEffect(() => {
        if (auth.token === "") {
            if (session?.accessToken !== undefined) {
                if (loginType === 'facebook') {
                    // console.log('token', session?.accessToken);
                    loginWithDiff('facebook', session?.accessToken).then(res => {
                        // console.log('res', res.data.data.socialLogin)
                        if (res.data?.data?.socialLogin !== null) {
                            res.data.data.socialLogin.token = res.data.data.socialLogin.authToken
                            setErr({ ...err, login: '' })
                            if (dataSignIn.checkBox) {
                                document.cookie = `auth=${JSON.stringify(res.data.data.socialLogin)}; expires=" + ${expires(1000 * 60 * 60 * 24 * 7 * 4 * 6)} + ";path=/;`
                            } else {
                                document.cookie = `auth=${JSON.stringify(res.data.data.socialLogin)}; expires=" + ${expires(1000 * 60 * 60 * (1 / 2))} + ";path=/;`
                            }
                            UIStore.update(s => { s.auth = res.data.data.socialLogin });
                            setIsOpen(false);
                            props.getIsOpen(false);
                            addToast(props.t('login_success'), { appearance: 'success' });
                        } else {
                            console.log('12');
                        }
                    })

                    // actLoginFacebook(session?.accessToken).then(data => {
                    //     if (data.status === 200) {
                    //         setErr({ ...err, login: '' })
                    //         //    localStorage.setItem('auth', JSON.stringify(data));
                    //         if (dataSignIn.checkBox) {
                    //             document.cookie = `auth=${JSON.stringify(data)}; expires=" + ${expires(1000 * 60 * 60 * 24 * 7 * 4 * 6)} + ";path=/;`
                    //         } else {
                    //             document.cookie = `auth=${JSON.stringify(data)}; expires=" + ${expires(1000 * 60 * 60 * 24)} + ";path=/;`
                    //         }
                    //         UIStore.update(s => { s.auth = data });
                    //         setIsOpen(false);
                    //         props.getIsOpen(false);
                    //         addToast(props.t('login_success'), { appearance: 'success' });
                    //     }
                    // })
                } else if (loginType === 'google') {
                    loginWithDiff('google', session?.accessToken).then(res => {
                        // console.log('res', res.data.data.socialLogin)
                        // console.log('res2', session?.accessToken)
                        if (res.data?.data?.socialLogin !== null) {
                            res.data.data.socialLogin.token = res.data.data.socialLogin.authToken
                            setErr({ ...err, login: '' })
                            if (dataSignIn.checkBox) {
                                document.cookie = `auth=${JSON.stringify(res.data.data.socialLogin)}; expires=" + ${expires(1000 * 60 * 60 * 24 * 7 * 4 * 6)} + ";path=/;`
                            } else {
                                document.cookie = `auth=${JSON.stringify(res.data.data.socialLogin)}; expires=" + ${expires(1000 * 60 * 60 * (1 / 2))} + ";path=/;`
                            }
                            UIStore.update(s => { s.auth = res.data.data.socialLogin });
                            setIsOpen(false);
                            props.getIsOpen(false);
                            addToast(props.t('login_success'), { appearance: 'success' });
                        } else {
                            console.log('1');
                        }
                    })



                    // actLoginGoogle(session?.accessToken).then(data => {
                    //     if (data.status === 200) {
                    //         setErr({ ...err, login: '' })
                    //         //    localStorage.setItem('auth', JSON.stringify(data));
                    //         if (dataSignIn.checkBox) {
                    //             document.cookie = `auth=${JSON.stringify(data)}; expires=" + ${expires(1000 * 60 * 60 * 24 * 7 * 4 * 6)} + ";path=/;`
                    //         } else {
                    //             document.cookie = `auth=${JSON.stringify(data)}; expires=" + ${expires(1000 * 60 * 60 * 24)} + ";path=/;`
                    //         }
                    //         UIStore.update(s => { s.auth = data });
                    //         setIsOpen(false);
                    //         props.getIsOpen(false);
                    //         addToast(props.t('login_success'), { appearance: 'success' });
                    //     }
                    // })

                } else {

                }
                document.cookie = `next-i18next=${language}; expires=Session	; path=/; samesite=Strict`;

            }
        }
    }, [session?.accessToken])
    const [tab, setTab] = useState(true)
    const modelRef = useRef(null);
    const [eye0, setEsey0] = useState(true);
    const [eye1, setEsey1] = useState(true);
    const [eye2, setEsey2] = useState(true);
    const [loadingAll, setLoadingAll] = useState(false);
    const expires = (data) => {
        // 1000 * 60 * 60 * 24 * 7 * 4 * 6
        var timeToAdd = data
        var date = new Date();
        var expiryTime = parseInt(date.getTime()) + timeToAdd;
        date.setTime(expiryTime);
        return date.toUTCString();
    }
    const onLogin = (username, password) => {
        setLoadingAll(true)



        // actLogin({ username, password, clientMutationId: v4() }).then(data => {
        //     setLoadingAll(false)
        //     if (data.status === 403) {
        //         setErr({ ...err, login: props.t('incorrect_password') })
        //     }
        //     if (data.status === 200) {

        //         setErr({ ...err, login: '' })
        //         //    localStorage.setItem('auth', JSON.stringify(data));
        //         if (dataSignIn.checkBox) {
        //             document.cookie = `auth=${JSON.stringify(data)}; expires=" + ${expires(1000 * 60 * 60 * 24 * 7 * 4 * 6)} + ";path=/;`
        //         } else {
        //             document.cookie = `auth=${JSON.stringify(data)}; expires=" + ${expires(1000 * 60 * 60 * 24)} + ";path=/;`
        //         }
        //         UIStore.update(s => { s.auth = data });
        //         setIsOpen(false);
        //         props.getIsOpen(false);
        //         addToast(props.t('login_success'), { appearance: 'success' });
        //     }
        // })

        qrLogin(username, password, v4()).then(data => {
            setLoadingAll(false)
            if (data?.data?.data?.login?.authToken !== undefined) {
                data.data.data.login.token = data.data.data.login.authToken
                setErr({ ...err, login: '' })
                // console.log('data?.data?.data?.login', data?.data?.data?.login)
                if (dataSignIn.checkBox) {
                    document.cookie = `auth=${JSON.stringify(data?.data?.data?.login)}; expires=" + ${expires(1000 * 60 * 60 * 24 * 7 * 4 * 6)} + ";path=/;`
                } else {
                    document.cookie = `auth=${JSON.stringify(data?.data?.data?.login)}; expires=" + ${expires(1000 * 60 * 60 * (1 / 2))}  + ";path=/;`
                }
                UIStore.update(s => { s.auth = data?.data?.data?.login });
                setIsOpen(false);
                props.getIsOpen(false);
                addToast(props.t('login_success'), { appearance: 'success' });
            } else {
                setErr({ ...err, login: props.t('incorrect_password') })
            }
        })

        // UIStore.update(s => { s.auth = true });
        // setIsOpen(false);
        // props.getIsOpen(false);
    }
    const checkLoginType = (value) => {
        UIStore.update(s => { s.loginType = value })
    }
    const handleClickAuth = (e) => {
        let email = ''
        let password = ''
        if (dataSignIn.email === '') {
            email = props.t('email_required')
        } else if (!validateEmail(dataSignIn.email)) {
            email = props.t('incorrect_email_format')
        } else {
            email = ''
        }

        if (dataSignIn.password === '') {
            password = props.t('password_required')
        } else if (dataSignIn.password < 6) {
            password = props.t('password_must')
        } else {
            password = ''
        }


        setErrSignIn({ ...err, email, password })

        if (email === '' && password === '') {
            onLogin(dataSignIn.email, dataSignIn.password)
        }

    }
    const handleClickClose = (e) => {
        setIsOpen(false);
        props.getIsOpen(false);
        UIStore.update(s => { s.checkChangePassword = false })
    }
    const handleClickClose2 = (e) => {
        setIsOpenEmail(false);
    }
    const handleClicksetEsey0 = (e) => {
        setEsey0(!eye0);
    }
    const handleClicksetEsey1 = (e) => {
        setEsey1(!eye1);
    }
    const handleClicksetEsey2 = (e) => {
        setEsey2(!eye2);
    }
    const handleClickTabDk = (e) => {
        setTab(false);
    }
    const handleClickTabDn = (e) => {
        setTab(true);
    }
    useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (modelRef.current && !modelRef.current.contains(event.target)) {
                setIsOpen(false);
                setIsOpenEmail(false)
                props.getIsOpen(false);
                UIStore.update(s => { s.checkChangePassword = false })

            }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [modelRef]);
    useEffect(() => {
        return () => { setLogin(''), setPopUp(false) }
    }, [])

    const [dataSignUp, setDataSignUp] = useState({
        nameSU: '',
        emailSU: '',
        passwordSU: '',
        checkPasswordSU: '',
        checkBoxSU: false
    })
    const [dataSignIn, setDataSignIn] = useState({
        email: '',
        password: '',
        checkBox: false
    })
    const [errSignUp, setErrSignUp] = useState({
        name: '',
        email: '',
        password: '',
        checkPassword: '',
        checkbox: '',

    })
    const [errSignIn, setErrSignIn] = useState({
        email: '',
        password: '',

    })
    const [err, setErr] = useState({
        login: '',
        register: ''
    })

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const validateSignUp = () => {
        let name = ''
        let email = ''
        let password = ''
        let checkPassword = ''
        let checkbox = ''

        if (dataSignUp.nameSU === '') {
            name = props.t('name_required')
        } else {
            name = ''
        }

        if (dataSignUp.emailSU === '') {
            email = props.t('email_required')
        } else if (!validateEmail(dataSignUp.emailSU)) {
            email = props.t('incorrect_email_format')
        } else {
            email = ''
        }

        if (dataSignUp.passwordSU === '') {
            password = props.t('password_required')
        } else if (dataSignUp.passwordSU.length < 6) {
            password = props.t('password_must')
        } else {
            password = ''
        }

        if (dataSignUp.checkPasswordSU === '') {
            checkPassword = (props.t('confirm_password_required'))
        } else if (dataSignUp.checkPasswordSU !== dataSignUp.passwordSU) {
            checkPassword = (props.t('compare_password'))
        } else {
            checkPassword = ''
        }

        if (!dataSignUp.checkBoxSU) {
            checkbox = (props.t('you_must_agree_to_the_terms'))
        } else {
            checkbox = ''
        }

        setErrSignUp({ name, email, password, checkPassword, checkbox })
        if (name === '' && email === '' && password === '' && checkPassword === '' && checkbox === '') {
            return true
        } else {
            return false
        }

    }

    const onChangeSignIn = (e) => {
        const target = e.target
        const name = target.name
        var value = ''
        if (name !== "checkBoxSU") {
            value = target.value
        } else {
            value = target.checked
        }
        // if()
        setDataSignUp({
            ...dataSignUp,
            [name]: value
        })
    }

    const onOpenPrivacyPolicy = () => {
        setIsOpen(false)
        if (props.i18n.language === 'en') {
            router.push('/privacy-policy')
        } else {
            router.push('/chinh-sach-bao-mat')
        }

    }

    const onOpenTerms = () => {
        setIsOpen(false)
        if (props.i18n.language === 'en') {
            router.push('/return-policy')
        } else {
            router.push('/chinh-sach-doi-tra')
        }

    }

    const register = async () => {
        if (validateSignUp()) {
            await setLoadingAll(true)
            await qrRegister(dataSignUp.nameSU, dataSignUp.passwordSU, dataSignUp.emailSU).then(res => {
                setLoadingAll(false)
                if (res?.data?.data?.registerUser?.user?.email !== undefined) {
                    onLogin(dataSignUp.emailSU, dataSignUp.passwordSU)
                } else {
                    setErr({ ...err, register: props.t('account_already_exists') })
                }
            })
            // await actRegister({ username: dataSignUp.nameSU, email: dataSignUp.emailSU, password: dataSignUp.passwordSU, password_again: dataSignUp.checkPasswordSU }).then(data => {
            //     if (data.status === 200) {
            //         onLogin(dataSignUp.emailSU, dataSignUp.passwordSU)
            //         setLoadingAll(false)
            //     } else if (data.status === 406) {
            //         setErr({ ...err, register: props.t('account_already_exists') })
            //         setLoadingAll(false)
            //     } else {
            //         setErr({ ...err, register: props.t('there_is_an_error') })
            //         setLoadingAll(false)
            //     }
            // })
        } else {
            console.log('Registration failed')
        }
    }
    const { t } = props

    const [email, setEmail] = useState('')
    const [emailErr, setEmailErr] = useState('')
    const [isOpenEmail, setIsOpenEmail] = useState(false)
    const findEmail = () => {
        setIsOpen(!isOpen)
        setIsOpenEmail(!isOpenEmail)
        props.getIsOpen(false);

    }
    const findEmail2 = () => {
        if (email === '') {

            setEmailErr(props.t('email_required'))
        } else if (!validateEmail(email)) {
            setEmailErr(props.t('incorrect_email_format'))
        } else {
            setEmailErr('')
            // console.log('email', email);
            resendEmail(email).then(res => {
                // console.log('res', res)
                addToast(props.t('send_mail'), { appearance: 'success' });
            })
            // actSendEmail(email).then(data => {
            //     if (data.status === 200) {
            //         addToast(props.t('send_mail'), { appearance: 'success' });
            //     } else {
            //         addToast(props.t('error'), { appearance: 'error' });
            //     }
            // })

        }

    }


    return (
        <>

            {/* <Transition
                appear={true}
                show={isOpen}
                enter="transition-opacity ease-linear duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="transition-opacity ease-linear duration-300"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
            > */}
            {isOpen &&
                <div className="fixed z-100 inset-0 overflow-y-auto">
                    <div className="flex items-end justify-center min-h-screen pt-4 pb-20 text-center sm:block sm:p-0">
                        <div className="fixed inset-0 transition-opacity" aria-hidden="true">
                            <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
                        </div>
                        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                        <div ref={modelRef} className=" inline-block align-bottom bg-white text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                            <div className="bg-white  pt-6 pb-6 ">
                                <div className="flex justify-between border-b-1 border-gray-300 px-8">
                                    <div className="flex font-semibold md:text-2xl">
                                        <div className={`md:mr-12 mr-6 pb-6 ${tab ? 'border-black border-b-4' : 'opacity-40'} cursor-pointer`} onClick={(e) => handleClickTabDn(e)}>{t('login')}</div>
                                        <div className={`mx-5 pb-6 ${tab ? 'opacity-40' : 'border-black border-b-4'} cursor-pointer`} onClick={(e) => handleClickTabDk(e)}>{t('register')}</div>
                                    </div>
                                    <div>
                                        <button onClick={(e) => handleClickClose(e)}><img src="../img/x_close_model.svg" alt="ss" /></button>
                                    </div>
                                </div>
                                <div className={`giang ${tab ? '' : 'hidden'} `}>
                                    <div className="pt-6 pb-6 mx-8">
                                        <div className="grid md:grid-cols-2 grid-cols-1 gap-8">

                                            <div className="">
                                                <div className="text-base font-semibold mb-3">Email</div>
                                                <input className="px-6 py-4 w-full border border-gray-300" placeholder={t('enter_email')} value={dataSignIn.email} onChange={(e) => setDataSignIn({ ...dataSignIn, email: e.target.value })} />
                                                <div className="text-red-500">{errSignIn.email}</div>
                                            </div>
                                            <div className="relative">
                                                <div className="text-base font-semibold mb-3">{t('password')}</div>
                                                <div className="relative">
                                                    <input type={eye0 ? 'password' : 'text'} className="px-6 py-4 w-full border border-gray-300" placeholder={t('enter_password')} value={dataSignIn.password} onChange={(e) => setDataSignIn({ ...dataSignIn, password: e.target.value })} />
                                                    <img className="absolute top-0 right-0 mt-5 mr-3 cursor-pointer" onClick={(e) => handleClicksetEsey0(e)} src={`../img/${eye0 ? 'eye.png' : 'eye2.png'}`} alt="esey" />
                                                </div>
                                                <div className="text-red-500">{errSignIn.password}</div>
                                            </div>
                                        </div>
                                        <div className="grid md:grid-cols-2 grid-cols-1 md:gap-8 mt-5">
                                            <div className="flex items-center order-last md:order-first mt-4 md:mt-0">
                                                <label className="label-nho text-base">{t('remember_me')}
                                                    <input type="checkbox" onChange={(e) => setDataSignIn({ ...dataSignIn, checkBox: e.target.checked })} />
                                                    <span className="checkmark rounded"></span>
                                                </label>
                                            </div>
                                            <div className="flex justify-self-end cursor-pointer">
                                                <div onClick={() => findEmail()}>
                                                    <a>
                                                        {t('forgot_password')}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex flex-col items-center md:mt-9 mt-3">
                                            {err.login !== '' && <div className="text-red-500">{err.login}</div>}
                                            <button className=" text-lg px-14 py-3 bg-black text-white font-bold uppercase" onClick={(e) => handleClickAuth(e)}>{t('login')}</button>
                                        </div>
                                    </div>
                                    <hr className="mx-8 mt-12" />

                                    <div className="pt-6 pb-3 mx-8">
                                        <h6 className="mb-3">{t('login_with')}</h6>
                                        <div className="grid md:grid-cols-2 grid-cols-1 gap-8">
                                            <div>
                                                <button className="text-sm flex items-center justify-center font-semibold text-white bg-blue-800 py-4 w-full"
                                                    onClick={() => { setPopUp(true), setLogin(providers.facebook.id), checkLoginType('facebook') }}
                                                // onClick={() => signIn(providers.facebook.id)}
                                                >
                                                    <img className="mr-3" src="../img/fb.svg" alt="dsd" />
                                                    <span>{t('login_faceook')}</span>
                                                </button>
                                            </div>
                                            <div>
                                                <button className="text-sm flex items-center justify-center font-semibold bg-white text-gray-900 py-4 w-full border border-gray-300"
                                                    onClick={() => { setPopUp(true), setLogin(providers.google.id), checkLoginType('google') }}
                                                // onClick={() => signIn(providers.google.id)}
                                                >
                                                    <img className="mr-3" src="../img/gg.svg" alt="dsd" />
                                                    <span>{t('login_google')}</span>
                                                </button>
                                            </div>
                                            {/* <button className="text-sm flex items-center justify-center font-semibold bg-white text-gray-900 py-4 w-full border border-gray-300"
                                                onClick={() => { signOut() }}
                                            // onClick={() => signIn(providers.google.id)}
                                            >
                                                <img className="mr-3" src="../img/gg.svg" alt="dsd" />
                                                <span>{t('login_google')}</span>
                                            </button> */}
                                        </div>
                                    </div>
                                </div>
                                <div className={`giang ${tab ? 'hidden' : ''} `}>

                                    <div className="pt-6 pb-6 mx-8">
                                        <div className="grid md:grid-cols-2 grid-cols-1 gap-8">
                                            <div className="">
                                                <div className="text-base font-semibold mb-3">{t('full_name')}</div>
                                                <input className="px-6 py-4 w-full border border-gray-300" placeholder={t('enter_full_name')} onChange={(e) => onChangeSignIn(e)} name="nameSU" />
                                                <div className="text-red-500">{errSignUp.name}</div>
                                            </div>
                                            <div className="relative">
                                                <div className="text-base font-semibold mb-3">Email</div>
                                                <input className="px-6 py-4 w-full border border-gray-300" placeholder={t('enter_email')} onChange={(e) => onChangeSignIn(e)} name="emailSU" />
                                                <div className="text-red-500">{errSignUp.email}</div>
                                            </div>
                                        </div>
                                        <div className="grid md:grid-cols-2 grid-cols-1 gap-8 mt-6">
                                            <div className="">
                                                <div className="text-base font-semibold mb-3">{t('password')}</div>
                                                <div className="relative">
                                                    <input type={eye1 ? 'password' : 'text'} className="px-6 py-4 w-full border border-gray-300" placeholder={t('enter_password')} onChange={(e) => onChangeSignIn(e)} name="passwordSU" />
                                                    <img className="absolute top-0 right-0 mt-5 mr-3 cursor-pointer" onClick={(e) => handleClicksetEsey1(e)} src={`../img/${eye1 ? 'eye.png' : 'eye2.png'}`} alt="esey" />
                                                    <div className="text-red-500">{errSignUp.password}</div>

                                                </div>
                                            </div>
                                            <div className="relative">
                                                <div className="text-base font-semibold mb-3">{t('confirm_password')}</div>
                                                <div className="relative">
                                                    <input type={eye2 ? 'password' : 'text'} className="px-6 py-4 w-full border border-gray-300" placeholder={t('enter_password')} onChange={(e) => onChangeSignIn(e)} name="checkPasswordSU" />
                                                    <img className="absolute top-0 right-0 mt-5 mr-3 cursor-pointer" onClick={(e) => handleClicksetEsey2(e)} src={`../img/${eye2 ? 'eye.png' : 'eye2.png'}`} alt="esey" />
                                                    <div className="text-red-500">{errSignUp.checkPassword}</div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="grid grid-cols-1 gap-8 mt-5">
                                            <div className="flex items-center">
                                                <label className="label-nho text-base"> {t('i_agree_with')}
                                                    <span onClick={onOpenPrivacyPolicy} className="font-semibold"> {t('privacy_policy')}</span>&nbsp;
                                                    {t('and')}&nbsp;
                                                    <span onClick={onOpenTerms} className="font-semibold">{t('terms')}</span>&nbsp;
                                                    {t('ours')}&nbsp;
                                                    <div className="text-red-500">{errSignUp.checkbox}</div>

                                                    <input type="checkbox" name="checkBoxSU" onClick={(e) => onChangeSignIn(e)} />
                                                    <span className="checkmark rounded"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div className="flex flex-col items-center md:mt-9 mt-3 ">
                                            {err.register !== '' && <div className="text-red-500">{err.register}</div>}
                                            <button className=" text-lg px-14 py-3 bg-black text-white font-bold uppercase" onClick={() => register()}>{t('register')}</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            }

            {isOpenEmail &&
                <div className="fixed z-100 inset-0 overflow-y-auto">
                    <div className="flex items-end justify-center min-h-screen pt-4 pb-20 text-center sm:block sm:p-0">
                        <div className="fixed inset-0 transition-opacity" aria-hidden="true">
                            <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
                        </div>
                        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                        <div ref={modelRef} className=" inline-block align-bottom bg-white text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                            <div className="bg-white  pt-6 pb-6 ">
                                <div className="flex justify-between border-b-1 border-gray-300 px-8">
                                    <div className="flex font-semibold md:text-2xl">
                                        <div className={`md:mr-12 mr-6 pb-6 'border-black border-b-4' cursor-pointer`} >{t('forgot_password')}</div>
                                    </div>
                                    <div>
                                        <button onClick={(e) => handleClickClose2(e)}><img src="../img/x_close_model.svg" alt="ss" /></button>
                                    </div>
                                </div>
                                <div className={`giang ${tab ? '' : 'hidden'} `}>
                                    <div className="pt-6 pb-6 mx-8">
                                        <div className="grid md:grid-cols-6 grid-cols-1 gap-8">
                                            <div className="md:col-span-4">
                                                <div className="text-base font-semibold mb-3">{t('input_email_full')}</div>
                                                <input className="px-6 text-lg py-4 w-full border border-gray-300" placeholder={t('input_email')} value={email} onChange={(e) => setEmail(e.target.value)} />
                                                <div className="text-red-500">{emailErr}</div>
                                            </div>
                                            <div className="flex flex-col items-center md:mt-9 mt-3 md:col-span-2">
                                                <button className="text-lg w-full py-4 bg-black text-white font-bold uppercase" onClick={(e) => findEmail2(e)}>{t('send')}</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="mx-8 mt-6" />
                                    <div className="pt-6 pb-3 mx-8">
                                        <h6 className="mb-3">{t('login_with')}</h6>
                                        <div className="grid md:grid-cols-2 grid-cols-1 gap-8">
                                            <div>
                                                <button className="text-sm flex items-center justify-center font-semibold text-white bg-blue-800 py-4 w-full"
                                                    onClick={() => { setPopUp(true), setLogin(providers.facebook.id), checkLoginType('facebook') }}
                                                // onClick={() => signIn(providers.facebook.id)}
                                                >
                                                    <img className="mr-3" src="../img/fb.svg" alt="dsd" />
                                                    <span>{t('login_faceook')}</span>
                                                </button>
                                            </div>
                                            <div>
                                                <button className="text-sm flex items-center justify-center font-semibold bg-white text-gray-900 py-4 w-full border border-gray-300"
                                                    onClick={() => { setPopUp(true), setLogin(providers.google.id), checkLoginType('google') }}
                                                // onClick={() => signIn(providers.google.id)}
                                                >
                                                    <img className="mr-3" src="../img/gg.svg" alt="dsd" />
                                                    <span>{t('login_google')}</span>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
            {/* </Transition> */}

            {popup && !session ? (
                <NewWindow url={`/sign-in/${login}`} onUnload={() => setPopUp(false)} center='screen' />
            ) : null}
            {loadingAll && <LoadingAll />}
        </>
    )

}));
