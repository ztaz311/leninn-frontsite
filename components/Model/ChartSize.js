import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
export const ChartSize = memo((props) => {

    return (
        <>
            <div className={`fixed z-50 top-0 left-0 right-0 bottom-0 flex justify-center items-center bg-gray-38383880 chart-size`}>
                <div ref={props.wrapperModelRef} className="bg-white min-h-xl relative max-w-3xl w-full">
                    <button className="md:mx-8 mx-4 my-5 md:my-9 absolute top-0 right-0 flex justify-center items-center cursor-pointer z-1000" onClick={props.closeModal}>
                        <img className="w-6 h-6" src="../img/x_close_model.svg" />
                    </button>
                    <div className="md:pt-16 pt-10 relative">
                        {/* <div className="md:px-11 mt-3 px-4">
                            <div className="font-semibold text-lg">
                                {props.t('instructions_for_choosing_a_size')}
                            </div>
                            <div className="mt-6 mb-12">
                                <div className="flex justify-center bg-gray-200 text-sm py-4 font-semibold">
                                    <div className="w-4/12 uppercase flex justify-center">
                                        size
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        {props.t('height')} (cm)
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        {props.t('weight')} (kg)
                                    </div>
                                </div>
                                <div className="flex justify-center text-sm py-4 font-semibold">
                                    <div className="w-4/12 uppercase flex justify-center">
                                        xxs
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        150-159
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        50-70
                                    </div>
                                </div>
                                <div className="flex justify-center bg-gray-200 text-sm py-4 font-semibold">
                                    <div className="w-4/12 uppercase flex justify-center">
                                        xxs
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        150-159
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        50-70
                                    </div>
                                </div>
                                <div className="flex justify-center text-sm py-4 font-semibold">
                                    <div className="w-4/12 uppercase flex justify-center">
                                        xxs
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        150-159
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        50-70
                                    </div>
                                </div>
                                <div className="flex justify-center bg-gray-200 text-sm py-4 font-semibold">
                                    <div className="w-4/12 uppercase flex justify-center">
                                        xxs
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        150-159
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        50-70
                                    </div>
                                </div>
                                <div className="flex justify-center text-sm py-4 font-semibold">
                                    <div className="w-4/12 uppercase flex justify-center">
                                        xxs
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        150-159
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        50-70
                                    </div>
                                </div>
                                <div className="flex justify-center bg-gray-200 text-sm py-4 font-semibold">
                                    <div className="w-4/12 uppercase flex justify-center">
                                        xxs
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        150-159
                                    </div>
                                    <div className="w-4/12 uppercase flex justify-center">
                                        50-70
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        <img src={props.data} className="my-5 px-2" />
                    </div>
                </div>
            </div>
        </>
    )
});