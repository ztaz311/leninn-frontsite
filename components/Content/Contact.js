import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import { ItemBlog } from './ItemBlog'
import renderHTML from 'react-render-html';

export const Contact = memo((props) => {
    return (
        <>
            <div className="container-fix">
                <div className="px-4">
                    <div className="mt-16">
                        <div className="flex justify-center text-4xl font-semibold">
                            <div>{props.data.title?.rendered}</div>
                        </div>
                        <div className="max-w-3xl mx-auto mt-12">
                            <div className="md:mt-16 mt-8">
                                <div className="content">
                                    {renderHTML(props.data.content?.rendered)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
});