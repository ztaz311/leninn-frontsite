import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import { ItemProduct } from './ItemProduct'
import { actGetListProduct } from '../../action/product'
import { Loading } from "../Aside/Loading";
import { UIProduct } from "../Model/UIStore";
export const ListProductHome = memo((props) => {

    return (
        <>
            <div className="container-fix w-full">
                {/* {loading && <Loading />} */}

                <div className="px-4">
                    <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
                        {
                            props.data.length > 0 && props.data.map((contest, idx) => {
                                return <ItemProduct key={idx} data={contest} checkCDN={true} />
                            })
                        }
                    </div>
                </div>
            </div>
        </>
    )
});
