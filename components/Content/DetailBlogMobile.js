import React from 'react';
import { useKeenSlider } from "keen-slider/react"
import { ItemBlog } from './ItemBlog'

function DetailBlogMobile(props) {
    const [sliderRef, slider] = useKeenSlider({
        loop: true,

    })
    function ArrowLeft(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (
            <img onClick={props.onClick} className="absolute top-1/3 md:w-14 md:h-14 w-10 h-10 cursor-pointer md:-ml-8 transform -translate-y-1/2" src="../img/left-qa.svg" alt="right" />
        )
    }

    function ArrowRight(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (
            <img onClick={props.onClick} className="absolute top-1/3 md:w-14 md:h-14 w-10 h-10 cursor-pointer md:-mr-8 transform -translate-y-1/2 left-auto right-0" src="../img/right-qa.svg" alt="right" />
        )
    }

    return (
        <div ref={sliderRef} className="keen-slider">
            {

                props.itemPostsRelate.map((content, keys) => {
                    return (
                        <div className="keen-slider__slide" keys={keys + '_mobile'}>
                            {/* <img src={`${content.src}`} alt="gallyry" /> */}
                            <ItemBlog key={keys} data={content} />
                        </div>
                    )
                })
            }
            {
                <>
                    <ArrowLeft
                        onClick={(e) => e.stopPropagation() || slider.prev()}
                        disabled={sliderRef === 0}
                    />
                    <ArrowRight
                        onClick={(e) => e.stopPropagation() || slider.next()}
                        disabled={sliderRef === slider?.details().size - 1}
                    />
                </>
            }
        </div>
    );
}

export default DetailBlogMobile;