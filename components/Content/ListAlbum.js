import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import renderHTML from 'react-render-html';
import { CoverTime } from "../Conver/CoverTime";
import { isMobile } from 'react-device-detect';

// var he = require('he');

export const ListAlbum = memo((props) => {
    useEffect(() => {
        // console.log('123', props.currentSlide)
        props.slider?.destroy()
        setTimeout(() => {
            props.slider?.refresh()

        }, 100);
    }, [props.slug])
    return (
        <>
            <div className="container-fix">
                <div className="px-4">
                    <div className="mt-12">
                        <div className="flex justify-center opacity-50"><CoverTime datatime={props?.data[0].node.modified} /></div>
                        <div className="text-center mt-4 text-4xl font-semibold md:w-full mx-auto max-w-xl">
                            {props?.data[0].node.title}
                        </div>
                        <div className="text-center mt-6 text-sm max-w-3xl  mx-auto content">
                            {renderHTML(props?.data[0]?.node?.content || '')}
                        </div>
                    </div>
                </div>
            </div>
            <div className="mt-16">
                <div>
                    <div className="navigation-wrapper">
                        <div ref={props.sliderRef} className="keen-slider">
                            {
                                props?.data[0]?.node?.gallery?.galleryField && props?.data[0]?.node?.gallery?.galleryField.map((content, keys) => {
                                    return <div key={keys} className="keen-slider__slide box flex justify-center " >
                                        <img className="box__inner md:h-800 h-auto md:w-auto w-full object-contain" src={`${content.sourceUrl}`} alt="" />
                                    </div>
                                })
                            }

                        </div>
                        {props.slider && (
                            <>
                                <ArrowLeft
                                    onClick={(e) => e.stopPropagation() || props.slider.prev()}
                                    disabled={props.currentSlide === 0}
                                />
                                <ArrowRight
                                    onClick={(e) => e.stopPropagation() || props.slider.next()}
                                    disabled={props.currentSlide === props.slider.details().size - 1}
                                />
                            </>
                        )}
                    </div>
                </div>
            </div>
            <div className="container-fix mt-12">
                <div className="px-4">
                    <div className="md:flex justify-between pt-8 border-t-1 border-gray-300">
                        {
                            props.next.length > 0 &&
                            <Link href={`/album/${props.next[0].node.slug}`}>
                                <a>
                                    <div className="flex mb-8">
                                        <div className="mr-8 w-60 h-32 overflow-hidden">
                                            <img className="w-full h-full object-cover" src={`${props.next[0].node.featuredImage.node.sourceUrl}`} alt="" />
                                        </div>
                                        <div className="mt-2">
                                            <div className="w-full flex justify-start mb-3">
                                                <img src="../img/arrrowlong.svg" />
                                            </div>
                                            <div className="font-semibold">{props.next[0].node.title}</div>
                                        </div>
                                    </div>
                                </a>
                            </Link>
                        }

                        <div className="md:w-80 md:block hidden">
                        </div>
                        {
                            props.prev.length > 0 &&
                            <Link href={`/album/${props.prev[0].node.slug}`}>
                                <a>
                                    <div className="flex mb-8">
                                        <div className="mr-8 mt-2">
                                            <div className="w-full flex justify-end mb-3">
                                                <img className="transform rotate-180" src="../img/arrrowlong.svg" />
                                            </div>
                                            <div className="text-right font-semibold">{props.prev[0].node.title}</div>
                                        </div>
                                        <div className="w-60 h-32 overflow-hidden">
                                            <img className="w-full h-full object-cover" src={`${props.prev[0].node.featuredImage.node.sourceUrl}`} alt="" />
                                        </div>

                                    </div>
                                </a>
                            </Link>
                        }

                    </div>
                </div>
            </div>
        </>
    )
    function ArrowLeft(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (

            <svg onClick={props.onClick}
                className={"md:left-12 left-4 arrow arrow--left" + disabeld} viewBox="0 0 59 58" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path id="ic_right copy" d="M30 0.715698L1.71573 29L30 57.2842" stroke={isMobile ? "white" : "black"} strokeWidth="2" />
            </svg>
        )
    }

    function ArrowRight(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (
            <svg onClick={props.onClick}
                className={"md:right-12 right-4 arrow arrow--right" + disabeld} viewBox="0 0 59 58" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path id="ic_right" d="M29 0.715698L57.2843 29L29 57.2842" stroke={isMobile ? "white" : "black"} strokeWidth="2" />
            </svg>
        )
    }
});