import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
// var he = require('he');

export const ItemGallery = memo((props) => {
    return (
        <>
            <div className="relative">
                <Link href={`/album/${props.data.slug}`}>
                    <a>
                        <div className="relative parent-img-ga">
                            {/* <img src={`${props.data.fimg_url ? props.data.fimg_url : (props.data.acf.gallery_field.length > 0 ? props.data.acf.gallery_field[0] : '')}`} alt="gallyry" /> */}
                            <img src={`${props.data.featuredImage.node.sourceUrl}`} alt="gallyry" />
                        </div>
                        <div className="absolute top-0 w-full h-full justify-center items-center flex text-xs md:text-2xl font-semibold text">
                            <div className="md:w-72 w-36 text-center">
                                <h4 className="bg-black md:px-4 md:py-2 px-1 text-white uppercase md:leading-7">{props.data.title}</h4>
                            </div>
                        </div>
                    </a>
                </Link>
            </div>
        </>
    )
});