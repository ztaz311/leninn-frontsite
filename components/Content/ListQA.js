import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
// import { ItemProduct } from './ItemProduct'
import { isMobile } from 'react-device-detect';
import { useKeenSlider } from "keen-slider/react"
import { UIAcf } from "../Model/UIStore";
import { Loading } from "../Aside/Loading";
import { actGetListAcf } from "../../action/page";
export const ListQA = memo((props) => {
    const [currentSlide, setCurrentSlide] = React.useState(0)
    // const [loading, setLoading] = useState(true)
    const [sliderRef, slider] = useKeenSlider({
        loop: true,
        slidesPerView: isMobile ? 6 : 6,
        spacing: 10,
        slideChanged(s) {
            setCurrentSlide(s.details().relativeSlide)
        },
    })
    return (
        <>

            <div className="container-fix w-full">
                <div className="">
                    {/* {loading && <Loading />} */}
                    <div className="navigation-wrapper slider-qa py-8 justify-center">
                        {/* <div ref={sliderRef} className="keen-slider md:px-0 mx-5"> */}
                        <div ref={sliderRef} className="keen-slider md:px-5 mx-5">

                            {
                                props.data.length > 0 && props.data.map((content, keys) => {

                                    return <div key={keys} className="justify-between">
                                        <img className="md:mx-5" src={`${content.img.sourceUrl}`} alt="qa" />
                                    </div>

                                })
                            }
                        </div>
                        {/* {slider && (
                            <>
                                <ArrowLeft
                                    onClick={(e) => e.stopPropagation() || slider.prev()}
                                    disabled={currentSlide === 0}
                                />
                                <ArrowRight
                                    onClick={(e) => e.stopPropagation() || slider.next()}
                                    disabled={currentSlide === slider.details().size - 1}
                                />
                            </>
                        )} */}
                    </div>
                </div>
            </div>
        </>
    )
    function ArrowLeft(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (
            <img onClick={props.onClick} className="absolute top-1/2 md:w-14 md:h-14 w-10 h-10 cursor-pointer md:-ml-8 transform -translate-y-1/2" src="../img/left-qa.svg" alt="right" />
        )
    }

    function ArrowRight(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (
            <img onClick={props.onClick} className="absolute top-1/2 md:w-14 md:h-14 w-10 h-10 cursor-pointer md:-mr-8 transform -translate-y-1/2 left-auto right-0" src="../img/right-qa.svg" alt="right" />
        )
    }
});