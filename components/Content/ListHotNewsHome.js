import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import { actGetListPost } from "../../action/post"
import { Loading } from "../Aside/Loading"
import { UIPost } from "../Model/UIStore"
import { i18n, withTranslation } from "../../i18n";
import { useKeenSlider } from "keen-slider/react"
import { CoverTime } from "../Conver/CoverTime"
import { isMobile } from 'react-device-detect';

export const ListHotNewsHome = withTranslation('common')(memo((props) => {
    const [currentSlide, setCurrentSlide] = React.useState(0)

    const [sliderRef, slider] = useKeenSlider({
        loop: true,
        slidesPerView: isMobile ? 1 : 3,
        spacing: 15,
        slideChanged(s) {
            setCurrentSlide(s.details().relativeSlide)
        },
    })
    const { t } = props

    // console.log('123', props.data.data)

    function ArrowLeft(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (
            <img onClick={props.onClick} className="absolute top-1/2 md:w-14 md:h-14 w-10 h-10 cursor-pointer md:-ml-24 transform -translate-y-1/2" src="../img/left-qa.svg" alt="right" />
        )
    }

    function ArrowRight(props) {
        const disabeld = props.disabled ? " arrow--disabled hidden" : ""
        return (
            <img onClick={props.onClick} className="absolute top-1/2 md:w-14 md:h-14 w-10 h-10 cursor-pointer md:-mr-0 transform -translate-y-1/2 left-auto right-0" src="../img/right-qa.svg" alt="right" />
        )
    }
    return (
        <>
            <div className="bg-black">
                <div className="container-fix w-full ">
                    <div className="md:w-56 md:ml-48 ml-12">
                        <Link href={`/blog`}>
                            <a>
                                <div className="uppercase text-2xl font-semibold text-white pt-11 pb-4">{t('blogs_hot')}</div>
                            </a>
                        </Link>
                        <div className="w-20 border-t-1 border-white"></div>
                    </div>
                    <div className="px-4 md:pt-24 md:pb-12 pt-4 pb-10">
                        <div className="md:flex md:justify-between ">
                            <div className="">
                                <div className="w-27"></div>
                            </div>
                            <div className="md:flex-1 ">
                                <div className="md:flex md:justify-between ">
                                    {

                                        props.data.data.length > 0 &&

                                        <div ref={sliderRef} className="keen-slider bg-black">
                                            {
                                                props.data.data.map((item, keys) => {
                                                    return (

                                                        <div key={keys} className="keen-slider__slide">
                                                            <Link key={keys} href={`/post/${item.slug}`}>
                                                                <a>
                                                                    <div className="md:max-w-270px w-full mb-8 md:mb-1 overflow-hidden">
                                                                        <div className="md:max-w-270px md:max-h-270px w-full md:h-80 h-60 mb-8 md:mb-1 overflow-hidden ">
                                                                            {item?.fimg_url && <img className="w-full h-full object-cover" src={`${item?.fimg_url}`} alt="noibat" />}

                                                                        </div>
                                                                        <div className="mt-7 text-lg leading-6 font-semibold text-white">
                                                                            {item.title.rendered}
                                                                        </div>
                                                                        <div className="mt-5 text-white text-sm opacity-50">
                                                                            <CoverTime datatime={item.modified} />
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </Link>
                                                        </div>

                                                    )
                                                })
                                            }
                                        </div>
                                    }
                                    {
                                        <>
                                            <ArrowLeft
                                                onClick={(e) => e.stopPropagation() || slider.prev()}
                                                disabled={sliderRef === 0}
                                            />
                                            <ArrowRight
                                                onClick={(e) => e.stopPropagation() || slider.next()}
                                                disabled={sliderRef === slider?.details().size - 1}
                                            />
                                        </>
                                    }
                                    {/* {
                                        props.data.data.length > 0 &&
                                        props.data.data.map((item, keys) => {
                                            return (
                                                <Link key={keys} href={`/post/${item.slug}`}>
                                                    <a>
                                                        <div className="md:max-w-270px  w-full mb-8 md:mb-1 overflow-hidden">
                                                            <div className="md:max-w-270px md:max-h-270px w-full md:h-80 h-60 mb-8 md:mb-1 overflow-hidden">
                                                                {item?.fimg_url && <img className="w-full h-full object-cover" src={`${item?.fimg_url}`} alt="noibat" />}

                                                            </div>
                                                            <div className="mt-7 text-lg leading-6 font-semibold text-white">
                                                                {item.title.rendered}
                                                            </div>
                                                            <div className="mt-5 text-white text-sm opacity-50">
                                                                <CoverTime datatime={item.modified} />
                                                            </div>
                                                        </div>
                                                    </a>
                                                </Link>)

                                        })
                                    } */}


                                    {/* <Link href="/blog">
                                        <a className="md:relative top-0 right-0 text-white flex justify-center md:mb-10">
                                            <div className="mr-2 block md:hidden">{t('see_more')}</div>
                                            <img src="../img/arrowlongrightwhite.svg" alt="arrow" />
                                        </a>
                                    </Link> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}));