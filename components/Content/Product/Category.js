
import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import { i18n, withTranslation } from '../../../i18n'
import { isMobile } from 'react-device-detect';
export const Category = withTranslation('common')(memo((props) => {
    const [categoryActive, setCategoryActive] = useState('')
    useEffect(() => {
        setCategoryActive(props.active)
    }, [props.active])
    const { t } = props

    let group = props.dataChil?.reduce((r, a) => {
        r[a.parent] = [...r[a.parent] || [], a];
        return r;
    }, {});

    const [checkHover, setCheckHover] = useState({})

    return (
        <>
            <div className="container-fix w-full pt-6">
                <div className="px-4">
                    <div className="md:flex justify-center flex-wrap py-4">
                        <div className="px-7">
                            <Link href={`/products`} as="">
                                <a>
                                    <div className={`text-black font-semibold text-sm leading-4 py-3 tab-product uppercase hover:text-gray-500 ${categoryActive === '' ? 'active opacity-100' : 'opacity-40'}`}>{t('all')}</div>
                                </a>
                            </Link>
                        </div>
                        {
                            props.data.sort((a, b) => a?.menu_order - b?.menu_order).map((contest, idx) => {
                                return (
                                    <div key={idx} className="px-7 ">
                                        {
                                            group !== undefined && group[contest.id] !== undefined && group[contest.id]?.length > 0 ?

                                                <div onMouseEnter={() => setCheckHover({ ...checkHover, index: idx })} onMouseLeave={() => setCheckHover({ ...checkHover, index: -1 })} >
                                                    <Link href={`/products/${contest.slug}`} >
                                                        <a>
                                                            <div className={`text-black font-semibold text-sm leading-4 py-3 tab-product uppercase hover:text-gray-500 ${categoryActive === contest.slug || (group[contest.id] !== undefined && group !== undefined && group[contest.id].filter(s => s.slug === categoryActive)?.length > 0) ? 'active opacity-100' : 'opacity-40'}`}>{`${contest.name} `}</div>
                                                        </a>
                                                    </Link>
                                                    {checkHover.index === idx &&
                                                        <>
                                                            {
                                                                isMobile ?
                                                                    <div
                                                                    //  className={`absolute z-50 bg-white`}
                                                                    >
                                                                        {

                                                                            group[contest.id] !== undefined && group[contest.id]?.length > 0 &&
                                                                            group[contest.id].map((item, index) => {
                                                                                return (
                                                                                    <Link href={`/products/${item.slug}`} key={index}>
                                                                                        <a>
                                                                                            <div className={`text-black font-semibold text-sm leading-4 py-2 tab-product ml-3 hover:text-gray-500 ${categoryActive === item.slug ? 'active' : ''}`}>{item?.name}</div>
                                                                                        </a>
                                                                                    </Link>
                                                                                )
                                                                            })
                                                                        }
                                                                    </div>
                                                                    :
                                                                    <div className='flex absolute z-50 bg-white justify-center items-center left-0.5 right-0.5'>
                                                                        {

                                                                            group[contest.id] !== undefined && group[contest.id]?.length > 0 &&
                                                                            group[contest.id].sort((a, b) => a?.menu_order - b?.menu_order).map((item, index) => {
                                                                                return (
                                                                                    <Link href={`/products/${item.slug}`} key={index}>
                                                                                        <a>
                                                                                            <div className={`text-black font-semibold text-sm leading-4 pt-5 pb-2 tab-product mx-5 hover:text-gray-500 ${categoryActive === item.slug ? 'opacity-100' : 'opacity-40'}`}>{item?.name}</div>
                                                                                        </a>
                                                                                    </Link>
                                                                                )
                                                                            })
                                                                        }
                                                                    </div>
                                                            }
                                                        </>
                                                    }
                                                </div>
                                                :
                                                <>
                                                    <Link href={`/products/${contest.slug}`} >
                                                        <a>
                                                            <div className={`text-black font-semibold text-sm leading-4 py-3 tab-product uppercase hover:text-gray-500 ${categoryActive === contest.slug ? 'active opacity-100' : 'opacity-40'}`}>{`${contest.name}`}</div>
                                                        </a>
                                                    </Link>
                                                </>
                                        }
                                        {/* <Link href={`/products/${contest.slug}`} >
                                            <a>
                                                <div className={`text-black font-semibold text-sm leading-4 py-3 tab-product uppercase ${categoryActive === contest.slug ? 'active' : ''}`}>{`${contest.name}`}</div>
                                            </a>
                                        </Link>
                                        <img src='../img/arrow_down.png' /> */}
                                        {/* <div className={`absolute z-50`}>
                                            {
                                                group[contest.id] !== undefined && group[contest.id]?.length > 0 &&
                                                group[contest.id].map((item, index) => {
                                                    return (
                                                        <div key={index} className={`text-black font-semibold text-sm leading-4 py-1 tab-product `}>{item.name}</div>
                                                    )
                                                })
                                            }
                                        </div> */}
                                    </div>
                                )
                            })
                        }

                    </div>
                </div>
            </div>
        </>
    )
}));