import { useKeenSlider } from "keen-slider/react";
import {
    FacebookShareButton
} from 'next-share';
import { useRouter } from 'next/router';
import React, { memo, useEffect, useRef, useState } from 'react';
import { isMobile } from 'react-device-detect';
import InnerImageZoom from 'react-inner-image-zoom';
import renderHTML from 'react-render-html';
import { withTranslation } from '../../i18n';
import useScrollBlock from '../../unity/useScrollBlock';
import { Toss } from "../Model/Toss";
import { UIStore } from "../Model/UIStore";
import { ItemProduct } from './ItemProduct';
import { ChartSize } from '../Model/ChartSize'
// var he = require('he');


export const DetailProduct = withTranslation('common')(memo((props) => {

    const ref1 = useRef(null)
    const ref2 = useRef(null)
    const ref3 = useRef(null)
    const router = useRouter()
    const [toss, setToss] = useState([])
    const [size, setSize] = useState(null)
    const [color, setColor] = useState(null)
    const [quantily, setQuantily] = useState(1)

    const [indexRmv, setIndexRmv] = useState([])
    const [indexRmv2, setIndexRmv2] = useState([])
    const [modelChart, setModelChart] = useState(false)
    const [blockScroll, allowScroll] = useScrollBlock();
    const [scroll, setScroll] = useState(false)
    const cartTotal = UIStore.useState(s => s.cart.total);
    const cartData = UIStore.useState(s => s.cart.data);
    const [sliderMain, setSliderMain] = useState({
        vitri: 0,
        src: ''
    });
    const [checkHover, setCheckHover] = useState(false)
    const [price, setPrice] = useState({
        price: '',
        salePrice: '',
        regularPrice: ''
    })
    const [stock, setStoke] = useState({
        status: '',
        quantily: ''
    })
    const [variationID, setVariationID] = useState(props.data.databaseId)
    const [images, setImages] = useState(new Array(props.data.galleryImages.nodes.length + 1).fill({ sourceUrl: '../../img/loading.gif' }))

    const [currentSlide, setCurrentSlide] = React.useState(0)
    const [sliderRef, slider] = useKeenSlider({
        loop: false,
        // rtl: true,
        slidesPerView: 4,
        spacing: 10,
        initial: 0,
        slideChanged(s) {
            setCurrentSlide(s.details().relativeSlide)
        },
    })

    const { t } = props
    const handleClick = async (e) => {
        e.preventDefault();
        let data = {
            id: props.data.id,
            quantily: quantily,
            size: size,
            color: color,
            image: images[0].sourceUrl,
            name: props.data.name,
            price: price.price,
            slug: props.data.slug,
            validationId: variationID,
            sale: props?.data?.salePrice !== undefined && props.data.salePrice !== "" && props.data.salePrice !== null ? true : false
        }
        var indexCart = cartData.findIndex(data => data.id === props.data.id && data.size === size && data.color === color)
        let cart = {}
        if (indexCart > -1) {
            let cartTotalFetch = [...cartData]
            cartTotalFetch[indexCart] = { ...cartTotalFetch[indexCart], quantily: cartTotalFetch[indexCart].quantily + quantily }
            cart = {
                total: cartTotal,
                data: cartTotalFetch,

            }
        } else {
            cart = {
                total: cartTotal + 1,
                data: [...cartData, data]

            }
        }

        await UIStore.update(s => { s.classNoti = 'add'; s.cart = cart; })
        // await setTimeout(function () {
        //     UIStore.update(s => { s.classNoti = ''; s.cart.total = s.cart.total; });
        // }, 700);
        await setToss([...toss, data])
        await setIndexRmv([...indexRmv, toss.length])
        localStorage.setItem('cart', JSON.stringify(cart));

    };

    const handleClickPlus = (e) => {
        setQuantily(quantily + 1)
    }
    const handleClickSub = (e) => {
        if (quantily > 1) {
            setQuantily(quantily - 1)
        }
    }
    const handleChangeSlide = async (vitrin, srcn, e) => {
        await setSliderMain({ ...sliderMain, vitri: vitrin, src: "../../img/loading.gif" })
        await setTimeout(() => {
            setSliderMain({ ...sliderMain, vitri: vitrin, src: srcn })
        }, 10);

    }

    useEffect(() => {
        setTimeout(() => {
            setIndexRmv2(indexRmv)
        }, 6000);
    }, [indexRmv]);
    const openModal = (event) => {
        document.body.classList.add('compensate-for-scrollbar');
        setModelChart(true);
    }
    const hideModal = (event) => {
        document.body.classList.remove('compensate-for-scrollbar');
        setModelChart(false);
    }
    const wrapperModelRef = useRef(null);
    useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (wrapperModelRef.current && !wrapperModelRef.current.contains(event.target)) {
                hideModal()
            }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperModelRef]);




    useEffect(() => {
        window.scrollTo(0, 0)
    }, [scroll])


    useEffect(async () => {
        await setImages(new Array(props.data.galleryImages.nodes.length + 1).fill({ sourceUrl: '../../img/loading.gif' }))
        await setSliderMain({
            vitri: 0,
            src: ''
        })
       
        setPrice({
            price: props.data.price,
            salePrice: props.data.salePrice,
            regularPrice: props.data.regularPrice
        })
        setStoke({
            status: props.data.stockStatus,
            quantily: props.data.stockQuantity
        })

        if (props.data.attributes?.nodes[1]) {
            setSize(props.data.attributes.nodes[1].terms.nodes[0].name)
        }

        if (props.data.attributes?.nodes[0] !== undefined) {
            setColor(props.data.attributes.nodes[0].terms.nodes[0].name)
        }
        setQuantily(1)

        await slider?.destroy()
        await setTimeout(() => {
            slider?.refresh()
        }, 100);

        let dataImg = await [props.data.image, ...props.data.galleryImages.nodes]
        await setTimeout(() => {
            setImages(dataImg)
        }, 500);
        dataImg.forEach(element => {
            let image = new Image();
            image.src = element.sourceUrl + '?morph=800';
        });
        dataImg.forEach(element => {
            let image2 = new Image();
            image2.src = element.sourceUrl + '?morph=128';
        });
        return () => {
            setSliderMain({
                vitri: 0,
                src: ''
            })
        }
    }, [props.data])

    useEffect(() => {
        
        props.data.variations?.nodes.forEach(element => {
            let colorVa = element.attributes.nodes[0]
            let sizeVa = element.attributes.nodes[1]
            if ((colorVa?.value || null) == color?.toUpperCase() && (sizeVa?.value || null) == size?.toUpperCase()) {
                setPrice({
                    price: element.price,
                    salePrice: element.salePrice,
                    regularPrice: element.regularPrice
                })
                setStoke({
                    status: element.stockStatus,
                    quantily: element.stockQuantity
                })
                setVariationID(element.databaseId)
                return
            }
        });
    }, [size, color, props.data.variations?.nodes,quantily])
    console.log('price',price)
    const printPrice = () => {
        if (price.salePrice !== "" && price.salePrice !== null) {
            return (
                <div><span className="line-through italic">{price.regularPrice} </span> - {price.salePrice} </div>
            )
        } else {
            return <div>{price.price} </div>
        }
    }

    const formatCurencyToNumber = (currency) => {
        // var cur_re = /\D*(\d+|\d.*?\d)(?:\D+(\d{2}))?\D*$/;
        // var parts = cur_re.exec(currency);
        // var number = parseFloat(parts[1].replace(/\D/, '') + '.' + (parts[2] ? parts[2] : '00'));
        // return number

        var number = currency?.toString()?.match(/\d/g);
        number = number.join("");
        return number.toString()
    }

    var km = 0
    if (price.salePrice !== null && price.salePrice != price.regularPrice && price.salePrice != "" && price.regularPrice) {
        if (formatCurencyToNumber(price.salePrice) !== formatCurencyToNumber(price.regularPrice)) {
            const ctkm = (formatCurencyToNumber(price.regularPrice) - formatCurencyToNumber(price.salePrice)) / (formatCurencyToNumber(price.regularPrice) / 100)
            km = Math.floor(ctkm)
        }
    }


    return (
        <>
            {toss.length > 0 &&
                toss.map((item, index) => {
                    if (indexRmv2.find(element => element === index) !== undefined) {
                        return null;
                    } else {
                        return <div key={index}><Toss data={item} t={t} onClose={() => setIndexRmv2(indexRmv)} /></div>
                    }
                })
            }
            {props.data?.sizeChart?.sizeChart !== null && modelChart && <ChartSize t={t} wrapperModelRef={wrapperModelRef} closeModal={(e) => hideModal(e)} data={props.data?.sizeChart?.sizeChart?.sourceUrl || props.data?.sizeChart?.sizeChart?.mediaItemUrl} />}
            <div className="container-fix">
                <div className="px-4">
                    <div className="md:mt-12 mt-6">
                        <div className="grid md:grid-cols-2 md:gap-x-10 md:gap-y-0 grid-cols-1 gap-y-4">
                            <div>
                                <div className="relative">
                                    <InnerImageZoom
                                        src={`${!sliderMain?.src ? images[0]?.sourceUrl + '?morph=800' : sliderMain?.src + '?morph=800'}`}
                                        // src={images[0].sourceUrl}
                                        alt="leninn"
                                        zoomPreload
                                        zoomScale={1}
                                        zoomType="hover"
                                        afterZoomIn={(e) => { e?.cancelable && e.preventDefault(), isMobile && blockScroll(), isMobile && setScroll(!scroll) }}
                                        afterZoomOut={(e) => { e?.cancelable && e.preventDefault(), isMobile && allowScroll() }}
                                    />
                                    {km !== 0 &&
                                        <div className="absolute" style={{ top: '0.5rem', backgroundColor: 'red' }}>
                                            <span className="text-sm px-2 text-white">{km}% OFF</span>
                                        </div>
                                    }
                                    {stock.quantily !== null && stock.quantily > 0 &&
                                        <div className="absolute" style={{ top: '0.5rem', right: 0, backgroundColor: 'black' }}>
                                            <span className="text-sm px-2 text-white">{stock.quantily} LEFT</span>
                                        </div>
                                    }
                                </div>
                                <div className="navigation-wrapper mt-6 ">
                                    {images.length > 0 &&
                                        <div ref={sliderRef} className="keen-slider"
                                            onMouseEnter={() => setCheckHover(true)} onMouseLeave={() => setCheckHover(false)}>
                                            {
                                                images.map((item, keys) => {
                                                    return (
                                                        <div onClick={(e) => handleChangeSlide(keys, item.sourceUrl, e)} key={keys}
                                                            className={`keen-slider__slide cursor-pointer ${sliderMain.vitri == keys ? 'active' : ''}`}>
                                                            <img className="md:w-32 w-24" src={`${item.sourceUrl}?morph=128`} alt={`${item.name}`} />
                                                        </div>
                                                    )
                                                })
                                            }
                                            {
                                                images?.length > 4 && checkHover &&
                                                <>
                                                    <img onClick={(e) => e.stopPropagation() || slider.prev()} className="absolute top-1/2 left-0 md:w-5 md:h-5 w-5 h-5 cursor-pointer bg-gray-100 rounded-2xl p-1" src="../../img/left-slides2.svg" alt="left" />
                                                    <img onClick={(e) => e.stopPropagation() || slider.next()} className="absolute top-1/2 right-0 md:w-5 md:h-5 w-5 h-5 cursor-pointer bg-gray-100 rounded-2xl p-1" src="../../img/right-slides2.svg" alt="right" />
                                                </>
                                            }
                                        </div>}

                                </div>
                            </div>
                            <div className="md:pl-28">
                                <div className="pb-6 border-b-1 border-gray-300">
                                    <h1 className="font-semibold text-4xl pb-4 leading-9">
                                        {props.data.name}
                                    </h1>
                                    <div className="text-lg leading-5">
                                        {printPrice()}
                                    </div>
                                    <div className="mt-5">
                                        <div className="content"

                                        >
                                            {props.data.description !== null && renderHTML(props.data.description)}
                                        </div>
                                        <div className="border-gray-300 border-t-1 border-b-1 py-5 mt-8 flex justify-between">
                                            <div className="">
                                                <div
                                                    ref={ref1} style={{ height: Math.max(ref1?.current?.clientHeight, ref2?.current?.clientHeight, ref3?.current?.clientHeight) || 'auto' }}
                                                    className={`mr-4 text-sm mb-2 `}>{t('quantily')}:</div>
                                                <div className="styled-select relative border-gray-900 border-1 flex py-1">
                                                    <div className="md:px-4 px-3 font-semibold cursor-pointer border-r-2 border-gray-200 py-1" onClick={(e) => handleClickSub(e)}>-</div>
                                                    <input className="md:w-10 w-8 text-center"
                                                        value={quantily}
                                                        // onChange={(e) => setQuantily(e.target.value)} value={quantily} 
                                                        readOnly
                                                    />
                                                    <div className="md:px-4 px-3 font-semibold cursor-pointer border-l-2 border-gray-200 py-1" onClick={(e) => handleClickPlus(e)}>+</div>
                                                </div>
                                            </div>
                                            {size?.length > 0 && props.data.attributes?.nodes[1] !== undefined &&
                                                <div className="md:w-auto w-24 overflow-hidden">
                                                    <div
                                                        ref={ref2} style={{ height: Math.max(ref1?.current?.clientHeight, ref2?.current?.clientHeight, ref3?.current?.clientHeight) || 'auto' }}
                                                        className="mr-4 text-sm mb-2">{t('choose_size')}:</div>
                                                    <div className="styled-select relative border-gray-900 border-1 z-1">
                                                        <select
                                                            value={size}
                                                            className="pl-3 md:pr-14 pr-6 py-2 relative cursor-pointer bg-transparent z-10 w-full overflow-hidden uppercase"
                                                            onChange={(e) => {
                                                                setSize(e.target.value)
                                                                // changePrice('size', e.target.value)
                                                            }}
                                                        >
                                                            {

                                                                props.data.attributes.nodes[1].terms.nodes.map((item, keys) => {
                                                                    return <option key={keys} value={`${item.name}`}>{item.name}</option>
                                                                })

                                                            }

                                                        </select>
                                                        <div className="absolute top-0 mt-4 mr-3 right-0 z-0">
                                                            <img src="../../img/ic-arrow-down.svg" alt="xx" />
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                            {color?.length > 0 && props.data.attributes?.nodes[0] !== undefined &&
                                                <div className="md:w-auto w-24 overflow-hidden">
                                                    <div ref={ref3} style={{ height: Math.max(ref1?.current?.clientHeight, ref2?.current?.clientHeight, ref3?.current?.clientHeight) || 'auto' }} className="mr-4 text-sm mb-2">{t('choose_color')}:</div>
                                                    <div className="styled-select relative">
                                                        <select
                                                            value={color}
                                                            className="pl-3 md:pr-14  pr-6 py-2 border-gray-900 border-1 relative cursor-pointer bg-transparent z-10 overflow-hidden w-full uppercase"
                                                            onChange={(e) => {
                                                                setColor(e.target.value)
                                                                // changePrice('color', e.target.value)
                                                            }}
                                                        >
                                                            {
                                                                props.data.attributes.nodes[0].terms.nodes.map((item, keys) => {
                                                                    return <option key={keys} value={`${item.name}`} >{item.name}</option>
                                                                })
                                                            }
                                                        </select>
                                                        <div className="absolute top-0 mt-4 mr-3 right-0">
                                                            <img src="../../img/ic-arrow-down.svg" alt="xx" />
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                        <div>
                                            <div className="flex justify-between items-center py-8">
                                                <div className="flex justify-between items-center">
                                                    <div className="mr-5">
                                                        {t('share')}:
                                                    </div>
                                                    <div className="mr-3">
                                                        <FacebookShareButton
                                                            url={`${process.env.NEXTAUTH_URL}/${router.asPath}`}
                                                            quote={props.data.name}
                                                            hashtag={`#leninn #${props?.data?.name}`}
                                                        >
                                                            <img className="cursor-pointer" src="../../img/icon-fb-black.svg" alt="dd" />
                                                        </FacebookShareButton>

                                                    </div>
                                                </div>
                                                {
                                                    props.data?.sizeChart?.sizeChart !== null &&
                                                    <div className="flex justify-between items-center cursor-pointer" onClick={(e) => openModal(e)}>
                                                        <img className="mr-2" src="../../img/ic_left.svg" />
                                                        <div className={`uppercase font-semibold text-sm`}>{t('size_chart')}</div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                        <div className="flex justify-between">
                                            {
                                                stock.status !== "OUT_OF_STOCK" ?
                                                    <>
                                                        <div>
                                                            <button onClick={(e) => handleClick(e)} className="uppercase font-semibold py-3 px-4 md:px-10 bg-gray-300 text-xs md:text-base">{t('add_to_cart')}</button>
                                                        </div>
                                                        <div>
                                                            <button onClick={(e) => { handleClick(e), router.push('/cart') }} className="uppercase font-semibold py-3 text-xs md:text-base px-10   text-white bg-gray-900">{t('buy_now')}</button>
                                                        </div>
                                                    </>
                                                    :
                                                    <>
                                                        <div>
                                                            <button className="uppercase font-semibold py-3 px-4 md:px-10 bg-gray-300 text-xs md:text-base pointer-events-none">{t('out_of_stock')}</button>
                                                        </div>
                                                    </>
                                            }

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="md:mt-24 mt-12">
                            <div className="flex justify-center">
                                <div className="text-2xl font-semibold leading-7 uppercase">{t('related_products')}</div>
                            </div>
                            <div className="md:mt-12 mt-8">
                                <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
                                    {/* {console.log(' props.data.related.nodes', props)} */}
                                    {
                                        props.data.related.nodes.length > 0 && props.data.related.nodes.map((contest, idx) => {
                                            return <ItemProduct key={idx} data={contest} />
                                        })
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}));