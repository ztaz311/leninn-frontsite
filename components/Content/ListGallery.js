import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import { ItemGallery } from './ItemGallery'
import { actGetListGallery } from "../../action/gallery";
import { Loading } from "../Aside/Loading";
import { SeeMore } from "../Button/SeeMore";
import { UIGallery } from "../Model/UIStore";

export const ListGallery = memo((props) => {
    // const itemGalleryUI = UIGallery.useState(s => s.DATA_GALLERY_ALL_REQUEST);
    // const [itemGallery, setItemGallery] = useState(itemGalleryUI)
    // const [page, setPage] = useState(1);
    // const [showSeeMore, setShowSeeMore] = useState(false);
    // const [loading, setLoading] = useState(true);
    // const [perPage, setPerPage] = useState(12);

    // const hanleSeeMore = (e) => {
    //     setPage(page + 1);
    //     setShowSeeMore(false);
    //     setLoading(true);
    // }

    // useEffect(() => {
    //     setLoading(false);
    //     if (props.data?.length >= perPage) {
    //         setShowSeeMore(true);
    //     } else {
    //         setShowSeeMore(false);
    //     }
    // }, [props.data])

    // const [dataLocal, setDataLocal] = useState(props.data)
    // useEffect(() => {
    //     if (page > 1) {
    //         actGetListGallery(perPage, page, null).then(data => {
    //             setLoading(false)
    //             setDataLocal([...dataLocal, ...data.data])
    //         })
    //     }
    // }, [page])

    return (
        <>
            <div className="">
                <div className="grid grid-cols-1 md:grid-cols-2">
                    {
                        props.data.length > 0 && props.data.map((content, keys) => {
                            return <ItemGallery data={content.node} key={keys} />
                        })
                    }
                </div>
                {/* {showSeeMore && <SeeMore clickSeeMore={(e) => hanleSeeMore(e)} />}
                {loading && <Loading />} */}
            </div>
        </>
    )
});