import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'

export const ItemBlog = memo((props) => {
    const formatDate = (date) => {
        const d2 = new Date(date);
        const ye2 = new Intl.DateTimeFormat('en', { year: 'numeric' })?.format(d2);
        const mo2 = new Intl.DateTimeFormat('en', { month: 'numeric' })?.format(d2);
        const da2 = new Intl.DateTimeFormat('en', { day: '2-digit' })?.format(d2);
        return `${da2}.${mo2}.${ye2}`;
    }
    return (
        <>
            <div className="flex justify-center md:mb-16 mb-6">
                <div className="relative md:max-w-970px w-full">
                    <Link href={`/post/${props.data.slug}`}>
                        <a>
                            <div className="relative parent-img-bl">
                                <img src={`${props.data.fimg_url === undefined ? props.data.src : props.data.fimg_url}`} alt="gallyry" />
                            </div>
                            <div className="w-full flex justify-center mt-8">
                                <div className="md:max-w-3xl w-full">
                                    <div className="text-2xl font-semibold leading-7">
                                        {props.data.title?.rendered}
                                    </div>
                                    <div className="mt-6 opacity-50 leading-4"> {formatDate(props.data.date_created === undefined ? props.data.date : props.data.date_created)}</div>
                                </div>

                            </div>
                        </a>
                    </Link>
                </div>
            </div>
        </>
    )
});