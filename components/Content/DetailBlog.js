import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import { ItemBlog } from './ItemBlog'
import renderHTML from 'react-render-html';
import { actGetPostInCategory } from "../../action/post";
import { i18n, withTranslation } from "../../i18n";
import {
    BrowserView,
    MobileView,
} from 'react-device-detect';
import DetailBlogMobile from './DetailBlogMobile'
import {
    FacebookShareButton,
} from 'next-share'
import { useRouter } from 'next/router'

export const DetailBlog = withTranslation('common')(memo((props) => {
    const [itemPostsRelate, setItemPostsRelate] = useState([])
    const [loading, setLoading] = useState(true);
    const router = useRouter()

    const setPostsAgain = async x => {
        let dataPr = [...itemPostsRelate]
        for await (let element of x) {
            var arr1 = {
                id: element.id,
                title: element.title.rendered,
                slug: element.slug,
                src: element.fimg_url,
                date_created: element.modified,
                status: element.status,
                ping_status: element.ping_status,
                price: element.price,
            }
            dataPr.push(arr1)
        }
        await setItemPostsRelate(dataPr);
        await setLoading(false);

    }
    const d = new Date(props.data.modified);
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    const mo = new Intl.DateTimeFormat('en', { month: 'numeric' }).format(d);
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    const date = (`${da}.${mo}.${ye}`);



    useEffect(() => {
        actGetPostInCategory(4, 1, props.data.categories.length > 0 ? props.data.categories[0] : '', [props.data.id], null).then(data => setPostsAgain(data.data))

    }, [])
    const { t } = props


    // console.log('123', itemPostsRelate.length)
    return (
        <>


            <div className="container-fix">
                <div className="px-4">
                    <div className="md:mt-12 mt-6  md:max-w-970px mx-auto">
                        <div className="">
                            <div className="md:text-4xl text-xl font-semibold leading-8">{props.data.title.rendered}</div>
                        </div>
                        <div className="mt-6">
                            <div className=" leading-4 opacity-50">
                                {date}
                            </div>
                        </div>
                        <div className="content mt-6">
                            {renderHTML(props.data.content.rendered)}
                        </div>
                        <div className="mt-14 border-t-1 border-b-1 border-gray-300 flex items-center py-5">
                            <div className="mr-4">
                                Chia sẻ
                            </div>
                            <div>
                                {/* <img src="../img/ic-facebook.svg" /> */}
                                <FacebookShareButton
                                    url={`${process.env.NEXTAUTH_URL}/${router.asPath}`}
                                    quote={props.data.title.rendered}
                                    hashtag={`#leninn`}
                                >
                                    <img src="../img/ic-facebook.svg" className="cursor-pointer" />

                                    {/* <img className="cursor-pointer" src="../img/icon-fb-black.svg" alt="dd" /> */}
                                    {/* <FacebookIcon /> */}
                                </FacebookShareButton>
                            </div>
                        </div>
                        <div className="mt-4">
                            {
                                props.data.tags.length > 0 &&
                                props.data.tags.map((item, keys) => {
                                    <Link key={keys} href="/">
                                        <a className="py-3 px-4 bg-gray-200 text-xs font-semibold rounded-2xl text-gray-500 mr-4 mb-4 mt-4 md:mt-0">
                                            {item}
                                        </a>
                                    </Link>
                                })
                            }

                        </div>
                    </div>
                    <div className="md:mt-24 mt-12">
                        <div className="flex justify-center">
                            <div className="text-2xl font-semibold leading-7">{t('related_posts')}</div>
                        </div>

                        <div className="md:mt-16 mt-8">
                            <BrowserView>
                                {
                                    itemPostsRelate.length > 0 &&
                                    itemPostsRelate.map((content, keys) => {
                                        return <ItemBlog key={keys} data={content} />
                                    })
                                }
                            </BrowserView>
                            <MobileView>
                                {itemPostsRelate.length > 0 &&
                                    <DetailBlogMobile itemPostsRelate={itemPostsRelate} />}
                            </MobileView>
                        </div>


                    </div>
                </div>

            </div>

        </>
    )

}));
