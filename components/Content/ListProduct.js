import React, { memo, useEffect, useState } from 'react';
// import useScrollBlock from '../../unity/useScrollBlock'
// import { Animate, AnimateGroup } from 'react-smooth'
// import DelayedList from 'react-delayed-list'
import { Fade, Stagger } from 'react-animation-components';
import InfiniteScroll from 'react-infinite-scroll-component';
import { getProduct } from '../../constants/querry';
import { withTranslation } from "../../i18n";
import { Loading } from '../Aside/Loading';
import { ItemProduct } from './ItemProduct';

export const ListProduct = withTranslation('common')(memo((props) => {
    const [categoryActive, setCategoryActive] = useState(props.id !== undefined ? props.id : '');
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(false);
    const [showSeeMore, setShowSeeMore] = useState(false);
    const [end, setEnd] = useState(true)
    const [perPage, setPerPage] = useState(12);
    const hanleSeeMore = () => {
        setPage(page + 1);
        setShowSeeMore(false);
        setLoading(true);

    }
    const [quantity, setQuantity] = useState(12)

    const [dataLocal, setDataLocal] = useState(props?.dataPr)

    const [pageInfo, setPageInfor] = useState(props.pageInfo?.endCursor)

    // useEffect(() => {
    //     if (page > 1 && props.pageInfo.hasNextPage) {
    //         getProduct(perPage * page, props?.id !== undefined && props?.id !== '' ? props?.id : 0, pageInfo).then(data => {
    //             setLoading(false)

    //             if (data.data.data.products.pageInfo.hasNextPage) {
    //                 setShowSeeMore(true);
    //                 setEnd(true)
    //                 setPageInfor(data.data.data.products.pageInfo.endCursor)
    //             } else {
    //                 setShowSeeMore(false);
    //                 setEnd(false)
    //             }
    //             // console.log('data.data.data.products?.nodes', data.data.data.products?.nodes)
    //             setDataLocal([...dataLocal, ...data.data.data.products?.nodes])

    //         });
    //     }
    //     return () => {
    //         // setPage(1)
    //         setPageInfor(props.pageInfo?.endCursor)

    //     }
    // }, [page])

    useEffect(() => {
        async function aaa() {
            await setDataLocal([])
            await setDataLocal(props?.dataPr)
            // if (props?.dataPr.length >= perPage) {
            //     setShowSeeMore(true);
            //     setLoading(false)
            //     setEnd(true)
            // } else {
            //     setShowSeeMore(false);
            //     setLoading(false)
            //     setEnd(false)

            // }
        }
        aaa()
    }, [props?.dataPr])
    // useEffect(() => {
    //     if (!props.pageInfo.hasNextPage) {
    //         // setShowSeeMore(false);
    //         // setLoading(true)
    //         setEnd(false)
    //     }
    // }, [props.pageInfo.hasNextPage])

    return (
        <>

            <InfiniteScroll
                // blockScroll={true}
                dataLength={dataLocal.length} //This is important field to render the next data
                next={() => hanleSeeMore()}
                hasMore={false}
                loader={<Loading />}
                endMessage={
                    <p className="text-center pt-10">
                        <b>{props.t('see_all')}</b>
                    </p>
                }

            >
                <div className="container-fix w-full pt-8">
                    <div className="px-4">

                        {

                            dataLocal.length > 0 &&
                            <Stagger in className="grid grid-cols-2 md:grid-cols-4 gap-4" delay={50}>
                                {
                                    dataLocal.length > 0 && dataLocal.map((contest, idx) => {
                                        return (
                                            <Fade key={idx}>
                                                <ItemProduct data={contest} />
                                            </Fade>
                                        )
                                    })
                                }
                            </Stagger>
                        }
                    </div>
                </div>
            </InfiniteScroll>

        </>

    )
}));