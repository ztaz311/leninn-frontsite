import React, { memo, useState, useRef, useEffect } from 'react';
import Link from 'next/link'
import { ItemBlog } from './ItemBlog'
import { Loading } from '../Aside/Loading'
import { SeeMore } from '../Button/SeeMore'
import { actGetListPost } from "../../action/post"
import { UIPost } from "../Model/UIStore"
import InfiniteScroll from 'react-infinite-scroll-component';
import { withTranslation } from "../../i18n";

export const ListBlog = withTranslation('common')(memo((props) => {
    // const itemPostsUI = UIPost.useState(s => s.DATA_POSTS_ALL_REQUEST)
    // const [itemPosts, setItemPosts] = useState(itemPostsUI)
    const [page, setPage] = useState(1);
    const [showSeeMore, setShowSeeMore] = useState(false);
    const [loading, setLoading] = useState(true);
    const [perPage, setPerPage] = useState(6);
    const [end, setEnd] = useState(true)
    const hanleSeeMore = () => {
        setPage(page + 1);
        setShowSeeMore(false);
        setLoading(true);
    }
    // const setPostsAgain = async x => {
    //     let dataPr = [...itemPosts]
    //     for await (let element of x) {
    //         var arr1 = {
    //             id: element.id,
    //             title: element.title.rendered,
    //             slug: element.slug,
    //             src: element.fimg_url,
    //             date_created: element.modified,
    //             status: element.status,
    //             ping_status: element.ping_status,
    //             price: element.price,
    //         }
    //         dataPr.push(arr1)
    //     }
    //     await setItemPosts(dataPr);
    //     await setLoading(false);
    //     if (x.length >= perPage) {
    //         setShowSeeMore(true);
    //     }
    //     function uiUpdate(dataPr) {
    //         if (page < 2) {
    //             UIPost.update(s => { s.DATA_POSTS_ALL_REQUEST = dataPr })
    //         }
    //     }
    //     await uiUpdate(dataPr)
    // }
    useEffect(() => {
        if (page > 1) {
            actGetListPost(perPage, page, null).then(data => {
                // console.log('data', data)
                if (data.status === 200) {
                    setLoading(false)
                    // props.data.data = [...props.data.data, ]
                    setDataLocal([...dataLocal, ...data.data])
                } else {
                    setEnd(false)
                }
            })
        }
    }, [page])
    // useEffect(() => {
    //     if (itemPostsUI.length < 1) {
    //         actGetListPost(perPage, 1, null).then(data => setPostsAgain(data.data))
    //     } else {
    //         setItemPosts(itemPostsUI)
    //     }
    // }, [])
    // console.log('props', props.data.data)
    useEffect(() => {
        // console.log('12', props.data.data)
        setLoading(false);
        if (props.data.data?.length >= perPage) {
            setShowSeeMore(true);
            setEnd(true)
        } else {
            setShowSeeMore(false);
            setEnd(false)
        }
    }, [props.data.data])
    const [dataLocal, setDataLocal] = useState(props.data.data)
    // console.log('1', dataLocal)
    return (
        <>
            <InfiniteScroll
                // blockScroll={true}
                dataLength={dataLocal.length} //This is important field to render the next data
                next={() => hanleSeeMore()}
                hasMore={end}
                loader={<Loading />}
                endMessage={
                    <p style={{ textAlign: 'center' }}>
                        <b>{props.t('see_all')}</b>
                    </p>
                }
            // height="100%"
            // scrollableTarget="div4"
            // below props only if you need pull down functionality
            // refreshFunction={hanleSeeMore}
            // pullDownToRefresh
            // pullDownToRefreshThreshold={50}
            // pullDownToRefreshContent={
            //     <h3 style={{ textAlign: 'center' }}>&#8595; Pull down to refresh</h3>
            // }
            // releaseToRefreshContent={
            //     <h3 style={{ textAlign: 'center' }}>&#8593; Release to refresh</h3>
            // }
            >
                <div className="container-fix">
                    <div className="px-4">
                        <div className="md:mt-16 mt-8">
                            {
                                dataLocal && dataLocal.map((item, keys) => {
                                    return <ItemBlog key={keys} data={item} />
                                })
                            }
                        </div>
                    </div>
                </div>
            </InfiniteScroll>


            {/* <div className="container-fix">
                <div className="px-4">
                    <div className="md:mt-16 mt-8">
                        {
                            dataLocal && dataLocal.map((item, keys) => {
                                return <ItemBlog key={keys} data={item} />
                            })
                        }
                    </div>
                </div>
                {showSeeMore && <SeeMore clickSeeMore={(e) => hanleSeeMore(e)} />}
                {loading && <Loading />}
            </div> */}
        </>
    )
}));