import Link from 'next/link';
import React, { memo } from 'react';

export const ItemProduct = memo((props) => {
    const handleClick1 = (e) => {
        e.preventDefault();
    };


    const addSrc = (num, potision, char) => {

        num = num.toString();
        if (num.length > potision) {
            var new_num = num.substring(0, num.length - potision) + char + num.slice(-potision);
            return new_num
        } else {
            // var new_num = '0=' + num;
            return num
        }
    }
    const setImage = (item) => {
        if (item.name.substring(0, 3) != 'DSC' || item.name.substr(item.name.length - 6) === 'scaled') {
            if (item.src.search('-570x570') == -1) {
                item.src = addSrc(item.src, 4, '-570x570')
            }
        } else {
            if (item.src.search('-570x570') == -1) {
                item.src = item.src.replace("-scaled", "-570x570")
            }
        }
        return item.src
    }

    const setImgCDN = (sourceUrl) => {

        if (props?.checkCDN) {
            if (sourceUrl.search('-570x570') !== -1) {
                sourceUrl = sourceUrl.replace("-570x570", "")
                sourceUrl = sourceUrl + '?morph=280x373'
                return sourceUrl
            } else {
                return sourceUrl

            }
        }
        sourceUrl = sourceUrl
        return sourceUrl

    }

    // console.log('price', props.data.price.split(' - '))
    // console.log('regularPrice', props?.data?.variations?.nodes)

    const formatCurencyToNumber = (currency) => {
        // var currency = "528.000₫"; //it works for US-style currency strings as well
        // var cur_re = /\D*(\d+|\d.*?\d)(?:\D+(\d{2}))?\D*$/;
        // var parts = cur_re.exec(currency);
        // // console.log('parts', parts)
        // // console.log('currency', currency)
        // var number = parseFloat(parts[1].replace(/\D/, '') + '.' + (parts[2] ? parts[2] : '00'));
        // return number
        var number = currency?.toString()?.match(/\d/g);
        number = number?.join("");
        return number?.toString()
    }


    var km = []
    if (props.data?.salePrice !== null) {
        let kmm = []
        if (props?.data?.variations?.nodes !== undefined) {
            props?.data?.variations?.nodes.forEach(element => {
                if (element?.salePrice !== null) {
                    if (formatCurencyToNumber(element.salePrice) !== formatCurencyToNumber(element.regularPrice)) {
                        const ctkm = (formatCurencyToNumber(element.regularPrice) - formatCurencyToNumber(element.salePrice)) / (formatCurencyToNumber(element.regularPrice) / 100)
                        // console.log('mk', Math.floor(km))
                        kmm.push(Math.floor(ctkm))
                    }
                }

            });
            km = kmm
        }
        if (formatCurencyToNumber(props?.data?.salePrice) !== formatCurencyToNumber(props?.data?.regularPrice)) {
            const ctkm = (formatCurencyToNumber(props?.data?.regularPrice) - formatCurencyToNumber(props?.data?.salePrice)) / (formatCurencyToNumber(props?.data?.regularPrice) / 100)
            // console.log('mk', Math.floor(km))
            kmm.push(Math.floor(ctkm))
        }
        km = kmm
    }

    return (
        <>
            <Link href={`/product/${props.data.slug}`}>
                <a>
                    <div className=" z-0">
                        <div className="relative">
                            <div className="relative">
                                <img className={'w-full'} src={`${props.data?.images !== undefined ? setImage(props.data.images[0]) : setImgCDN(props.data.image?.sourceUrl)} `} alt={`${props.data?.name}`} />
                                {km.length > 0 &&
                                    <div className="absolute" style={{ top: '0.5rem', backgroundColor: 'red' }}>
                                        <span className="text-sm px-2 text-white">{Math.max(...km)}% OFF</span>
                                    </div>
                                }
                                {props.data.stockQuantity &&
                                    <div className="absolute" style={{ top: '0.5rem', right: '0.5rem', backgroundColor: 'black', }}>
                                        <span className="text-sm px-2 text-white">{props.data?.stockQuantity} LEFT</span>
                                    </div>
                                }
                            </div>
                            <div className="absolute top-0 w-full h-full hover:bg-ffffff99 hover:opacity-100 flex justify-center items-center opacity-0 ">
                                <div className="text-center">
                                    <div className="font-semibold text-lg leading-snug">{`${props.data?.name}`}</div>
                                    {
                                        km.length > 0 &&
                                        <div className="leading-5 text-base line-through">
                                            {props.data?.regularPrice.slice(-1) !== '₫' ? props.data?.regularPrice.toString()?.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + '₫' : props.data.regularPrice}
                                        </div>
                                    }

                                    <div className="leading-5 text-base">
                                        {props.data?.price?.slice(-1) !== '₫' ? props.data?.price?.toString()?.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + '₫' : props?.data?.price}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </Link>
        </>
    )
});