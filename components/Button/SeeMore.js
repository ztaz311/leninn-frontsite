import React, { memo, useState, useEffect } from 'react';
import { withTranslation } from '../../i18n';

export const SeeMore = withTranslation('common')(memo((props) => {
    const clickSeeMore = (e) => {
        props.clickSeeMore();
    }
    return (
        <div className="flex justify-center pt-8">
            <button className="border border-black text-black px-4 py-2 md:px-10 font-semibold" onClick={(e) => clickSeeMore(e)}>{props.t('see_more')}</button>
        </div>
    );
}));
