module.exports = {
  purge: ['./pages/**/*.js', './components/**/*.js'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'xs': '360px',
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',

    },
    extend: {
      height: {
        '70px': '70px',
        '17': '4.25rem',
        '150': '37.5rem',
        '800': '800px'
      },
      width: {
        '27': '6.75rem',

      },
      paddingTop: {
        '50t': '50%'
      },
      backgroundColor: {
        'ffffff99': '#ffffffc7',
        'F7F7F7': '#F7F7F7'
      },
      color: {
        '949494': '#949494'
      },
      zIndex: {
        '0': 0,
        '10': 10,
        '20': 20,
        '30': 30,
        '40': 40,
        '50': 50,
        '25': 25,
        '50': 50,
        '75': 75,
        '100': 100,
        '1000': 1000,
        'auto': 'auto',
      }
    }
  },
  variants: {
    extend: {

    },
  },
  plugins: [],
}
