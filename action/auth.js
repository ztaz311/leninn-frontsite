import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';


export const actRegister = (body) => {
    return HTTP.callApiMAIN('wp/v1/users/register', 'POST', body).then(response => {
        if (response?.code === 200) {
            return new Promise((resolve) => { //ok
                resolve({ status: 200 })
            })
        } else {
            if (response?.data?.code === 406) { // ton tai 
                return new Promise((resolve) => {
                    resolve({ status: 406 })
                })
            } else {
                return new Promise((resolve) => {
                    resolve({ status: -1 })
                })
            }
        }
    })
}

export const actLogin = (body) => {
    return HTTP.callApiMAIN('jwt-auth/v1/token', 'POST', body).then(response => {
        // console.log('responseLOgin', response)
        if (response?.status === 403) {
            return new Promise((resolve) => {
                resolve({ status: 403 })
            })
        }
        if (response.token !== '') {
            return new Promise((resolve) => {
                resolve({ status: 200, data: response })
            })
        }
    })
}

export const actGetInfor = (id) => {
    return HTTP.callApi('customers/' + id, 'GET', null).then(response => {
        if (response?.id !== undefined) {
            return new Promise((resolve) => {
                resolve({ status: 200, data: response })
            })
        }
    })
}
export const actUpdateInfor = (id, body) => {
    return HTTP.callApi('customers/' + id, 'PUT', body).then(response => {
        if (response?.id !== undefined) {
            return new Promise((resolve) => {
                resolve({ status: 200, data: response })
            })
        } else if (response?.data?.data?.status === 400) {
            return new Promise((resolve) => {
                resolve({ status: 400 })
            })
        } else {
            return new Promise((resolve) => {
                resolve({ status: -1 })
            })
        }
    })
}
export const actGetOrder = (id, page = 1, per_page = 5) => {
    return HTTP.callApi('orders?page=' + page + '&per_page=' + per_page + '&customer=' + id, 'GET', null).then(response => {
        // console.log('response', response)
        if (response?.length > 0) {
            return new Promise((resolve) => {
                resolve({ status: 200, data: response })
            })
        } else {
            return new Promise((resolve) => {
                resolve({ status: -1, data: [] })
            })
        }
    })
}

export const actDetailtOrder = (id) => {
    return HTTP.callApi(`orders/${id}`, 'GET', null).then(response => {
        if (response?.id !== undefined) {
            return new Promise((resolve) => {
                resolve({ status: 200, data: response })
            })
        } else {
            return new Promise((resolve) => {
                resolve({ status: -1 })
            })
        }
    })
}

export const actLoginFacebook = (token) => {
    return HTTP.callApiMAIN('sociallogin/v1/facebook', 'POST', { token }).then(response => {
        // console.log('response', response)
        // console.log('token', token)
        if (response?.token !== "" || response?.token !== undefined) {
            return new Promise((resolve) => {
                resolve({ status: 200, data: { profile: response.data, token: 'facebook_token' } })
            })
        } else {
            return new Promise((resolve) => {
                resolve({ status: -1 })
            })
        }
    })
}

export const actLoginGoogle = (token) => {
    return HTTP.callApiMAIN('sociallogin/v1/google', 'POST', { token }).then(response => {
        // console.log('token', response)
        if (response?.data?.id !== "" || response?.data?.id !== undefined) {
            return new Promise((resolve) => {
                resolve({ status: 200, data: { profile: response.data, token: 'google_token' } })
            })
        } else {
            return new Promise((resolve) => {
                resolve({ status: -1 })
            })
        }
    })
}

export const actSendEmail = (email) => {
    return HTTP.callApiMAIN('forgotpassword/v1/reset', 'POST', { user_login: email }).then(response => {
        if (response.status === 200) {
            return new Promise((resolve) => {
                resolve({ status: 200 })
            })
        } else {
            return new Promise((resolve) => {
                resolve({ status: -1 })
            })
        }
    })
}
export const actChangePasswordEmail = (body) => {
    return HTTP.callApiMAIN('forgotpassword/v1/confirm', 'POST', body).then(response => {
        // console.log('res', response)
        if (response.status === 200) {
            return new Promise((resolve) => {
                resolve({ status: 200 })
            })
        } else {
            return new Promise((resolve) => {
                resolve({ status: -1 })
            })
        }
    })
}