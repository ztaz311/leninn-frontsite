import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';

export const actGetListCategory = (body, proxy = '') => {
    if (proxy !== '') {
        return HTTP.callApiProxy('products/categories', 'GET', body).then(response => {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            });
        })
    } else {
        return HTTP.callApi('products/categories', 'GET', body).then(response => {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            });
        })
    }
}

export const actGetLogo = (body, proxy = '') => {
    return HTTP.callApiWP('logo', 'GET', body).then(response => {
        if (response?.url !== '' && response?.url !== undefined) {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response.url })
            });
        } else {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: -1 })
            });
        }
    })

}