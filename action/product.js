import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';

export const actGetListProduct = (category_id, page, per_page, body, proxy = '') => {
    var paramCategory = '?category=' + category_id;
    var paramPage = '&page=' + page;
    var paramOrderby = '&orderby=id'
    var paramOrder = '&order=desc'
    var paramPerPage = per_page !== '' ? '&per_page=' + per_page : ''
    if (proxy !== '') {
        return HTTP.callApiProxy('products' + paramCategory + paramPage + paramOrderby + paramOrder + paramPerPage, 'GET', body).then(response => {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            });
        })
    } else {
        return HTTP.callApi('products' + paramCategory + paramPage + paramOrderby + paramOrder + paramPerPage, 'GET', body).then(response => {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            });
        })
    }
}

export const actGetListProductInCategory = (id, arr, body, proxy = '') => {
    var paramParentExclude = '&exclude='
    var arrN = '';
    arr.map((item, keys) => {
        arrN = arrN + paramParentExclude + item;
    })
    paramParentExclude = arrN;
    if (proxy !== '') {
        return HTTP.callApiProxy('products?per_page=4&order_by=desc&category=' + id + paramParentExclude, 'GET', body).then(response => {
            // return new Promise.then(.resolve({ status: 200, data: response });)
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            });
            // return response;
        })
    } else {
        return HTTP.callApi('products?per_page=4&order_by=desc&category=' + id + paramParentExclude, 'GET', body).then(response => {
            // return new Promise.then(.resolve({ status: 200, data: response });)
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            });
            // return response;
        })
    }
}
export const actGetProductDetail = (slug, body = null, proxy = '') => {
    if (proxy !== '') {
        return HTTP.callApiProxy('products?slug=' + slug, 'GET', body).then(response => {
            if (response.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: response[0] })
                });
            } else {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: [] })
                });
            }
        })
    } else {
        return HTTP.callApi('products?slug=' + slug, 'GET', body).then(response => {
            if (response.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: response[0] })
                });
            } else {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: [] })
                });
            }
        })
    }
}


export const actGetValidationId = (id, proxy = '') => {
    if (proxy !== '') {
        return HTTP.callApiProxy(`products/${id}/variations?per_page=100`, 'GET', null).then(response => {
            if (response?.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: response })
                });
            }
        })
    } else {
        return HTTP.callApi(`products/${id}/variations?per_page=100`, 'GET', null).then(response => {
            if (response?.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: response })
                });
            }
        })
    }
}


export const actSendCart = (body) => {
    return HTTP.callApi(`orders`, 'POST', body).then(response => {
        if (response?.id !== undefined) {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200 })
            });
        } else {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: -1 })
            });
        }
    })
}
export const actGetCoupons = (code) => {
    return HTTP.callApi(`coupons?code=${code}`, 'GET', null).then(response => {
        if (response.length > 0) {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            });
        } else {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: -1 })
            });
        }
    })
}