import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';


export const actGetIntro = (slug, body, proxy = '') => {
    var paramSlug = 'slug=' + slug;
    if (proxy !== '') {
        return HTTP.callApiWP_Proxy('pages?' + paramSlug, 'GET', body).then(response => {
            // console.log('response', response)

            if (response.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: response[0] })
                });
            } else {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: [] })
                });
            }
        })
    } else {
        return HTTP.callApiWP('pages?' + paramSlug, 'GET', body).then(response => {
            // console.log('response2', response)

            if (response.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: response[0] })
                });
            } else {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: [] })
                });
            }
        })
    }
}
export const actGetPostInCategory = (per_page, page, id, arr, body) => {
    var paramPerPage = '&per_page=' + per_page;
    var paramCategory = '&category=' + id;
    var paramParentExclude = '&exclude='
    var arrN = '';
    arr.map((item, keys) => {
        arrN = arrN + paramParentExclude + item;
    })
    paramParentExclude = arrN;
    return HTTP.callApiWP('posts?order=desc&orderby=id' + paramCategory + paramParentExclude + paramPerPage, 'GET', body).then(response => {
        if (response.status == 400) {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: [] })
            })
        }
        return new Promise(async (resolve, rejects) => {
            resolve({ status: 200, data: response })
        })
    })
}
export const actGetListAcf = (per_page, page, body, proxy = '') => {
    if (proxy !== '') {
        return HTTP.callApiMAIN_Proxy('acf/v3/options/logo-client?per_page=12', 'GET', body).then(response => {
            if (response.status == 400) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: [] })
                })
            }
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response.acf.logo_client })
            })
        })
    } else {
        return HTTP.callApiMAIN('acf/v3/options/logo-client?per_page=12', 'GET', body).then(response => {
            if (response.status == 400) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: [] })
                })
            }
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response.acf.logo_client })
            })
        })
    }
}
export const actGetListSearchProduct = (per_page, page, keyword, type, body) => {
    var paramPerPage = '&per_page=' + per_page;
    var paramPage = '&page=' + page;
    var paramSearch = '&keyword=' + keyword;
    var paramSubtype = 'subtype=' + type;
    return HTTP.callApiMAIN('product/v1/search?' + paramSubtype + paramSearch + paramPage + paramPerPage, 'GET', body).then(response => {
        if (response.status == 400) {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: [] })
            })
        }
        return new Promise(async (resolve, rejects) => {
            resolve({ status: 200, data: response[0].data, total: response[0].total })
        })
    })
}

export const actGetFooter = (lang) => {
    let url = `footer-VI`
    if (lang === 'en') {
        url = `footer-EN`
    }
    return HTTP.callApiV3(url, 'GET', null).then(response => {
        // console.log('response', response)
        if (response?.acf !== undefined) {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response.acf })
            })
        } else {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: -1 })
            })
        }
        // if (response.status == 400) {
        //     return new Promise(async (resolve, rejects) => {
        //         resolve({ status: 200, data: [] })
        //     })
        // }
        // return new Promise(async (resolve, rejects) => {
        //     resolve({ status: 200, data: response[0].data, total: response[0].total })
        // })
    })
}
export const actGetBanner = (lang) => {
    return HTTP.callApiWP_Proxy('slide_trang_chu', 'GET', null).then(response => {
        return new Promise(async (resolve, rejects) => {
            resolve({ data: response[0]?.acf.slide_home.img || null })
        })
    })
}