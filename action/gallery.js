import * as Types from '../constants/ActionType';
import HTTP from '../services/HTTP';

export const actGetListGallery = (per_page, page, body, proxy = '') => {
    var paramPerPage = '&per_page=' + per_page;
    var paramPage = '&page=' + page;
    if (proxy !== '') {
        return HTTP.callApiWP_Proxy('gallery?order=desc&orderby=id' + paramPerPage + paramPage, 'GET', body).then(response => {
            // return new Promise.then(.resolve({ status: 200, data: response });)
            if (response.status == 400) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: [] })
                })
            }
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            })
            // return response;
        })
    } else {
        return HTTP.callApiWP('gallery?order=desc&orderby=id' + paramPerPage + paramPage, 'GET', body).then(response => {
            // return new Promise.then(.resolve({ status: 200, data: response });)
            if (response.status == 400) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: [] })
                })
            }
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: response })
            })
            // return response;
        })
    }
}
export const actGetAlbum = (slug, body, proxy = '') => {
    var paramSlug = '&slug=' + slug;
    if (proxy !== '') {
        return HTTP.callApiWP_Proxy('gallery?' + paramSlug, 'GET', body).then(response => {
            if (response.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: response[0] })
                });
            } else {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: [] })
                });
            }
        })
    } else {
        return HTTP.callApiWP('gallery?' + paramSlug, 'GET', body).then(response => {
            if (response.length > 0) {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 200, data: response[0] })
                });
            } else {
                return new Promise(async (resolve, rejects) => {
                    resolve({ status: 404, data: [] })
                });
            }
        })
    }
}
export const actGetPostInCategory = (per_page, page, id, arr, body) => {
    var paramPerPage = '&per_page=' + per_page;
    var paramCategory = '&category=' + id;
    var paramParentExclude = '&exclude='
    var arrN = '';
    arr.map((item, keys) => {
        arrN = arrN + paramParentExclude + item;
    })
    paramParentExclude = arrN;
    return HTTP.callApiWP('posts?order=desc&orderby=id' + paramCategory + paramParentExclude + paramPerPage, 'GET', body).then(response => {
        if (response.status == 400) {
            return new Promise(async (resolve, rejects) => {
                resolve({ status: 200, data: [] })
            })
        }
        return new Promise(async (resolve, rejects) => {
            resolve({ status: 200, data: response })
        })
    })
}